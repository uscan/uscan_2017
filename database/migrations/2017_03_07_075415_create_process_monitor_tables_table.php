<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessMonitorTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('process_monitor', function (Blueprint $table) {
          $table->string('log_id',15);
          $table->string('enterprise_id',15);
          $table->string('reference_id',15);
          $table->string('referece_type',15);
          $table->string('state',50);
          $table->datetime('start_date');
          $table->datetime('end_date');
          $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
          $table->primary(['log_id','enterprise_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('process_monitor');
    }
}
