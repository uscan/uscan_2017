<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportGeneratorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('report_generators', function (Blueprint $table) {
          $table->string('report_id',15);
          $table->string('variant',60)->unique();

          $table->date('start_date');
          $table->date('end_date');

          $table->string('created_by', 100);
          $table->string('updated_by', 100);

          $table->integer('report_name_id');
          $table->foreign('report_name_id')->references('report_name_id')->on('report_name');

          $table->string('enterprise_id',15);
          $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');

          $table->timestamps();
          $table->primary(['report_id','enterprise_id']);
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_generators');
    }
}
