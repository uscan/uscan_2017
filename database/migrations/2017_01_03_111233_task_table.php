<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TaskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('task', function (Blueprint $table) {

            $table->string('task_id',15);

            $table->string('task_name', 150);
            $table->tinyInteger('active');
            $table->string('created_by', 100);
            $table->string('updated_by', 100);
            $table->integer('task_type_id');
            $table->foreign('task_type_id')->references('task_type_id')->on('task_type');

            $table->string('enterprise_id',15);
            $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');

            $table->string('customer_id',15);
            $table->foreign('customer_id')->references('customer_id')->on('customer_child');

            $table->string('plant_id',15);
            $table->foreign('plant_id')->references('plant_id')->on('plant');

            $table->string('portal_id',15);
            $table->foreign('portal_id')->references('portal_id')->on('portal');

            $table->string('program_id',15);
            $table->foreign('program_id')->references('program_id')->on('program');

            $table->softDeletes();
            $table->primary(['task_id','enterprise_id']);
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
