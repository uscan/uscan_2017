<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportNameCustomerReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_name_customer', function (Blueprint $table) {

        $table->string('report_id',15);
        $table->foreign('report_id')->references('report_id')->on('report_generators');

        $table->string('customer_id',15);
        $table->foreign('customer_id')->references('customer_id')->on('customer_child');


        $table->string('enterprise_id',15);
        $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
        $table->primary(array('report_id', 'customer_id','enterprise_id'));
        $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_name_customer');
    }
}
