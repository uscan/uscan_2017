<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerMasterTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_master_type', function (Blueprint $table) {
              $table->integer('customer_master_type_id');
              $table->string('customer_master_type',30);
              $table->string('enterprise_id',15);
              $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');
              $table->timestamps();
              $table->softDeletes();
              $table->primary(['customer_master_type_id','enterprise_id'],'multiple_primary_key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_master_type');
    }
}
