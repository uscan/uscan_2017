<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlantContactModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plant_contact', function (Blueprint $table) {
            $table->increments('contact_id');
            $table->string('contact_name',30);
            $table->string('contact_email_id',50);
            $table->string('contact_number',15);

            $table->string('plant_id',15);
            $table->foreign('plant_id')->references('plant_id')->on('plant');
            $table->softDeletes();
            $table->string('enterprise_id',15);
            $table->foreign('enterprise_id')->references('enterprise_id')->on('enterprise');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plant_contact');
    }
}
