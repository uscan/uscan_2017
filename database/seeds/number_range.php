<?php

use Illuminate\Database\Seeder;

class number_range extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $number_range  = array(
                         array('number_range_id'=>'1','table_name' => 'enterprise','current_no' => '0010000002','starting_no' => '0010000002','ending_no' => '0019999999','description' => 'enterprise','enterprise_id' => '0010000002','created_by' => 'Super Admin','updated_by' => 'Super Admin'),
                         array('number_range_id'=>'2','table_name' => 'user','current_no' => '0020000001','starting_no' => '0020000001','ending_no' => '0029999999','description' => 'user','enterprise_id' => '0010000002','created_by' => 'Super Admin','updated_by' => 'Super Admin'),
                         array('number_range_id'=>'3','table_name' => 'customer_parent','current_no' => '0030005','starting_no' => '0030001','ending_no' => '0039999','description' => 'Customer Parent','enterprise_id' => '0010000002','created_by' => 'Super Admin','updated_by' => 'Super Admin'),
                         array('number_range_id'=>'4','table_name' => 'customer_child','current_no' => '0030000006','starting_no' => '0030000001','ending_no' => '0039999999','description' => 'Customer Child','enterprise_id' => '0010000002','created_by' => 'Super Admin','updated_by' => 'Super Admin'),
                         array('number_range_id'=>'5','table_name' => 'plant','current_no' => '0040000101','starting_no' => '0040000001','ending_no' => '0049999999','description' => 'Plant','enterprise_id' => '0010000002','created_by' => 'Super Admin','updated_by' => 'Super Admin'),
                         array('number_range_id'=>'6','table_name' => 'portal','current_no' => '0050000000','starting_no' => '0050000001','ending_no' => '0059999999','description' => 'Portal','enterprise_id' => '0010000002','created_by' => 'Super Admin','updated_by' => 'Super Admin'),
                         array('number_range_id'=>'7','table_name' => 'task','current_no' => '0060000000','starting_no' => '0060000001','ending_no' => '0069999999','description' => 'task','enterprise_id' => '0010000002','created_by' => 'Super Admin','updated_by' => 'Super Admin'),
                         array('number_range_id'=>'8','table_name' => 'scheduler','current_no' => '0070000000','starting_no' => '0070000001','ending_no' => '0079999999','description' => 'Super Admin','enterprise_id' => '0010000002','created_by' => 'Super Admin','updated_by' => 'Super Admin'),
                         array('number_range_id'=>'9','table_name' => 'report_generator','current_no' => '0080000000','starting_no' => '0080000001','ending_no' => '0089999999','description' => 'report_generator','enterprise_id' => '0010000002','created_by' => 'Super Admin','updated_by' => 'Super Admin'),
                         array('number_range_id'=>'10','table_name' => 'program','current_no' => '0090000005','starting_no' => '0090000001','ending_no' => '0099999999','description' => 'program','enterprise_id' => '0010000002','created_by' => 'Super Admin','updated_by' => 'Super Admin'),
                         array('number_range_id'=>'11','table_name' => 'process_monitor','current_no' => '0000001000','starting_no' => '0000001001','ending_no' => '9999999999','description' => 'process_monitor','enterprise_id' => '0010000002','created_by' => 'Super Admin','updated_by' => 'Super Admin'),
                       );
        DB::table('number_range')->insert($number_range);
    }
}
