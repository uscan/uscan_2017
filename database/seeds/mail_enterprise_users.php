<?php

use Illuminate\Database\Seeder;

class mail_enterprise_users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $mail_enterprise_users = array(array('mail_comm_id' => '1','ent_user_id' => 'Jhondoe@doe.com','ent_user_name' => 'Jhondoe','user_group_id' => '1','created_by' => 'Jhondoe','updated_by' => 'Jhondoe','enterprise_id' => '0010000002'),
                                       array('mail_comm_id' => '2','ent_user_id' => 'Dan@dd.com','ent_user_name' => 'Dan','user_group_id' => '1','created_by' => 'Dan','updated_by' => 'Dan','enterprise_id' => '0010000002'),
                                       array('mail_comm_id' => '3','ent_user_id' => 'Site Manager','ent_user_name' => 'Site Manager','user_group_id' => '1','created_by' => 'Dan','updated_by' => 'Jhondoe','enterprise_id' => '0010000002'),
                                       array('mail_comm_id' => '4','ent_user_id' => 'HR Manager','ent_user_name' => 'HR Manager','user_group_id' => '1','created_by' => 'Dan','updated_by' => 'Dan','enterprise_id' => '0010000002'),
                                       array('mail_comm_id' => '5','ent_user_id' => 'Financial Controller','ent_user_name' => 'Financial Controller','user_group_id' => '1','created_by' => 'Dan','updated_by' => 'Jhondoe','enterprise_id' => '0010000002'),
                                       array('mail_comm_id' => '6','ent_user_id' => 'Quality Manager','ent_user_name' => 'Quality Manager','user_group_id' => '1','created_by' => 'Dan','updated_by' => 'Dan','enterprise_id' => '0010000002'),
                                       array('mail_comm_id' => '7','ent_user_id' => 'Logistics Manager','ent_user_name' => 'Logistics Managerr','user_group_id' => '1','created_by' => 'Dan','updated_by' => 'Jhondoe','enterprise_id' => '0010000002'),
                                       array('mail_comm_id' => '8','ent_user_id' => 'Health & Safety Manager','ent_user_name' => 'Health & Safety Manager','user_group_id' => '1','created_by' => 'Dan','updated_by' => 'Dan','enterprise_id' => '0010000002'),
                                       array('mail_comm_id' => '9','ent_user_id' => 'Environmental Manager','ent_user_name' => 'Environmental Manager','user_group_id' => '1','created_by' => 'Dan','updated_by' => 'Dan','enterprise_id' => '0010000002')

                                      );
        DB::table('mail_enterprise_users')->insert($mail_enterprise_users);
    }
}
