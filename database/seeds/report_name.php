<?php

use Illuminate\Database\Seeder;

class report_name extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $report_name  = array(
          array('report_name_id' => '1','report_name' => 'Master Dashboard','report_type_id' => '1','program_id' => '0090000006','enterprise_id' => '0010000002')
        );
        DB::table('report_name')->insert($report_name);

      }
    }
