<?php

use Illuminate\Database\Seeder;

class task_communication_options extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $task_communication_options  = array(
                         array('communication_option_id' => '1','communication_option_name' => 'Send','enterprise_id' => '0010000002'),
                         array('communication_option_id' => '2','communication_option_name' => 'Always Send','enterprise_id' => '0010000002'));
        DB::table('task_communication_options')->insert($task_communication_options);
    }
}
