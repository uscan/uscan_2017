<?php

use Illuminate\Database\Seeder;

class enterprise extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         //

        // DB::table('task_type')->delete();
        // DB::table('program')->delete();
        // DB::table('number_range')->delete();
        // DB::table('user_group')->delete();
        // DB::table('role_right')->delete();
        // DB::table('rights')->delete();
        // DB::table('role')->delete();
        // DB::table('customer_child')->delete();
        // DB::table('customer_type')->delete();
        // DB::table('customer_master_type')->delete();
        // DB::table('enterprise_contact')->delete();
        // DB::table('enterprise_address')->delete();
        // DB::table('enterprise')->delete();
        // DB::table('frequency')->delete();
        // DB::table('start_type')->delete();

        $enterprise  = array( array('enterprise_id' => '0010000001','enterprise_name' => 'Univva','tin_number' => 'tin_number','duns_number' => 'duns_number','license_number' => 'license_number','active' => '1','created_by' => 'user','updated_by' => 'user'),
                              array('enterprise_id' => '0010000002','enterprise_name' => 'Federal Mogul','tin_number' => 'tin_number','duns_number' => 'duns_number','license_number' => 'license_number','active' => '1','created_by' => 'user','updated_by' => 'user'));
        DB::table('enterprise')->insert($enterprise);

        $enterprise_address  = array( array('soldto_street' => 'soldto_street','soldto_city' => 'soldto_city','soldto_state' => 'soldto_state','soldto_country' => 'soldto_country','soldto_region' => 'soldto_region','soldto_zip_code' => '111','billto_street' => 'billto_street','billto_city' => 'billto_city','billto_street' => 'billto_street','billto_state' => 'billto_state','billto_country' => 'billto_country','billto_region' => 'billto_region','billto_zip_code' => '111','enterprise_id' => '0010000002'), array('soldto_street' => 'soldto_street','soldto_city' => 'soldto_city','soldto_state' => 'soldto_state','soldto_country' => 'soldto_country','soldto_region' => 'soldto_region','soldto_zip_code' => '111','billto_street' => 'billto_street','billto_city' => 'billto_city','billto_state' => 'billto_state','billto_street' => 'billto_street','billto_country' => 'billto_country','billto_region' => 'billto_region','billto_zip_code' => '111','enterprise_id' => '0010000002'));
        DB::table('enterprise_address')->insert($enterprise_address);

        $enterprise_contact  = array( array('primary_contact_name' => 'primary_contact_name','primary_contact_email_id' => 'primary_contact_email_id','primary_contact_number' => '1111','secondary_contact_name' => 'secondary_contact_name','secondary_contact_email_id' => 'secondary_contact_email_id','secondary_contact_number' => '11111','enterprise_id' => '0010000002'), array('primary_contact_name' => 'primary_contact_name','primary_contact_email_id' => 'primary_contact_email_id','primary_contact_number' => '1111','secondary_contact_name' => 'secondary_contact_name','secondary_contact_email_id' => 'secondary_contact_email_id','secondary_contact_number' => '111111','enterprise_id' => '0010000002'));
        DB::table('enterprise_contact')->insert($enterprise_contact);

    }
}
