$(document).ready(function(){
  $("#plant-table").validate({
          rules: {

            plant_name:{
							 required: true,
						 },
          },
        messages: {
          plant_name:"&nbsp;&nbsp;*",
            }
  });

  $("#second_form").validate({
          rules: {

          },
        messages: {

            }
  });

  $("#third_form").validate({
          rules: {

          },
        messages: {

            }
  });

  $("#additional").validate({
          rules: {
              contact_no: {
  								 phoneUS: true,
  						 },
               contact_email: {
                  email: true
              },
          },
        messages: {

            }
  });

});
