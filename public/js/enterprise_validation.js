$(document).ready(function(){
  $("#enterprise-table").validate({
          rules: {

            enterprise_name:"required",
            tin_number:"required",
            duns_number:"required",
            license_number:"required",

          },
        messages: {
          enterprise_name:"&nbsp;&nbsp;*",
          tin_number:"&nbsp;&nbsp;*",
          duns_number:"&nbsp;&nbsp;*",
          license_number:"&nbsp;&nbsp;*",
            }
  });

  $("#second_form").validate({
          rules: {

          },
        messages: {

            }
  });

  $("#third_form").validate({
          rules: {

          },
        messages: {

            }
  });

  $("#additional").validate({
          rules: {
              contact_no: {
  								 phoneUS: true,
  						 },
               contact_email: {
                  email: true
              },
          },
        messages: {

            }
  });

});
