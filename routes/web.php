<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

  if (Auth::check())
    {

        return redirect('home');

    }
    else {

       return redirect('login');

    }


});

Route::get('/logout', function () {

  if (Auth::check())
    {

      return redirect('home');

    }
    else {
        return view('auth/login',['status' => 'logout']);
    }


});

Auth::routes();

Route::get('/home', 'HomeController@index');



Route::group(['middleware' => 'auth'], function () {


      Route::group(['middleware' => 'ProductAdmin'], function () {

            Route::get('customer_create', function()
        {
            return View::make('customer/customer_create');
        });

        Route::get('customer_display', function()
        {
            return View::make('customer/customer_display');
        });

        Route::get('customer_change', function()
        {
            return View::make('customer/customer_change');
        });

        Route::post('parent_insert', array(
            'uses' => 'customer_parent_controller@store',
            'as' => 'parent_insert'
        ));

        Route::get('parent_results', array(
            'uses' => 'customer_parent_controller@show',
            'as' => 'parent_results'
        ));

        Route::get('customer_type', array(
            'uses' => 'customer_type_controller@index',
            'as' => 'customer_type'
        ));



        Route::post('validateCustomerCreate', array(
            'uses' => 'customer_child_controller@validateCustomerCreate',
            'as' => 'validateCustomerCreate'
        ));

        Route::post('validateCustomerChange', array(
            'uses' => 'customer_child_controller@validateCustomerChange',
            'as' => 'validateCustomerChange'
        ));



        Route::post('validateCustomerParentCreate', array(
            'uses' => 'customer_parent_controller@validateCustomerParentCreate',
            'as' => 'validateCustomerParent'
        ));

        Route::post('validateCustomerParentChange', array(
            'uses' => 'customer_parent_controller@validateCustomerParentChange',
            'as' => 'validateCustomerParentChange'
        ));


        Route::get('customer_master_type', array(
            'uses' => 'customer_master_type_controller@index',
            'as' => 'customer_master_type'
        ));





        Route::post('child_insert', array(
            'uses' => 'customer_child_controller@store',
            'as' => 'child_insert'
        ));



        Route::post('customer_search', array(
            'uses' => 'customer_child_controller@show',
            'as' => 'customer_search'
        ));

        Route::post('parent_search', array(
            'uses' => 'customer_parent_controller@search',
            'as' => 'parent_search'
        ));


        Route::get('customer_results', array(
            'uses' => 'customer_child_controller@showall',
            'as' => 'customer_display'
        ));

        Route::post('customer_delete', 'customer_child_controller@destroy');
        Route::post('parent_delete', 'customer_parent_controller@destroy');


        Route::post('customer_update', 'customer_child_controller@update');
        Route::post('parent_update', 'customer_parent_controller@update');


        Route::get('customer_child_controller/autocomplete_customer_name', 'customer_child_controller@autocomplete_customer_name');
        Route::get('customer_child_controller/autocomplete_customer_code', 'customer_child_controller@autocomplete_customer_code');
        Route::get('customer_child_controller/autocomplete_duns_no', 'customer_child_controller@autocomplete_duns_no');
        Route::get('customer_child_controller/autocomplete_parent_name', 'customer_child_controller@autocomplete_parent_name');
        Route::get('customer_child_controller/autocomplete_parent_code', 'customer_child_controller@autocomplete_parent_code');

        Route::get('cust_name', array(
            'uses' => 'customer_child_controller@get_customer',
            'as' => 'cust_name'
        ));

        //Plant Start


        Route::get('/customer', function () {
            return view('customer/customer');
        });

        Route::get('/plant_create', function () {
            return view('plant/plant_create');
        });

        Route::get('/plant_display', function () {
            return view('plant/plant_display');
        });

        Route::get('/plant_change', function () {
           return view('plant/plant_change');
       });

        Route::post('plant_display_show', 'plant_controller@show');
        Route::get('plant_display_show_all', 'plant_controller@showall');

        // Route::get('/', function () {
        //     return view('home');
        // });





        Route::post('plant_save','plant_controller@store');
        Route::post('plant_delete', 'plant_controller@destroy');
        Route::POST('plant_edit', 'plant_controller@edit');
        Route::post('plant_update', 'plant_controller@update');
        Route::get('fetch_plant_name', 'plant_controller@fetch_plant_name');
        Route::get('fetch_plant_id', 'plant_controller@fetch_plant_id');
        Route::get('fetch_plant_code', 'plant_controller@fetch_plant_code');
        Route::get('fetch_plant_duns_no', 'plant_controller@fetch_plant_duns_no');
        Route::get('fetch_plant_supplier_code', 'plant_controller@fetch_plant_supplier_code');

        //Plant End



        //scheduler
        Route::get('/scheduler_create', function () {
            return view('scheduler/scheduler_create');
        });

        Route::get('/scheduler_display', function () {
            return view('scheduler/scheduler_display');
        });

        Route::get('/scheduler_change', function () {
            return view('scheduler/scheduler_change');
        });

        Route::get('portal_name_for_scheduler', array(
            'uses' => 'scheduler_controller@get_portal',
            'as' => 'portal_name_for_scheduler'
        ));

        Route::POST('task_name_for_scheduler', array(
            'uses' => 'scheduler_controller@get_task_name',
            'as' => 'task_name_for_scheduler'
        ));

        Route::get('task_type_for_scheduler', array(
            'uses' => 'scheduler_controller@get_task_type',
            'as' => 'task_type_for_scheduler'
        ));

        Route::get('job_priority_for_scheduler', array(
            'uses' => 'scheduler_controller@get_priority_for_scheduler',
            'as' => 'job_priority_for_scheduler'
        ));

        Route::get('start_conditions_for_scheduler', array(
            'uses' => 'scheduler_controller@get_start_conditions_for_scheduler',
            'as' => 'start_conditions_for_scheduler'
        ));

        Route::get('frequency_for_scheduler', array(
            'uses' => 'scheduler_controller@get_frequency',
            'as' => 'frequency_for_scheduler'
        ));

        Route::get('get_jobs_for_scheduler', array(
            'uses' => 'scheduler_controller@get_jobs_for_scheduler',
            'as' => 'get_jobs_for_scheduler'
        ));

        Route::POST('scheduler_save', array(
            'uses' => 'scheduler_controller@store',
            'as' => 'scheduler_save'
        ));

        Route::get('fetch_job_name', 'scheduler_controller@fetch_job_name');
        Route::get('fetch_task_type', 'scheduler_controller@fetch_task_type');
        Route::get('fetch_task_name', 'scheduler_controller@fetch_task_name');
        Route::post('job_display_show', 'scheduler_controller@show');

        Route::get('get_jobs', array(
            'uses' => 'scheduler_controller@get_jobs',
            'as' => 'get_jobs'
        ));

        Route::post('get_frequency_for_scheduler', array(
            'uses' => 'scheduler_controller@get_frequency_for_scheduler',
            'as' => 'get_frequency_for_scheduler'
        ));

        Route::post('get_after_job_for_scheduler', array(
            'uses' => 'scheduler_controller@get_after_job_for_scheduler',
            'as' => 'get_after_job_for_scheduler'
        ));

        Route::get('get_task_name_for_scheduler', array(
            'uses' => 'scheduler_controller@get_task_name_for_scheduler',
            'as' => 'get_task_name_for_scheduler'
        ));

        Route::post('scheduler_update', array(
            'uses' => 'scheduler_controller@scheduler_update',
            'as' => 'scheduler_update'
        ));

        Route::post('scheduler_delete', array(
            'uses' => 'scheduler_controller@destroy',
            'as' => 'scheduler_delete'
        ));

        //**********scheduler END **********************

        //portal_start


        //Routes for the page portal_create starts

        // Route::get('cust_name', array(
        //     'uses' => 'portal_controller@get_customer',
        //     'as' => 'cust_name'
        // ));

        Route::get('plant_name', array(
            'uses' => 'portal_controller@get_plant',
            'as' => 'plant_name'
        ));

        Route::get('portal_create','portal_controller@portal_create_index');
        Route::post('portal_create_store','portal_controller@store');

        //Routes for the page portal_create ends


        //Routes for the page portal_change starts

        Route::get('portal_change1', 'portal_controller@portal_display_index');
        Route::post('portal_change_show', 'portal_controller@show');

         Route::get('portal_change',function()
         {
          return View::make('portal/portal_change');
         });

         Route::post('portal_edit',function()
         {
          return View::make('portal_display');
         });


        Route::post('portal_delete', 'portal_controller@destroy');
        Route::post('portal_update', 'portal_controller@update');

        Route::get('portal_controller/autocomplete_portal_name', 'portal_controller@autocomplete_portal_name');
        Route::get('portal_controller/autocomplete_portal_url', 'portal_controller@autocomplete_portal_url');
        Route::get('portal_controller/autocomplete_customer_name', 'portal_controller@autocomplete_customer_name');
        Route::get('portal_controller/autocomplete_plant_name', 'portal_controller@autocomplete_plant_name');

        Route::post('port_pd_ref', array(
            'uses' => 'portal_controller@port_pd_ref',
            'as' => 'port_pd_ref'
        ));

        Route::post('portal_details_results', array(
            'uses' => 'portal_controller@portal_details_results',
            'as' => 'portal_details_results'
        ));

        Route::post('pd_delete', array(
            'uses' => 'portal_controller@pd_delete',
            'as' => 'pd_delete'
        ));

        Route::get('get_current_prt_det_id', array(
            'uses' => 'portal_controller@get_current_prt_det_id',
            'as' => 'get_current_prt_det_id'
        ));
        //Routes for the page portal_change ends


        //Routes for the page portal_display starts
        Route::get('portal_display1', 'portal_controller@portal_display_index');
        Route::post('portal_display_show', 'portal_controller@show');

         Route::get('portal_display',function()
         {
          return View::make('portal/portal_display');
         });
        //Routes for the page portal_display ends



        //portal_end


        //routes for enterprise

        // Route::get('enterprise_create','enterprise_controller@enterprise_create_index');
        // Route::post('enterprise_create_store','enterprise_controller@store');
        //

        // Route::post('enterprise_display_show', 'enterprise_controller@show');

        // Route::get('enterprise_display',function()
        // {
        //  return View::make('enterprise/enterprise_display');
        // });

        Route::get('enterprise_create',function()
        {
         return View::make('enterprise/enterprise_create');
        });

        Route::get('enterprise_display',function()
        {
         return View::make('enterprise/enterprise_display');
        });

        Route::get('enterprise_change',function()
        {
         return View::make('enterprise/enterprise_change');
        });

        Route::post('enterprise_update', 'enterprise_controller@update');
        Route::post('enterprise_save','enterprise_controller@store');
        Route::get('enterprise_show_all', 'enterprise_controller@enterprise_show_all');
        Route::post('enterprise_search', 'enterprise_controller@enterprise_search');
        Route::post('enterprise_delete', 'enterprise_controller@destroy');

        Route::get('fetch_enterprise_name', 'enterprise_controller@fetch_enterprise_name');
        Route::get('fetch_duns_number_name', 'enterprise_controller@fetch_duns_number_name');


        //route ends

        //Routes for user master

        Route::get('/create_role', function()
        {
            return View::make('user/create_role');
        });

        Route::get('get_right', array(
            'uses' => 'user_controller@get_right',
            'as' => 'get_right'
        ));

        Route::post('insert_role', array(
            'uses' => 'user_controller@save_role',
            'as' => 'save_role'
        ));

        Route::get('/create_userGroup', function()
        {
            return View::make('user/create_userGroup');
        });

        Route::get('get_role', array(
            'uses' => 'user_controller@get_role',
            'as' => 'get_role'
        ));

        Route::get('get_right_for_role', array(
            'uses' => 'user_controller@get_right_for_role',
            'as' => 'get_right_for_role'
        ));

        Route::post('insert_usergrp', array(
            'uses' => 'user_controller@save_usergroup',
            'as' => 'save_usergroup'
        ));

        Route::get('/create_user', function()
        {
            return View::make('user/create_user');
        });

        // Route::get('get_user_group', array(
        //     'uses' => 'user_controller@get_user_group',
        //     'as' => 'parent_results'
        // ));

        Route::get('get_role_by_groupId', array(
            'uses' => 'user_controller@get_role_by_groupId',
            'as' => 'parent_results'
        ));
        Route::post('insert_user', array(
            'uses' => 'user_controller@save_user',
            'as' => 'parent_results'
        ));
        Route::get('/display_roles', function()
        {
            return View::make('user/display_roles');
        });

        Route::get('/display_user', function()
        {
            return View::make('user/display_user');
        });

        Route::get('/change_user', function()
        {
            return View::make('user/change_user');
        });

        Route::get('get_user', array(
            'uses' => 'user_controller@get_user',
            'as' => 'get_user'
        ));
        Route::get('/display_user_group', function()
        {
            return View::make('user/display_user_group');
        });
        Route::get('get_user_group', array(
            'uses' => 'user_controller@get_user_group',
            'as' => 'parent_results'
        ));


        Route::get('fetch_user_id', 'user_controller@fetch_user_id');
        Route::get('fetch_first_name', 'user_controller@fetch_first_name');
        Route::get('fetch_last_name', 'user_controller@fetch_last_name');
        Route::post('user_display_show', 'user_controller@show');

        Route::post('user_update', 'user_controller@update');
        Route::post('user_delete', 'user_controller@destroy');

        Route::post('validate_user_name', array(
            'uses' => 'user_controller@validate_user_name',
            'as' => 'validate_user_name'
        ));

        Route::post('validate_user_name_change', array(
            'uses' => 'user_controller@validate_user_name_change',
            'as' => 'validate_user_name_change'
        ));
        //User end
        //task starts

        Route::get('task_create',function()
        {
         return View::make('task/task_create');
        });
        Route::get('task_change',function()
        {
         return View::make('task/task_change');
        });

        Route::get('task_display',function()
        {
         return View::make('task/task_display');
        });

        Route::get('task_type', array(
            'uses' => 'task_controller@return_task_type',
            'as' => 'task_type'
        ));

        // Route::get('get_program_name', array(
        //     'uses' => 'task_controller@return_program_name',
        //     'as' => 'get_program_name'
        // ));
        Route::post('task_get_program_for_customer_and_type', array(
            'uses' => 'task_controller@task_get_program_for_customer_and_type',
            'as' => 'task_get_program_for_customer_and_type'
        ));

        Route::post('task_get_plant_for_customer', array(
            'uses' => 'task_controller@task_get_plant_for_customer',
            'as' => 'task_get_plant_for_customer'
        ));

        Route::post('task_get_portal_for_plant', array(
            'uses' => 'task_controller@task_get_portal_for_plant',
            'as' => 'task_get_portal_for_plant'
        ));

        Route::post('task_insert', array(
            'uses' => 'task_controller@task_insert',
            'as' => 'task_insert'
        ));

        Route::post('task_report_insert', array(
            'uses' => 'task_controller@task_report_insert',
            'as' => 'task_report_insert'
        ));

        Route::get('task_results', array(
            'uses' => 'task_controller@show_all_task',
            'as' => 'task_results'
        ));

        Route::post('task_search', array(
            'uses' => 'task_controller@task_search',
            'as' => 'task_search'
        ));

        Route::post('task_delete', 'task_controller@destroy');
        Route::post('delete_ds_ref_task', 'task_controller@delete_ds_ref_task');
        Route::post('task_update', 'task_controller@update');


        Route::get('task_controller/autocomplete_task_type', 'task_controller@autocomplete_task_type');
        Route::get('task_controller/autocomplete_task_name', 'task_controller@autocomplete_task_name');


        Route::get('task_status', array(
            'uses' => 'task_controller@get_task_status',
            'as' => 'task_status'
        ));

        Route::get('communication_options', array(
            'uses' => 'task_controller@get_communication_options',
            'as' => 'communication_options'
        ));

        Route::get('mail_address', array(
            'uses' => 'task_controller@get_mail_address',
            'as' => 'mail_address'
        ));

        Route::get('group_mail_address', array(
            'uses' => 'task_controller@get_group_mail_address',
            'as' => 'group_mail_address'
        ));

        Route::post('distribution_strategy', array(
            'uses' => 'task_controller@distribution_strategy_insert',
            'as' => 'distribution_strategy'
        ));

        Route::post('ds_results', array(
            'uses' => 'task_controller@show_all_ds',
            'as' => 'ds_results'
        ));

        Route::post('get_mail_id', array(
            'uses' => 'task_controller@get_mail_id',
            'as' => 'get_mail_id'
        ));

        Route::post('task_ds_ref', array(
            'uses' => 'task_controller@task_ds_ref',
            'as' => 'task_ds_ref'
        ));

        Route::post('ds_delete', array(
            'uses' => 'task_controller@ds_delete',
            'as' => 'ds_delete'
        ));


        Route::post('ds_clear', array(
            'uses' => 'task_controller@ds_clear',
            'as' => 'ds_clear'
        ));

        Route::get('get_current_ds_val', array(
            'uses' => 'task_controller@get_current_ds_val',
            'as' => 'get_current_ds_val'
        ));


        Route::get('product_administration',function()
        {
         return View::make('product_administration/product_administration');
        });
        //task end

        //Report Routes starts
        Route::get('report_gm', function()
        {
            return View::make('reports/report_gm');
        });

        Route::get('get_bunit', array(
            'uses' => 'gm_report_controller@show',
            'as' => 'get_bunit'
        ));

        Route::get('get_gmreport_master', array(
            'uses' => 'gm_report_controller@get_gmreport_master',
            'as' => 'get_gmreport_master'
        ));

        Route::post('gmreport_master', array(
            'uses' => 'gm_report_controller@gmreport_master',
            'as' => 'gmreport_master'
        ));

        Route::post('get_plantdata', array(
            'uses' => 'gm_report_controller@get_plantdata',
            'as' => 'get_plantdata'
        ));

        Route::post('generate_pdf', array(
            'uses' => 'gm_report_controller@generate_pdf',
            'as' => 'generate_pdf'
        ));

        Route::post('get_fca_plantdata', array(
            'uses' => 'gm_report_controller@get_fca_plantdata',
            'as' => 'get_fca_plantdata'
        ));

        Route::post('get_ford_plantdata', array(
            'uses' => 'gm_report_controller@get_ford_plantdata',
            'as' => 'get_ford_plantdata'
        ));

        Route::post('get_psa_plantdata', array(
            'uses' => 'gm_report_controller@get_psa_plantdata',
            'as' => 'get_psa_plantdata'
        ));
        //Report Routes End

        //task end


        //Communication
        Route::get('create_mail_connection', function()
        {
            return View('communication/mail/create_mail_connection');
        });
        Route::get('change_mail_connection', function()
        {
            return View('communication/mail/change_mail_connection');
        });
        Route::get('display_mail_connection', function()
        {
            return View('communication/mail/display_mail_connection');
        });
        Route::post('save_communication', array(
            'uses' => 'communication_controller@save_communication',
            'as' => 'save_communication'
        ));
        Route::get('get_connection_details', array(
            'uses' => 'communication_controller@get_connection_details',
            'as' => 'get_connection_details'
        ));
        Route::post('connection_search', array(
            'uses' => 'communication_controller@connection_search',
            'as' => 'connection_search'
        ));

        Route::post('connection_delete', 'communication_controller@delete_connection');
        Route::post('update_connection', 'communication_controller@update_connection');
        Route::get('communication_controller/autocomplete_connection_name', 'communication_controller@autocomplete_connection_name');
        Route::get('communication_controller/autocomplete_protocol', 'communication_controller@autocomplete_protocol');
        Route::get('communication_controller/autocomplete_server', 'communication_controller@autocomplete_server');

        Route::get('create_system_user', function()
        {
            return View('communication/mail/create_system_user');
        });
        Route::get('change_system_user', function()
        {
            return View('communication/mail/change_system_user');
        });
        Route::get('display_system_user', function()
        {
            return View('communication/mail/display_system_user');
        });
        Route::get('get_connection', array(
            'uses' => 'communication_controller@get_connection',
            'as' => 'get_connection'
        ));
        Route::post('save_system_user', array(
            'uses' => 'communication_controller@save_system_user',
            'as' => 'save_system_user'
        ));
        Route::get('get_system_user_details', array(
            'uses' => 'communication_controller@get_system_user_details',
            'as' => 'get_system_user_details'
        ));

        Route::get('communication_controller/autocomplete_system_user_name', 'communication_controller@autocomplete_system_user_name');
        Route::get('communication_controller/autocomplete_email_id', 'communication_controller@autocomplete_email_id');
        Route::get('communication_controller/autocomplete_smtp_user_name', 'communication_controller@autocomplete_smtp_user_name');
        Route::post('update_system_user', 'communication_controller@update_system_user');
        Route::post('delete_system_user', 'communication_controller@delete_system_user');

        Route::post('search_system_user', array(
            'uses' => 'communication_controller@search_system_user',
            'as' => 'search_system_user'
        ));

        Route::get('create_ftp_connection', function()
        {
            return View('communication/ftp/create_ftp_connection');
        });
        Route::get('change_ftp_connection', function()
        {
            return View('communication/ftp/change_ftp_connection');
        });
        Route::get('display_ftp_connection', function()
        {
            return View('communication/ftp/display_ftp_connection');
        });

        Route::post('save_ftp_connection',array(
          'uses' => 'communication_controller@save_ftp_connection',
          'as' => 'save_ftp_connection'
        ));

        Route::get('get_ftp_connection_details',array(
          'uses' =>'communication_controller@get_ftp_connection_details',
          'as'=>'get_ftp_connection_details'
        ));
        Route::post('ftp_connection_search',array(
          'uses'=>'communication_controller@ftp_connection_search',
          'as'=>'ftp_connection_search'
        ));

        Route::get('communication_controller/autocomplete_ftp_server_type','communication_controller@autocomplete_ftp_server_type');
        Route::get('communication_controller/autocomplete_ftp_connection_name','communication_controller@autocomplete_ftp_connection_name');
        Route::get('communication_controller/autocomplete_ftp_host','communication_controller@autocomplete_ftp_host');
        Route::post('ftp_connection_delete','communication_controller@ftp_connection_delete');

        Route::post('update_ftp_connection',array(
          'uses'=>'communication_controller@update_ftp_connection',
          'as'=>'update_ftp_connection'
        ));

        Route::get('create_ftp_address',function(){
          return view('communication/ftp/create_ftp_address');
        });
        Route::get('change_ftp_address',function(){
          return view('communication/ftp/change_ftp_address');
        });
        Route::get('display_ftp_address',function(){
          return view('communication/ftp/display_ftp_address');
        });
        Route::post('save_ftp_address',array(
          'uses'=>'communication_controller@save_ftp_address',
          'as'=>'save_ftp_address'
        ));
        Route::get('get_ftp_connection',array(
          'uses'=>'communication_controller@get_ftp_connection',
          'as'=>'get_ftp_connection'
        ));

        Route::get('get_ftp_address_details',array(
          'uses' => 'communication_controller@get_ftp_address_details',
          'as' => 'get_ftp_address_details'
        ));

        Route::post('update_ftp_addresses',array(
          'uses'=>'communication_controller@update_ftp_addresses',
          'as'=>'update_ftp_addresses'
        ));

        Route::post('ftp_address_delete', 'communication_controller@ftp_address_delete');
        Route::get('communication_controller/autocomplete_address_name','communication_controller@autocomplete_address_name');
        Route::get('communication_controller/autocomplete_search_user_name','communication_controller@autocomplete_search_user_name');


        Route::get('get_connection_for_address',array(
          'uses'=>'communication_controller@get_connection_for_address',
          'as'=>'get_connection_for_address'
        ));

        Route::post('ftp_address_search',array(
          'uses'=>'communication_controller@ftp_address_search',
          'as'=>'ftp_address_search'
        ));

        Route::get('create_enterprise_users',function(){
          return view('communication/mail/create_enterprise_users');
        });
        Route::get('change_enterprise_users',function(){
          return view('communication/mail/change_enterprise_users');
        });
        Route::get('display_enterprise_users',function(){
          return view('communication/mail/display_enterprise_users');
        });
        Route::get('get_enterprise_user_group',array(
          'uses'=>'communication_controller@get_enterprise_user_group',
          'as'=>'get_enterprise_user_group'
        ));
        Route::post('user_group_insert',array(
          'uses'=>'communication_controller@user_group_insert',
          'as'=>'user_group_insert'
        ));
        Route::post('user_insert',array(
          'uses'=>'communication_controller@user_insert',
          'as'=>'user_insert'
        ));

        Route::get('user_results',array(
          'uses'=>'communication_controller@user_results',
          'as'=>'user_results'
        ));
        Route::get('usergroup_results',array(
          'uses'=>'communication_controller@usergroup_results',
          'as'=>'user_results'
        ));

        Route::post('enterprise_user_search',array(
          'uses'=>'communication_controller@enterprise_user_search',
          'as'=>'enterprise_user_search'
        ));

        Route::get('communication_controller/autocomplete_search_ent_email_id','communication_controller@autocomplete_search_ent_email_id');
        Route::get('communication_controller/autocomplete_search_ent_user_group_id','communication_controller@autocomplete_search_ent_user_group_id');
        Route::get('communication_controller/autocomplete_search_ent_user_name','communication_controller@autocomplete_search_ent_user_name');
        Route::post('delete_enterprise_user', 'communication_controller@delete_enterprise_user');

        Route::post('update_enterprise_user',array(
          'uses'=>'communication_controller@update_enterprise_user',
          'as'=>'update_enterprise_user'
        ));

        //monitor
        Route::get('process_monitor', function()
        {
            return View('monitor/process_monitor');
        });

        Route::get('get_process_monitor_details',array(
          'uses' => 'monitor_controller@get_process_monitor_details',
          'as' => 'get_process_monitor_details'
        ));
        Route::get('get_process_monitor_description',array(
          'uses' => 'monitor_controller@get_process_monitor_description',
          'as' => 'get_process_monitor_description'
        ));
             

      });
      
       Route::get('/report', 'chart_controller@showchart');
       Route::post('/getchart', 'chart_controller@getchart');
       
       
				   Route::get('report_generator', function()
				{
				    return View('report_generator/report_generator');
				});
				
				//url for plants users / corporate users
				Route::get('report_generator_screen', function()
				{
				    return View('report_generator_plant_users/report_generator');
				});
				
				Route::get('get_report_name_by_report_type', array(
				    'uses' => 'report_generator_controller@get_report_name_by_report_type',
				    'as' => 'get_report_name_by_report_type'
				));
				
				Route::get('get_product_group_by_business_unit', array(
				    'uses' => 'report_generator_controller@get_product_group_by_business_unit',
				    'as' => 'business_unit'
				));
				
				Route::get('get_plant_name_by_product_group', array(
				    'uses' => 'report_generator_controller@get_plant_name_by_product_group',
				    'as' => 'get_plant_name_by_product_group'
				));
				
				Route::get('get_cust_name_by_cust_type', array(
				    'uses' => 'report_generator_controller@get_cust_name_by_cust_type',
				    'as' => 'get_cust_name_by_cust_type'
				));
				Route::get('get_custType_custName_byReportType', array(
				    'uses' => 'report_generator_controller@get_custType_custName_byReportType',
				    'as' => 'get_custType_custName_byReportType'
				));
				Route::get('get_report_type', array(
				    'uses' => 'report_generator_controller@get_report_type',
				    'as' => 'business_unit'
				));
				Route::get('get_business_unit', array(
				    'uses' => 'report_generator_controller@get_business_unit',
				    'as' => 'business_unit'
				));
				Route::get('get_product_group', array(
				    'uses' => 'report_generator_controller@get_product_group',
				    'as' => 'product_group'
				));
				Route::get('get_plant_name', array(
				    'uses' => 'report_generator_controller@get_plant_name',
				    'as' => 'plant_name'
				));
				
				Route::get('get_plant_status', array(
				    'uses' => 'report_generator_controller@get_plant_status',
				    'as' => 'get_plant_status'
				));
				
				Route::get('get_cust_type', array(
				    'uses' => 'report_generator_controller@get_cust_type',
				    'as' => 'cust_type'
				));
				Route::get('get_cust_name', array(
				    'uses' => 'report_generator_controller@get_cust_name',
				    'as' => 'cust_name'
				));
				Route::get('get_variant_name', array(
				    'uses' => 'report_generator_controller@get_variant_name',
				    'as' => 'get_variant_name'
				));
				Route::get('get_selected_variant_name', array(
				    'uses' => 'report_generator_controller@get_selected_variant_name',
				    'as' => 'get_selected_variant_name'
				));
				Route::post('save_varient', array(
				    'uses' => 'report_generator_controller@save_varient',
				    'as' => 'parent_results'
				));
				Route::get('get_report_type', array(
				    'uses' => 'report_generator_controller@get_report_type',
				    'as' => 'get_report_type'
				));
				
				
				Route::get('get_report_name', array(
				    'uses' => 'report_generator_controller@get_report_name',
				    'as' => 'get_report_name'
				));
				Route::get('get_variant_details', array(
				    'uses' => 'report_generator_controller@get_variant_details',
				    'as' => 'get_variant_details'
				));
				Route::get('get_bu_by_report_type', array(
				    'uses' => 'report_generator_controller@get_bu_by_report_type',
				    'as' => 'get_bu_by_report_type'
				));
				Route::get('get_plant_by_report_type', array(
				    'uses' => 'report_generator_controller@get_plant_by_report_type',
				    'as' => 'get_plant_by_report_type'
				));
				Route::get('get_pg_by_report_type', array(
				    'uses' => 'report_generator_controller@get_pg_by_report_type',
				    'as' => 'get_pg_by_report_type'
				));
				Route::get('get_bu_by_reportID', array(
				    'uses' => 'report_generator_controller@get_bu_by_reportID',
				    'as' => 'get_bu_by_reportID'
				));
				Route::get('get_pg_by_reportID', array(
				    'uses' => 'report_generator_controller@get_pg_by_reportID',
				    'as' => 'get_pg_by_reportID'
				));
				Route::get('get_plant_by_reportID', array(
				    'uses' => 'report_generator_controller@get_plant_by_reportID',
				    'as' => 'get_plant_by_reportID'
				));
				Route::get('get_custType_by_reportID', array(
				    'uses' => 'report_generator_controller@get_custType_by_reportID',
				    'as' => 'get_custType_by_reportID'
				));
				Route::get('get_customer_by_reportID', array(
				    'uses' => 'report_generator_controller@get_customer_by_reportID',
				    'as' => 'get_customer_by_reportID'
				));
				Route::get('get_plant_status_by_reportID', array(
				    'uses' => 'report_generator_controller@get_plant_status_by_reportID',
				    'as' => 'get_plant_status_by_reportID'
				));
				
				Route::get('get_report_name_by_reportID', array(
				    'uses' => 'report_generator_controller@get_report_name_by_reportID',
				    'as' => 'get_report_name_by_reportID'
				));
				
				Route::post('delete_variant', 'report_generator_controller@destroy');
				Route::post('update_varient', 'report_generator_controller@update');
				
				
				//Schedule Reports
				Route::get('create_schedule_report', function()
				{
				    return View('report_generator/create_schedule_report');
				});
				Route::get('change_schedule_report', function()
				{
				    return View('report_generator/change_schedule_report');
				});
				Route::get('display_schedule_report', function()
				{
				    return View('report_generator/display_schedule_report');
				});
				
				//Schedule Reports for plant users
				Route::get('create_schedule_report_screen', function()
				{
				    return View('report_generator_plant_users/create_schedule_report');
				});
				Route::get('change_schedule_report_screen', function()
				{
				    return View('report_generator_plant_users/change_schedule_report');
				});
				Route::get('display_schedule_report_screen', function()
				{
				    return View('report_generator_plant_users/display_schedule_report');
				});
				
				
				Route::get('get_start_conditions_for_scheduler_reports', array(
				    'uses' => 'report_generator_controller@get_start_conditions_for_scheduler',
				    'as' => 'get_start_conditions_for_scheduler_reports'
				));
				Route::get('frequency_for_scheduler_report', array(
				    'uses' => 'report_generator_controller@get_frequency',
				    'as' => 'frequency_for_scheduler_report'
				));
				Route::POST('scheduler_save_report', array(
				    'uses' => 'report_generator_controller@save_schedule_reports',
				    'as' => 'scheduler_save_report'
				));
				Route::post('get_frequency_for_scheduler_report', array(
				    'uses' => 'report_generator_controller@get_frequency_for_scheduler',
				    'as' => 'get_frequency_for_scheduler_report'
				));
				Route::post('get_after_job_for_scheduler_report', array(
				    'uses' => 'report_generator_controller@get_after_job_for_scheduler',
				    'as' => 'get_after_job_for_scheduler_report'
				));
				Route::get('get_jobs_reports', array(
				    'uses' => 'report_generator_controller@get_jobs',
				    'as' => 'get_jobs_reports'
				));
				Route::get('get_jobs_for_scheduler_reports', array(
				    'uses' => 'report_generator_controller@get_jobs_for_scheduler_reports',
				    'as' => 'get_jobs_for_scheduler_reports'
				));
				Route::get('get_variant_for_scheduler', array(
				    'uses' => 'report_generator_controller@get_variant_for_scheduler',
				    'as' => 'get_variant_for_scheduler'
				));
				Route::get('get_variant', array(
				    'uses' => 'report_generator_controller@get_variant',
				    'as' => 'get_variant'
				));
				Route::post('scheduler_update_report', array(
				    'uses' => 'report_generator_controller@scheduler_update',
				    'as' => 'scheduler_update_report'
				));
				
				
				Route::get('fetch_job_name_report', 'report_generator_controller@fetch_job_name_report');
				
				// Route::get('fetch_job_name', 'report_generator_controller@fetch_job_name');
				
				Route::get('fetch_variant', 'report_generator_controller@fetch_variant');
				Route::post('job_display_show_report', 'report_generator_controller@show');
				
				Route::post('scheduler_delete_report', array(
				    'uses' => 'report_generator_controller@delete_job',
				    'as' => 'scheduler_delete_report'
				));
				
				// Cross refernce tables
				Route::get('plant_customer_code_change', 'plant_customer_code_controller@plant_customer_code_view_change');
				Route::get('get_plant_customer_code', 'plant_customer_code_controller@get_all_plant_customer_code');
				Route::post('update_plant_customer_code', 'plant_customer_code_controller@update_plant_customer_code');
				Route::get('plant_customer_code_display', 'plant_customer_code_controller@plants_customer_code_view_display');
				
				
				//
				Route::get('help_page_search', function()
				{
				    return View::make('product_administration/help_page');
				});

     
});
