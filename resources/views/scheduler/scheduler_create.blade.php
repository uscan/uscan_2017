@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/scheduler.css">
<script src="js/scheduler_validation.js"></script>
@stop
@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="display-title">
      <p>Create Scheduler Job</p>
   </div>
</div>
@stop
@section('content')
<div class="col-xs-12 content">
   <div id="tab_one">
      <form id="scheduler_create_form" autocomplete="off">
         <table style="width:30%;">
            <tr>
               <td style="background-color: #eee;"><strong>Basic Details</strong></td>
               <td></td>
            </tr>
            <tr>
               <td></td>
            </tr>
            <tr>
               <td>Scheduler Job Name</td>
               <td><input type="text" id="job_name" name="job_name"></td>
            </tr>
            <tr>
               <td>Task Type</td>
               <td><select id = "task_type" name="task_type">
                  </select>
               </td>
            </tr>
            <tr>
               <td>Task Name</td>
               <td><select id = "task_name" name="task_name" >
                  </select>
               </td>
            </tr>
            <tr>
               <td>Job Priority</td>
               <td>
                  <select id = "job_priority" name="job_priority" disabled>
                     <option id="0" value="0">Select Task Name</option>
                     <option id="1" value="1">High</option>
                     <option id="2" value="2">Medium</option>
                     <option id="3" value="3">Low</option>
                  </select>
               </td>
            </tr>
            <tr>
               <td>Active</td>
               <td><input type="checkbox" name="active" id="active"></td>
            </tr>
            <tr>
               <td></td>
            </tr>
         </table>
      </form>
      <div id="start_conditions_div" class="">
         <form id="start_conditions_form" class="">
            <table style="width:30%;">
               <tr>
                  <td style="background-color: #eee;"><strong>Start Conditions</strong></td>
                  <td></td>
               </tr>
               <tr>
                  <td>Start Conditions</td>
                  <td><select id = "start_conditions" name="start_conditions">
                     </select>
                  </td>
               </tr>
            </table>
         </form>
      </div>
      <div class="load" id="immediate">
         <form id="immediate_form" class="">
            <table style="width:30%;">
               <tr>
                  <td>Periodic</td>
                  <td><input type="checkbox" id="period_immediately" name="period_immediately" value=""></td>
               </tr>
               <tr>
                  <td>Frequency</td>
                  <td><select id="frequency" class="" name="frequency">
                     </select>
                  </td>
               </tr>
            </table>
         </form>
      </div>
      <div id="date_time_div" class="load">
         <form id="date_time_form" class="date_time_form">
            <table style="width:60%;">
               <tr>
                  <td>
                     Start Date
                  </td>
                  <td><input type="date" name="start_date" id="start_date"></td>
                  <td>
                     End Date
                  </td>
                  <td><input type="date" name="end_date" id="end_date"></td>
               </tr>
               <tr>
                  <td>Start Time</td>
                  <td><input type="time" name="start_time" id="start_time"></td>
                  <td>End Time</td>
                  <td><input type="time" name="end_time" id="end_time"></td>
               </tr>
               <tr>
                  <td>Periodic</td>
                  <td><input type="checkbox" name="periodic_date_time" id="periodic_date_time"></td>
               </tr>
            </table>
         </form>
         <form id="date_time_form_frequency">
            <table style="width:30%;">
               <tr>
                  <td id="frequency_label">Frequency</td>
                  <td><select id ="frequency_date_time" name="frequency_date_time">
                     </select>
                  </td>
               </tr>
            </table>
         </form>
      </div>
      <div id="after_job_div" class="load">
         <form id="after_job_from" class="after_job_from">
            <table style="width:30%;">
               <tr>
                  <td>Scheduler Job Name</td>
                  <td>
                     <select id = "predecessor_job_name" name="predecessor_job_name">
                     </select>
                  </td>
               </tr>
            </table>
         </form>
      </div>
   </div>

   <script type="text/javascript">
    jQuery.validator.addMethod('selectcheck', function(value) {
        return (value != '0');
    }, "");
    $(document).ready(function() {
        //on first time load start
        var active = 0;
        var period_immediately = 0;
        var start_conditions = 0;
        var periodic_date_time = 0;
        var frequency_date_time = 0;
        $("#set_up").trigger('click');
        $("#scheduler").trigger('click');

        task_type_for_scheduler();
        start_conditions_for_scheduler();

        function task_type_for_scheduler() {
            $.ajax({
                type: "GET",
                url: "task_type_for_scheduler",
                dataType: "json",
                success: function(data) {
                    $('#task_type').html('');
                    var results = data;
                    console.log(JSON.stringify(data));
                    for (var i = 0; i < results.length; i++) {
                        if (i == '0') {
                            $('#task_type').append("<option id =\"0\" value=\"0\">Select</option>");
                            $('#task_name').append("<option id =\"0\" value=\"0\">Select Task Type</option>");
                        }
                        var task_type_name = results[i].task_type_name
                        var task_type_id = results[i].task_type_id
                        $('#task_type').append("<option id =" + task_type_id + " value=" + task_type_id + ">" + task_type_name + "</option>");
                    }
                },
                beforeSend: function() {},
                error: function() {}
            });
        }

        $('#task_type').change(function() {
            var task_type_id = $('#task_type').val();
            get_task_name(task_type_id);
        });

        $('#task_name').change(function() {
            var task_name_id = $('#task_name').val();
            if (task_name_id == 0) {
                $("#job_priority").prop('disabled', true);
                $('#job_priority').val(0);
            } else {
                $("#job_priority").prop('disabled', false);
            }
        });

        function get_task_name(task_type_id) {
            $.ajax({
                type: "POST",
                url: "task_name_for_scheduler",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "task_type_id": task_type_id,
                },
                success: function(data) {
                    var results = data;
                    console.log(JSON.stringify(data));
                    $('#task_name').html('');
                    if (results.length == 0) {
                        $("#job_priority").prop('disabled', true);
                        $('#task_name').append("<option id =\"0\" value=\"0\">Task Doesn't exist</option>");
                    } else {
                        $("#job_priority").prop('disabled', false);
                        for (var i = 0; i < results.length; i++) {
                            if (i == '0') {
                                $('#task_name').append("<option id =\"0\" value=\"0\">Select</option>");
                            }
                            var task_name = results[i].task_name
                            var task_id = results[i].task_id
                            $('#task_name').append("<option id =" + task_id + " value=" + task_id + ">" + task_name + "</option>");
                        }
                    }
                },
                beforeSend: function() {},
                error: function() {}
            });
        }

        function frequency_for_scheduler_date_time() {
            $.ajax({
                type: "GET",
                url: "frequency_for_scheduler",
                dataType: "json",
                success: function(data) {
                  $('#frequency_date_time').html('');
                    var results = data;
                    console.log(JSON.stringify(data));
                    for (var i = 0; i < results.length; i++) {
                        if (i == '0') {
                            $('#frequency_date_time').append("<option id =\"0\" value=\"0\">Select</option>");
                        }
                        var frequency_name = results[i].frequency_name
                        var frequency_id = results[i].frequency_id
                        $('#frequency_date_time').append("<option id =" + frequency_id + " value=" + frequency_id + ">" + frequency_name + "</option>");
                    }
                },
                beforeSend: function() {},
                error: function() {}
            });
        }

        function frequency_for_scheduler_immediately() {
            $.ajax({
                type: "GET",
                url: "frequency_for_scheduler",
                dataType: "json",
                success: function(data) {
                  $('#frequency').html('');
                    var results = data;
                    console.log(JSON.stringify(data));
                    for (var i = 0; i < results.length; i++) {
                        if (i == '0') {
                            $('#frequency').append("<option id =\"0\" value=\"0\">Select</option>");
                        }
                        var frequency_name = results[i].frequency_name
                        var frequency_id = results[i].frequency_id
                        $('#frequency').append("<option id =" + frequency_id + " value=" + frequency_id + ">" + frequency_name + "</option>");
                    }
                },
                beforeSend: function() {},
                error: function() {}
            });
        }

        function start_conditions_for_scheduler() {
            $.ajax({
                type: "GET",
                url: "start_conditions_for_scheduler",
                dataType: "json",
                success: function(data) {
                    $('#start_conditions').html('');
                    var results = data;
                    console.log(JSON.stringify(data));
                    for (var i = 0; i < results.length; i++) {
                        if (i == '0') {
                            $('#start_conditions').append("<option id =\"0\" value=\"0\">Select</option>");
                        }
                        var start_type_name = results[i].start_type_name
                        var start_type_id = results[i].start_type_id
                        $('#start_conditions').append("<option id =" + start_type_id + " value=" + start_type_id + ">" + start_type_name + "</option>");
                    }
                },
                beforeSend: function() {},
                error: function() {}
            });
        }

        $('#period_immediately').on('change', function() {
            if (jQuery(this).is(":checked")) {
                $("#frequency").prop('disabled', false);
            } else {
                $('#frequency').val(0);
                $("#frequency").prop('disabled', true);
            }
        });

        $('#periodic_date_time').on('change', function() {
            if (jQuery(this).is(":checked")) {
                $("#frequency_date_time").prop('disabled', false);
            } else {
                $('#frequency_date_time').val(0);
                $("#frequency_date_time").prop('disabled', true);
            }
        });

        $('#start_conditions').on('change', function() {
            if (this.value == 1) {
                frequency_for_scheduler_immediately();
                $('#immediate').hide();
                $('#date_time_div').hide();
                $('#after_job_div').hide();
                $("#frequency").prop('disabled', true);
            } else if (this.value == 2) {
                frequency_for_scheduler_date_time();
                $('#immediate').hide();
                $('#date_time_div').show();
                $('#after_job_div').hide();
                $("#frequency_date_time").prop('disabled', true);
            } else if (this.value == 3) {
                get_jobs();
                $('#immediate').hide();
                $('#date_time_div').hide();
                $('#after_job_div').show();

            }
        });

        function get_jobs() {
            $.ajax({
                type: "GET",
                url: "get_jobs_for_scheduler",
                dataType: "json",
                success: function(data) {
                  $('#predecessor_job_name').html('');
                    var results = data;
                    console.log(JSON.stringify(data));
                    for (var i = 0; i < results.length; i++) {
                        if (i == '0') {
                            $('#predecessor_job_name').append("<option id =\"0\" value=\"0\">Select</option>");
                        }
                        var job_name = results[i].job_name
                        var job_id = results[i].job_id
                        $('#predecessor_job_name').append("<option id =" + job_id + " value=" + job_id + ">" + job_name + "</option>");
                    }
                },
                beforeSend: function() {},
                error: function() {}
            });
        }

        $(document).on('click', '#save_button', function() {
            start_conditions = $("#start_conditions option:selected").attr('id');
            if ($('#scheduler_create_form').valid()) {
                if ($('#start_conditions_form').valid()) {
                    if (start_conditions == "1") {
                        if ($('#period_immediately').is(":checked")) {
                            if ($('#immediate_form').valid()) {
                                save_msg(start_conditions);
                            }
                        } else {
                            save_msg(start_conditions);
                        }
                    } else if (start_conditions == "2") {
                        if ($('#periodic_date_time').is(":checked")) {
                            if ($('#date_time_form_frequency').valid()) {
                                save_msg(start_conditions);
                            }
                        } else {
                            if ($('#date_time_form').valid()) {
                                save_msg(start_conditions);
                            }
                        }

                    } else if (start_conditions == "3") {
                        if ($('#after_job_from').valid()) {
                            save_msg(start_conditions);
                        }
                    }
                }
            }

        });

        function save_msg(start_conditions) {
            $.msgBox({
                title: "Confirm",
                content: "Do you want to save the information?",
                type: "confirm",
                buttons: [{
                    value: "Yes"
                }, {
                    value: "No"
                }],
                success: function(result) {
                    if (result == "Yes") {
                        save(start_conditions);
                    } else {

                    }
                }
            });
        }

        function save(start_conditions) {

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }
            today = yyyy + '-' + mm + '-' + dd;

            var dt = new Date();
            var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
            var add_one_min_for_immediate = dt.getMinutes() + 1;

            var start_time_for_immediate = dt.getHours() + ":" + add_one_min_for_immediate + ":" + dt.getSeconds();
            // alert(start_time_for_immediate);

            var job_name = $('#job_name').val();
            var task_type = $("#task_type option:selected").attr('id');
            var task_name = $("#task_name option:selected").attr('id');
            var job_priority = $("#job_priority option:selected").attr('id');

            var frequency = 0;

            if ($('#active').is(":checked")) {
                active = 1;
            } else {
                active = 0;
            }

            if (start_conditions == "1") {
                var job_start_date = today;
                var job_end_date = today;
                var job_last_run_date = today;
                var job_start_time = start_time_for_immediate;
                var job_end_time = start_time_for_immediate;
                var job_last_run_time = start_time_for_immediate;
                if ($('#period_immediately').is(":checked")) {
                    period_immediately = 1;
                    var frequency = $("#frequency option:selected").attr('id');
                } else {
                    period_immediately = 0;
                    var frequency = 0;
                }
            } else if (start_conditions == "2") {
                var job_start_date = $('#start_date').val();
                var job_end_date = $('#end_date').val();
                var job_last_run_date = $('#start_date').val();

                var job_start_time = $('#start_time').val();
                var job_end_time = $('#end_time').val();
                var job_last_run_time = $('#start_time').val();

                if ($('#periodic_date_time').is(":checked")) {
                    var periodic_date_time = 1;
                    var frequency_date_time = $("#frequency_date_time option:selected").attr('id');
                } else {
                    var periodic_date_time = 0;
                    var frequency_date_time = 0;
                }
            } else if (start_conditions == "3") {
                var job_start_date = today;
                var job_end_date = today;
                var job_last_run_date = today;
                var job_start_time = time;
                var job_end_time = time;
                var job_last_run_time = time;

                var after_job = $("#predecessor_job_name option:selected").attr('id');
                //var predecessor_job_name = $("#predecessor_job_name option:selected").attr('id');
            }

            $.ajax({
                type: "POST",
                url: "scheduler_save",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "job_name": job_name,
                    "task_type": task_type,
                    "task_name": task_name,
                    "job_priority": job_priority,
                    "start_type_id": start_conditions,
                    "active": active,

                    //1.Immediate
                    "period_immediately": period_immediately,
                    "frequency": frequency,

                    //2.date_time
                    "periodic_date_time": periodic_date_time,
                    "frequency_date_time": frequency_date_time,

                    //3. After job
                    "after_job": after_job,
                    //"predecessor_job_name": predecessor_job_name,

                    "job_start_date": job_start_date,
                    "job_end_date": job_end_date,
                    "job_last_run_date": job_last_run_date,

                    "job_start_time": job_start_time,
                    "job_end_time": job_end_time,
                    "job_last_run_time": job_last_run_time,

                },
                success: function(data) {
                    after_save();
                    $.msgBox({
                        title: "Message",
                        content: "Scheduler added successfully",
                        type: "info",
                    });
                },
                beforeSend: function() {
                    $('#msg').show();

                },
                error: function() {}
            });
        }

        function after_save() {
            reset();
        }

        function reset() {
            $('#scheduler_create_form').trigger("reset");
            $('#start_conditions_form').trigger("reset");
            $('#immediate_form').trigger("reset");
            $('#date_time_form').trigger("reset");
            $('#date_time_form_frequency').trigger("reset");
            $('#after_job_from').trigger("reset");
        }

        $('#cancel_button').click(function() {
            reset();
        });

    });
</script>
</div>
@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
   <button id="cancel_button" class="headerbuttons" type="button" style="float: right; margin-right: 20px;">Cancel</button>
   <button id="save_button" class="headerbuttons" type="button"  style="float: right; margin-right: 20px;">Save</button>
</div>
@stop
