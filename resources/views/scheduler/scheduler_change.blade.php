@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/scheduler.css">
<script src="js/scheduler_validation.js"></script>
@stop
@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="display-title">
      Change Scheduler Job
   </div>
   <div id="show_all_link" class="col-xs-2" style="float: right;text-align:center;line-height:30px;">
      <span  id="show_all" style="">Show All Jobs</span>
   </div>
</div>
@stop
@section('content')
<div class="col-xs-12 content">
  <div id="search_page" class="search_page">
    <form id="scheduler_search_form" autocomplete="off">
       <table style="width:30%;">
          <tr>
             <td style="background-color: #eee;"><strong>Basic Details</strong></td>
             <td></td>
          </tr>
          <tr>
             <td></td>
          </tr>
          <tr>
             <td>Scheduler Job Name</td>
             <td><input type="text" id="job_name_search" name="job_name_search"></td>
          </tr>
          <tr>
             <td>Task Type</td>
             <td><input type="text" id="task_type_search" name="task_type_search"></td>
          </tr>
          <tr>
             <td>Task Name</td>
             <td><input type="text" id="task_name_search" name="task_name_search"></td>
          </tr>
       </table>
    </form>
  </div>
  <div class="list_of_jobs" id="list_of_jobs">
    <table style="width:100%;overflow: hidden;" class="tab">
    </table>
  </div>
   <div id="tab_one" class="load">
      <form id="scheduler_create_form" autocomplete="off">
         <table style="width:60%;">
            <tr>
               <td style="background-color: #eee;"><strong>Basic Details</strong></td>
               <td></td>
               <td style="background-color: #eee;width:16%;"><strong>Additional Details</strong></td>
            </tr>
            <tr>
               <td></td>
            </tr>
            <tr>
               <td>Scheduler Job Name</td>
               <td><input type="text" id="job_name" name="job_name"></td>
               <td>Created By</td>
               <td><input type="text" name="created_by" id="created_by" disabled></td>
            </tr>
            <tr>
               <td>Task Type</td>
               <td><select id = "task_type" name="task_type">
                  </select>
               </td>
               <td>Created Date</td>
               <td><input type="text" name="created_date" id="created_date" disabled></td>
            </tr>
            <tr>
               <td>Task Name</td>
               <td><select id = "task_name" name="task_name" >
                  </select>
               </td>
               <td>Modified By</td>
               <td><input type="text" name="modified_by" id="modified_by" disabled></td>
            </tr>
            <tr>
               <td>Job Priority</td>
               <td>
                  <select id = "job_priority" name="job_priority" disabled>
                     <option id="0" value="0">Select Task Name</option>
                     <option id="1" value="1">High</option>
                     <option id="2" value="2">Medium</option>
                     <option id="3" value="3">Low</option>
                  </select>
               </td>
               <td>Modified Date</td>
               <td><input type="text" name="modified_date" id="modified_date" disabled></td>
            </tr>
            <tr>
               <td>Active</td>
               <td><input type="checkbox" name="active" id="active"></td>
            </tr>
            <tr>
               <td></td>
            </tr>
         </table>
      </form>
      <div id="start_conditions_div" class="">
         <form id="start_conditions_form" class="">
            <table style="width:30%;">
               <tr>
                  <td style="background-color: #eee;"><strong>Start Conditions</strong></td>
                  <td></td>
               </tr>
               <tr>
                  <td>Start Conditions</td>
                  <td><select id = "start_conditions" name="start_conditions">
                     </select>
                  </td>
               </tr>
            </table>
         </form>
      </div>
      <div class="load" id="immediate">
         <form id="immediate_form" class="">
            <table style="width:30%;">
               <tr>
                  <td>Periodic</td>
                  <td><input type="checkbox" id="period_immediately" name="period_immediately" value=""></td>
               </tr>
               <tr>
                  <td>Frequency</td>
                  <td><select id="frequency" class="" name="frequency">
                     </select>
                  </td>
               </tr>
            </table>
         </form>
      </div>
      <div id="date_time_div" class="load">
        <form id="date_time_form" class="date_time_form">
           <table style="width:60%;">
              <tr>
                 <td>
                    Start Date
                 </td>
                 <td><input type="date" name="start_date" id="start_date"></td>
                 <td>
                    End Date
                 </td>
                 <td><input type="date" name="end_date" id="end_date"></td>
              </tr>
              <tr>
                 <td>Start Time</td>
                 <td><input type="time" name="start_time" id="start_time"></td>
                 <td>End Time</td>
                 <td><input type="time" name="end_time" id="end_time"></td>
              </tr>
              <tr>
                 <td>Periodic</td>
                 <td><input type="checkbox" name="periodic_date_time" id="periodic_date_time"></td>
              </tr>
           </table>
        </form>
         <form id="date_time_form_frequency">
            <table style="width:30%;">
               <tr>
                  <td id="frequency_label">Frequency</td>
                  <td><select id ="frequency_date_time" name="frequency_date_time">
                     </select>
                  </td>
               </tr>
            </table>
         </form>
      </div>
      <div id="after_job_div" class="load">
         <form id="after_job_from" class="after_job_from">
            <table style="width:30%;">
               <tr>
                  <td>Scheduler Job Name</td>
                  <td>
                     <select id = "predecessor_job_name" name="predecessor_job_name">
                     </select>
                  </td>
               </tr>
            </table>
         </form>
      </div>
   </div>
   <script type="text/javascript">
    jQuery.validator.addMethod('selectcheck', function(value) {
        return (value != '0');
    }, "");
    $(document).ready(function() {
        //on first time load start
        var active = 0;
        var period_immediately = 0;
        var start_conditions = 0;
        var periodic_date_time = 0;
        var frequency_date_time = 0;
        var filter_clicked = false;
        var job_id = "";

        $("#set_up").trigger('click');
        $("#scheduler").trigger('click');

        task_type_for_scheduler();
        start_conditions_for_scheduler();
        get_jobs();

        $.ajax({
            type: "GET",
            url: "get_task_name_for_scheduler",
            dataType: "json",
            success: function(data) {
                var results = data;
                console.log(JSON.stringify(data));
                for (var i = 0; i < results.length; i++) {
                    if (i == '0') {
                        $('#task_name').append("<option id =\"0\" value=\"0\">Select</option>");
                    }
                    var task_name = results[i].task_name
                    var task_id = results[i].task_id
                    $('#task_name').append("<option id =" + task_id + " value=" + task_id + ">" + task_name + "</option>");
                }
            },
            beforeSend: function() {},
            error: function() {}
        });

        $.ajax({
            type: "GET",
            url: "frequency_for_scheduler",
            dataType: "json",
            success: function(data) {
                var results = data;
                console.log(JSON.stringify(data));
                for (var i = 0; i < results.length; i++) {
                    if (i == '0') {
                        $('#frequency').append("<option id =\"0\" value=\"0\">Select</option>");
                    }
                    var frequency_name = results[i].frequency_name
                    var frequency_id = results[i].frequency_id
                    $('#frequency').append("<option id =" + frequency_id + " value=" + frequency_id + ">" + frequency_name + "</option>");
                }
            },
            beforeSend: function() {},
            error: function() {}
        });

        $.ajax({
            type: "GET",
            url: "frequency_for_scheduler",
            dataType: "json",
            success: function(data) {
                var results = data;
                console.log(JSON.stringify(data));
                for (var i = 0; i < results.length; i++) {
                    if (i == '0') {
                        $('#frequency_date_time').append("<option id =\"0\" value=\"0\">Select</option>");
                    }
                    var frequency_name = results[i].frequency_name
                    var frequency_id = results[i].frequency_id
                    $('#frequency_date_time').append("<option id =" + frequency_id + " value=" + frequency_id + ">" + frequency_name + "</option>");
                }
            },
            beforeSend: function() {},
            error: function() {}
        });

        function task_type_for_scheduler() {
            $.ajax({
                type: "GET",
                url: "task_type_for_scheduler",
                dataType: "json",
                success: function(data) {
                    $('#task_type').html('');
                    var results = data;
                    console.log(JSON.stringify(data));
                    for (var i = 0; i < results.length; i++) {
                        if (i == '0') {
                            $('#task_type').append("<option id =\"0\" value=\"0\">Select</option>");
                            $('#task_name').append("<option id =\"0\" value=\"0\">Select Task Type</option>");
                        }
                        var task_type_name = results[i].task_type_name
                        var task_type_id = results[i].task_type_id
                        $('#task_type').append("<option id =" + task_type_id + " value=" + task_type_id + ">" + task_type_name + "</option>");
                    }
                },
                beforeSend: function() {},
                error: function() {}
            });
        }

        $('#task_type').change(function() {
            var task_type_id = $('#task_type').val();
            get_task_name(task_type_id);
        });

        $('#task_name').change(function() {
            var task_name_id = $('#task_name').val();
            if (task_name_id == 0) {
                $("#job_priority").prop('disabled', true);
                $('#job_priority').val(0);
            } else {
                $("#job_priority").prop('disabled', false);
            }
        });

        function get_task_name(task_type_id) {
            $.ajax({
                type: "POST",
                url: "task_name_for_scheduler",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "task_type_id": task_type_id,
                },
                success: function(data) {
                    var results = data;
                    console.log(JSON.stringify(data));
                    $('#task_name').html('');
                    if (results.length == 0) {
                        $("#job_priority").prop('disabled', true);
                        $('#task_name').append("<option id =\"0\" value=\"0\">Task Doesn't exist</option>");
                    } else {
                        $("#job_priority").prop('disabled', false);
                        for (var i = 0; i < results.length; i++) {
                            if (i == '0') {
                                $('#task_name').append("<option id =\"0\" value=\"0\">Select</option>");
                            }
                            var task_name = results[i].task_name
                            var task_id = results[i].task_id
                            $('#task_name').append("<option id =" + task_id + " value=" + task_id + ">" + task_name + "</option>");
                        }
                    }
                },
                beforeSend: function() {},
                error: function() {}
            });
        }

        function frequency_for_scheduler_date_time() {
            $.ajax({
                type: "GET",
                url: "frequency_for_scheduler",
                dataType: "json",
                success: function(data) {
                    $('#frequency_date_time').html('');
                    var results = data;
                    console.log(JSON.stringify(data));
                    for (var i = 0; i < results.length; i++) {
                        if (i == '0') {
                            $('#frequency_date_time').append("<option id =\"0\" value=\"0\">Select</option>");
                        }
                        var frequency_name = results[i].frequency_name
                        var frequency_id = results[i].frequency_id
                        $('#frequency_date_time').append("<option id =" + frequency_id + " value=" + frequency_id + ">" + frequency_name + "</option>");
                    }
                },
                beforeSend: function() {},
                error: function() {}
            });
        }

        function frequency_for_scheduler_immediately() {
            $.ajax({
                type: "GET",
                url: "frequency_for_scheduler",
                dataType: "json",
                success: function(data) {
                    $('#frequency').html('');
                    var results = data;
                    console.log(JSON.stringify(data));
                    for (var i = 0; i < results.length; i++) {
                        if (i == '0') {
                            $('#frequency').append("<option id =\"0\" value=\"0\">Select</option>");
                        }
                        var frequency_name = results[i].frequency_name
                        var frequency_id = results[i].frequency_id
                        $('#frequency').append("<option id =" + frequency_id + " value=" + frequency_id + ">" + frequency_name + "</option>");
                    }
                },
                beforeSend: function() {},
                error: function() {}
            });
        }

        function start_conditions_for_scheduler() {
            $.ajax({
                type: "GET",
                url: "start_conditions_for_scheduler",
                dataType: "json",
                success: function(data) {
                    $('#start_conditions').html('');
                    var results = data;
                    console.log(JSON.stringify(data));
                    for (var i = 0; i < results.length; i++) {
                        if (i == '0') {
                            $('#start_conditions').append("<option id =\"0\" value=\"0\">Select</option>");
                        }
                        var start_type_name = results[i].start_type_name
                        var start_type_id = results[i].start_type_id
                        $('#start_conditions').append("<option id =" + start_type_id + " value=" + start_type_id + ">" + start_type_name + "</option>");
                    }
                },
                beforeSend: function() {},
                error: function() {}
            });
        }

        $('#period_immediately').on('change', function() {
            if (jQuery(this).is(":checked")) {
                $("#frequency").prop('disabled', false);
            } else {
                $('#frequency').val(0);
                $("#frequency").prop('disabled', true);
            }
        });

        $('#periodic_date_time').on('change', function() {
            if (jQuery(this).is(":checked")) {
                $("#frequency_date_time").prop('disabled', false);
            } else {
                $('#frequency_date_time').val(0);
                $("#frequency_date_time").prop('disabled', true);
            }
        });

        $('#start_conditions').on('change', function() {
            if (this.value == 1) {
                frequency_for_scheduler_immediately();
                $('#immediate').hide();
                $('#date_time_div').hide();
                $('#after_job_div').hide();
                $("#frequency").prop('disabled', true);
            } else if (this.value == 2) {
                frequency_for_scheduler_date_time();
                $('#immediate').hide();
                $('#date_time_div').show();
                $('#after_job_div').hide();
                $("#frequency_date_time").prop('disabled', true);
            } else if (this.value == 3) {
                get_jobs();
                $('#immediate').hide();
                $('#date_time_div').hide();
                $('#after_job_div').show();

            }
        });

        function get_jobs() {
            $.ajax({
                type: "GET",
                url: "get_jobs_for_scheduler",
                dataType: "json",
                success: function(data) {
                    $('#predecessor_job_name').html('');
                    var results = data;
                    console.log(JSON.stringify(data));
                    for (var i = 0; i < results.length; i++) {
                        if (i == '0') {
                            $('#predecessor_job_name').append("<option id =\"0\" value=\"0\">Select</option>");
                        }
                        var job_name = results[i].job_name
                        var job_id = results[i].job_id
                        $('#predecessor_job_name').append("<option id =" + job_id + " value=" + job_id + ">" + job_name + "</option>");
                    }
                },
                beforeSend: function() {},
                error: function() {}
            });
        }

        $(document).on('click', '#update_button', function() {
            start_conditions = $("#start_conditions option:selected").attr('id');
            if ($('#scheduler_create_form').valid()) {
                if ($('#start_conditions_form').valid()) {
                    if (start_conditions == "1") {
                        if ($('#period_immediately').is(":checked")) {
                            if ($('#immediate_form').valid()) {
                                update_msg(start_conditions);
                            }
                        } else {
                            update_msg(start_conditions);
                        }
                    } else if (start_conditions == "2") {
                        if ($('#periodic_date_time').is(":checked")) {
                            if ($('#date_time_form_frequency').valid()) {
                                update_msg(start_conditions);
                            }
                        } else {
                            if ($('#date_time_form').valid()) {
                                update_msg(start_conditions);
                            }
                        }

                    } else if (start_conditions == "3") {
                        if ($('#after_job_from').valid()) {
                            update_msg(start_conditions);
                        }
                    }
                }
            }

        });

        function update_msg(start_conditions) {
            $.msgBox({
                title: "Confirm",
                content: "Do you want to update the information?",
                type: "confirm",
                buttons: [{
                    value: "Yes"
                }, {
                    value: "No"
                }],
                success: function(result) {
                    if (result == "Yes") {
                        update(start_conditions);
                    } else {

                    }
                }
            });
        }

        function update(start_conditions) {

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }
            today = yyyy + '-' + mm + '-' + dd;

            var dt = new Date();
            var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

            var job_name = $('#job_name').val();
            var task_type = $("#task_type option:selected").attr('id');
            var task_name = $("#task_name option:selected").attr('id');
            var job_priority = $("#job_priority option:selected").attr('id');

            var frequency = 0;

            if ($('#active').is(":checked")) {
                active = 1;
            } else {
                active = 0;
            }

            if (start_conditions == "1") {
                var job_start_date = today;
                var job_end_date = today;
                var job_last_run_date = today;
                var job_start_time = time;
                var job_end_time = time;
                var job_last_run_time = time;
                if ($('#period_immediately').is(":checked")) {
                    period_immediately = 1;
                    var frequency = $("#frequency option:selected").attr('id');
                } else {
                    period_immediately = 0;
                    var frequency = 0;
                }
            } else if (start_conditions == "2") {
                var job_start_date = $('#start_date').val();
                var job_end_date = $('#end_date').val();
                var job_last_run_date = $('#start_date').val();
                var job_start_time = $('#start_time').val();
                var job_end_time = $('#end_time').val();
                var job_last_run_time = $('#start_time').val();

                if ($('#periodic_date_time').is(":checked")) {
                    var periodic_date_time = 1;
                    var frequency_date_time = $("#frequency_date_time option:selected").attr('id');
                } else {
                    var periodic_date_time = 0;
                    var frequency_date_time = 0;
                }
            } else if (start_conditions == "3") {
                var job_start_date = today;
                var job_end_date = today;
                var job_last_run_date = today;
                var job_start_time = time;
                var job_end_time = time;
                var job_last_run_time = time;

                var after_job = $("#predecessor_job_name option:selected").attr('id');
                // var predecessor_job_name = $("#predecessor_job_name option:selected").attr('id');
            }

            $.ajax({
                type: "POST",
                url: "scheduler_update",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "job_id":job_id,
                    "job_name": job_name,
                    "task_type": task_type,
                    "task_name": task_name,
                    "job_priority": job_priority,
                    "start_type_id": start_conditions,
                    "active": active,

                    //1.Immediate
                    "period_immediately": period_immediately,
                    "frequency": frequency,

                    //2.date_time
                    "periodic_date_time": periodic_date_time,
                    "frequency_date_time": frequency_date_time,

                    //3. After job
                    "after_job": after_job,
                    // "predecessor_job_name": predecessor_job_name,

                    "job_start_date": job_start_date,
                    "job_end_date": job_end_date,
                    "job_last_run_date": job_last_run_date,
                    "job_start_time": job_start_time,
                    "job_end_time": job_end_time,
                    "job_last_run_time": job_last_run_time,

                },
                success: function(data) {
                    after_update();
                    $.msgBox({
                        title: "Message",
                        content: "Scheduler updated successfully",
                        type: "info",
                    });
                },
                beforeSend: function() {
                    $('#msg').show();
                },
                error: function() {}
            });
        }

        function after_update() {
          $('#tab_one').hide();
          $('#update_button').hide();
          $('#cancel_button').hide();
          $('#back_search').show();
          if(filter_clicked==true)
          {
            // filter_clicked = false;
            filter_click();
          }
          else {
            $("#show_all_link").trigger("click");
            $("#show_all").trigger("click");
            $('#list-of-user').show();
          }
        }

        function reset() {
            $('#scheduler_create_form').trigger("reset");
            $('#start_conditions_form').trigger("reset");
            $('#immediate_form').trigger("reset");
            $('#date_time_form').trigger("reset");
            $('#date_time_form_frequency').trigger("reset");
            $('#after_job_from').trigger("reset");
        }

        $('#cancel_button').click(function() {
            $('#cancel_button').hide();
            $('#list_of_jobs').show();
            $('#back_search').show();
            $('#tab_one').hide();
            $('#update_button').hide();
            $('#show_all').hide();
            $('#search_page').hide();
            $('#search_button').hide();

        });

        var fetch_job_name;
        $('input[name="job_name_search"]').autoComplete({
            minChars: 0,
            source: function(term, response) {
                try {
                    fetch_job_name.abort();
                } catch (e) {}
                fetch_job_name = $.getJSON('fetch_job_name', {
                    job_name: term
                }, function(data) {
                    response(data);
                });
            }
        });

        var fetch_task_type;
        $('input[name="task_type_search"]').autoComplete({
            minChars: 0,
            source: function(term, response) {
                try {
                    fetch_task_type.abort();
                } catch (e) {}
                fetch_task_type = $.getJSON('fetch_task_type', {
                    task_type: term
                }, function(data) {
                    response(data);
                });
            }
        });

        var fetch_task_name;
        $('input[name="task_name_search"]').autoComplete({
            minChars: 0,
            source: function(term, response) {
                try {
                    fetch_task_name.abort();
                } catch (e) {}
                fetch_task_name = $.getJSON('fetch_task_name', {
                    task_name: term
                }, function(data) {
                    response(data);
                });
            }
        });

        $('#search_button').click(function() {
            filter_clicked = true;
            show_search_results_page();
            filter_click();
        });

        function filter_click() {
            $('#list_of_jobs').show();
            $('#tab_one').hide();
            show_all_link_clicked = false;
            var job_name_search = $('#job_name_search').val();
            var task_type_search = $('#task_type_search').val();
            var task_name_search = $('#task_name_search').val();

            if ((job_name_search == "") && (task_type_search == "")  && (task_name_search == "")) {
                  show_search_parameter_page();
                $.msgBox({
                    title: "Alert",
                    content: "Search Criteria Not Available",
                    type: "alert",
                });

            } else {
                display_data();
            }
        }

        function display_data() {
            var job_name_search = $('#job_name_search').val();
            var task_type_search = $('#task_type_search').val();
            var task_name_search = $('#task_name_search').val();
            $.ajax({
                type: "POST",
                url: "job_display_show",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "job_name_search": job_name_search,
                    "task_type_search": task_type_search,
                    "task_name_search":task_name_search,
                },
                success: function(data) {
                    if (data != "0") {
                        results = data;
                        $('.tab tr').remove();
                        $('.tab').append('<tr style="background-color: #D3D3D3;"><th style="display:none">Job ID</th><th>Scheduler Job Name</th><th>Task Type</th><th>Job Status</th><th>Active</th><th>Created By</th><th>Modified By</th><th>Edit</th><th>Delete</th></tr>');

                        for (var i = 0; i < results.length; i++) {
                            if (results[i].active == 0) {
                                active_check = '<input type="checkbox" disabled readonly>';
                            } else {
                                active_check = '<input type="checkbox" checked disabled readonly>';
                            }
                            $('.tab').append('<tr data-id="' + i + '" class="double_click"><td class="job_id" style="display:none">' + results[i].job_id + '</td><td  class="job_name_width" style="overflow:hidden">' + results[i].job_name + '</td><td>' + results[i].task_type_name + '</td><td>' + results[i].job_status + '</td><td>' + active_check + '</td><td>'+ results[i].created_by +'</td><td>'+ results[i].updated_by +'</td><td><button id="' + i + '" class="edit btn btn-default glyphicon glyphicon-edit edit_color" type="button"></button></td><td><button  data-id="' + i + '" id="delete_showall" type="button" class="btn btn-default delete_color" aria-label="Left Align"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td></tr>')
                        }
                    } else {
                        if (filter_clicked == false) {
                            show_search_parameter_page();
                        } else {
                            show_search_parameter_page();
                            $.msgBox({
                                title: "Alert",
                                content: "Data not found",
                                type: "alert",
                            });
                        }

                    }
                },
                beforeSend: function() {

                },
                error: function() {}
            });
        }

        $(document).on('click', '#show_all', function() {
            show_all_link_clicked = true;
            filter_clicked = false;
            show_search_results_page();
            show_table();

        });


        function show_search_results_page() {
              $('#show_all').hide();
              $('#list_of_jobs').show();
              $('#search_page').hide();
              $('#back_search').show();
              $('#search_button').hide();
        }

        function show_search_parameter_page()
        {
          // alert("show");
          $('#back_search').hide();
          $('#show_all').show();
          $('#list_of_jobs').hide();
          $('#search_page').show();
          $('#back_search').hide();
          $('#search_button').show();
        }

        $(document).on('click', '#back_search', function() {
            show_search_parameter_page();
        });

        function show_table() {
            $.ajax({
                type: "GET",
                url: "get_jobs",
                dataType: "json",
                success: function(data) {
                    if (data != "0") {
                        results = data;
                        $('.tab tr').remove();
                        $('.tab').append('<tr style="background-color: #D3D3D3;"><th>Scheduler Job Name</th><th>Task Type</th><th>Job Status</th><th>Active</th><th>Created By</th><th>Modified By</th><th>Edit</th><th>Delete</th></tr>');

                        for (var i = 0; i < results.length; i++) {
                            if (results[i].active == 0) {
                                active_check = '<input type="checkbox" disabled readonly>';
                            } else {
                                active_check = '<input type="checkbox" checked disabled readonly>';
                            }
                            $('.tab').append('<tr data-id="' + i + '" class="double_click"><td class="job_id" style="display:none">' + results[i].job_id + '</td><td class="job_name_width" style=" overflow : hidden">' + results[i].job_name + '</td><td>' + results[i].task_type_name + '</td><td>' + results[i].job_status + '</td><td>' + active_check + '</td><td>'+ results[i].created_by +'</td><td>'+ results[i].updated_by +'</td><td><button id="' + i + '" class="edit btn btn-default glyphicon glyphicon-edit edit_color" type="button"></button></td><td><button  data-id="' + i + '" id="delete_showall" type="button" class="btn btn-default delete_color" aria-label="Left Align"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td></tr>')
                        }
                    } else {
                        if (filter_clicked == false) {
                            back_search();
                        } else {
                            back_search();
                            $.msgBox({
                                title: "Alert",
                                content: "Data not found",
                                type: "alert",
                            });
                        }

                    }
                },
                beforeSend: function() {

                },
                error: function() {}
            });
        }

          $(document).on('dblclick', '.double_click', function() {
            show_change_screen();
            var id = $(this).data("id");
            job_id = results[id].job_id;
            $('#job_name').val(results[id].job_name);
            $('#task_type').val(results[id].task_type_id);
            $('#job_priority').val(results[id].job_priority);
            $('#created_by').val(results[id].created_by);
            $('#created_date').val(results[id].created_at);
            $('#modified_by').val(results[id].updated_by);
            $('#modified_date').val(results[id].updated_at);

            if (results[id].active == 0) {
                $('#active').prop('checked', false);
            } else {
                $('#active').prop('checked', true);
            }

            $('#task_name').val(results[id].task_id);

            $('#start_conditions').val(results[id].start_type_id);

            if (results[id].start_type_id == 1) {
                $('#immediate').hide();
                $('#date_time_div').hide();
                $('#after_job_div').hide();

                if (results[id].periodic == 1) {
                    $('#period_immediately').prop('checked', true);
                    $.ajax({
                        type: "POST",
                        url: "get_frequency_for_scheduler",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "job_id": results[id].job_id,
                        },
                        success: function(data) {
                            var results = data;
                            $('#frequency').val(results[0].frequency_id);
                        },
                        beforeSend: function() {},
                        error: function() {}
                    });
                }

            } else if (results[id].start_type_id == 2) {
                $('#immediate').hide();
                $('#date_time_div').show();
                $('#after_job_div').hide();
                $("#start_date").val(results[id].job_start_date);
                $("#start_time").val(results[id].job_start_time);
                $("#end_date").val(results[id].job_end_date);
                $("#end_time").val(results[id].job_end_time);
                if (results[id].periodic == 1) {
                    $('#periodic_date_time').prop('checked', true);
                    $.ajax({
                        type: "POST",
                        url: "get_frequency_for_scheduler",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "job_id": results[id].job_id,
                        },
                        success: function(data) {
                            var results = data;
                            $('#frequency_date_time').val(results[0].frequency_id);
                        },
                        beforeSend: function() {},
                        error: function() {}
                    });
                }

            } else if (results[id].start_type_id == 3) {

                $('#immediate').hide();
                $('#date_time_div').hide();
                $('#after_job_div').show();

                $.ajax({
                    type: "POST",
                    url: "get_after_job_for_scheduler",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "job_id": results[id].job_id,
                    },
                    success: function(data) {
                        var results = data;
                        $('#predecessor_job_name').val(results[0].after_job);
                    },
                    beforeSend: function() {},
                    error: function() {}
                });

            }
          });

        $(document).on('click', '.edit', function() {
            show_change_screen();
            var id = $(this).attr("id");
            job_id = results[id].job_id;
            $('#job_name').val(results[id].job_name);
            $('#task_type').val(results[id].task_type_id);
            $('#job_priority').val(results[id].job_priority);
            $('#created_by').val(results[id].created_by);
            $('#created_date').val(results[id].created_at);
            $('#modified_by').val(results[id].updated_by);
            $('#modified_date').val(results[id].updated_at);

            if (results[id].active == 0) {
                $('#active').prop('checked', false);
            } else {
                $('#active').prop('checked', true);
            }

            $('#task_name').val(results[id].task_id);

            $('#start_conditions').val(results[id].start_type_id);

            if (results[id].start_type_id == 1) {
                $('#immediate').hide();
                $('#date_time_div').hide();
                $('#after_job_div').hide();

                if (results[id].periodic == 1) {
                    $('#period_immediately').prop('checked', true);
                    $.ajax({
                        type: "POST",
                        url: "get_frequency_for_scheduler",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "job_id": results[id].job_id,
                        },
                        success: function(data) {
                            var results = data;
                            $('#frequency').val(results[0].frequency_id);
                        },
                        beforeSend: function() {},
                        error: function() {}
                    });
                }

            } else if (results[id].start_type_id == 2) {
                $('#immediate').hide();
                $('#date_time_div').show();
                $('#after_job_div').hide();
                $("#start_date").val(results[id].job_start_date);
                $("#start_time").val(results[id].job_start_time);
                $("#end_date").val(results[id].job_end_date);
                $("#end_time").val(results[id].job_end_time);
                if (results[id].periodic == 1) {
                    $('#periodic_date_time').prop('checked', true);
                    $.ajax({
                        type: "POST",
                        url: "get_frequency_for_scheduler",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "job_id": results[id].job_id,
                        },
                        success: function(data) {
                            var results = data;
                            $('#frequency_date_time').val(results[0].frequency_id);
                        },
                        beforeSend: function() {},
                        error: function() {}
                    });
                }

            } else if (results[id].start_type_id == 3) {

                $('#immediate').hide();
                $('#date_time_div').hide();
                $('#after_job_div').show();

                $.ajax({
                    type: "POST",
                    url: "get_after_job_for_scheduler",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "job_id": results[id].job_id,
                    },
                    success: function(data) {
                        var results = data;
                        $('#predecessor_job_name').val(results[0].after_job);
                    },
                    beforeSend: function() {},
                    error: function() {}
                });

            }

        });

        function show_change_screen() {
            $('#tab_one').show();
            $('#list_of_jobs').hide();
            $('#cancel_button').show();
            $('#update_button').show();
            $('#back_search').hide();

        }

        $(document).on('click', '#delete_search', function() {
          var id = $(this).data("id");
          job_id = results[id].job_id;
            $.msgBox({
                title: "Confirm",
                content: "Do you want to delete the data",
                type: "confirm",
                buttons: [{
                    value: "Yes"
                }, {
                    value: "No"
                }],
                success: function(result) {
                    if (result == "Yes") {
                        $.ajax({
                            type: "POST",
                            url: "scheduler_delete",
                            data: {
                              "_token": "{{ csrf_token() }}",
                              "job_id": job_id,
                            },
                            success: function() {
                                display_data();
                                $.msgBox({
                                    title: "Message",
                                    content: "Job deleted successfully",
                                    type: "info",
                                });
                            },
                            beforeSend: function() {

                            },
                            error: function() {}
                        });
                    }
                }
            });
        });

        $(document).on('click', '#delete_showall', function() {

          var id = $(this).data("id");
          job_id = results[id].job_id;
            $.msgBox({
                title: "Confirm",
                content: "Do you want to delete the data",
                type: "confirm",
                buttons: [{
                    value: "Yes"
                }, {
                    value: "No"
                }],
                success: function(result) {
                    if (result == "Yes") {
                        $.ajax({
                            type: "POST",
                            url: "scheduler_delete",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "job_id": job_id,
                            },
                            success: function() {
                                show_table();
                                $.msgBox({
                                    title: "Message",
                                    content: "Job deleted successfully",
                                    type: "info",
                                });
                            },
                            beforeSend: function() {

                            },
                            error: function() {}
                        });
                    }
                }
            });
        });

    });
</script>
</div>
@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
   <button id="cancel_button" class="headerbuttons load" type="button" style="float: right; margin-right: 20px;">Cancel</button>
   <button id="update_button" class="headerbuttons load" type="button"  style="float: right; margin-right: 20px;">Update</button>
   <button id="search_button" class="headerbuttons" type="button"  style="float: left; margin-right: 20px;">Search</button>
   <button id="back_search" class="headerbuttons load" type="button"  style="float: left; margin-right: 20px;">Back</button>
</div>
@stop
