@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" href="css/user_master.css">
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="display-title">
      Display User Groups
   </div>
</div>
@stop

@section('content')
<div class="col-xs-12 content">
  <table id="display_usergrp_table">  </table>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $.ajax({
      type:"GET",
      url:"get_user_groups",
      dataType:"json",
      data:{
        "_token": "{{ csrf_token() }}"
      },

      success:function(users){
        for (var i = 0; i < users.length; i++)
        {
          if(i == 0){
            $('#display_usergrp_table').append('<tr style="background-color: #D3D3D3;"><th>Group Name</th><th>Group ID</th><th> Roles Assigned</th></tr>');
          }
          var user_group_name = users[i].user_group_name;
          var user_group_id = users[i].user_group_id;
          var assigned_roles = users[i].role_name;

          $('#display_usergrp_table').append('<tr data-id="' + i + '" class="double_click"><td>' + user_group_name + '</td><td>' + user_group_id + '</td><td>' + assigned_roles + '</td></tr>')
        }
      }
    });

    $('#display_usergrp_table, tr').dblclick(function (role) {

    });
  });


  $(window).load(function() {
      console.log("uder load funciton");
      $("#master_data").trigger('click');
      $("#user_master").trigger('click');
      $("#user_group").trigger('click');
  });
</script>
@stop
