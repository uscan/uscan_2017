<!-- sidebar nav -->
<nav id="sidebar-nav" style="overflow-y: scroll;height: calc(100vh - 203px);">
   <div class="just-padding">
      <div class="list-group list-group-root well">
        @if((Auth::user()->group_id == '1')or(Auth::user()->group_id == '2'))
         <a href="#item-1" class="list-group-item" data-toggle="collapse">
         <i id="master_data" class="glyphicon glyphicon-chevron-right"></i>Master Data
         </a>
         <div class="list-group collapse" id="item-1">
            <div class="list-group collapse" id="item-1-1">
               <a href="#item-1-1-1" onclick="window.location='{{ url("enterprise_create")}}'" class="list-group-item" data-toggle="collapse">
               <i class="glyphicon glyphicon-chevron-right"></i>Create
               </a>
               <a href="#item-1-1-2" onclick="window.location='{{ url("enterprise_change")}}'" class="list-group-item" data-toggle="collapse">
               <i class="glyphicon glyphicon-chevron-right"></i>Change
               </a>
               <a href="#item-1-1-3" onclick="window.location='{{ url("enterprise_display")}}'" class="list-group-item" data-toggle="collapse">
               <i class="glyphicon glyphicon-chevron-right"></i>Display
               </a>
            </div>
            <a href="#item-1-2" class="list-group-item" data-toggle="collapse">
            <i id="user_master" class="glyphicon glyphicon-chevron-right"></i>User Master
            </a>
            <div class="list-group collapse" id="item-1-2">

               <a href="#item-1-2-1" onclick="window.location='{{ url("create_user")}}'" class="list-group-item" data-toggle="collapse">
               <i class="glyphicon glyphicon-chevron-right"></i>Create
               </a>
               <a href="#item-1-2-2" onclick="window.location='{{ url("change_user")}}'" class="list-group-item" data-toggle="collapse">
               <i class="glyphicon glyphicon-chevron-right"></i>Change
               </a>
               <a href="#item-1-2-3" onclick="window.location='{{ url("display_user")}}'" class="list-group-item" data-toggle="collapse">
               <i class="glyphicon glyphicon-chevron-right"></i>Display
               </a>
               <!-- </div> -->
            </div>
            <a href="#item-1-3" class="list-group-item" data-toggle="collapse">
            <i id="customer_master" class="glyphicon glyphicon-chevron-right"></i>Customer Master
            </a>
            <div class="list-group collapse" id="item-1-3">
               <a href="javascript:void(0)" onclick="window.location='{{ url("customer_create")}}'" class="list-group-item" data-toggle="collapse">
               <i class="glyphicon glyphicon-chevron-right"></i>Create
               </a>
               <a href="" class="list-group-item" onclick="window.location='{{ url("customer_change")}}'"  data-toggle="collapse">
               <i class="glyphicon glyphicon-chevron-right"></i>Change
               </a>
               <a href="javascript:void(0)" onclick="window.location='{{ url("customer_display")}}'" class="list-group-item" data-toggle="collapse">
               <i class="glyphicon glyphicon-chevron-right"></i>Display
               </a>
            </div>
            <a href="#item-1-4"  class="list-group-item" data-toggle="collapse">
            <i id="plant_master" class="glyphicon glyphicon-chevron-right"></i>Plant Master
            </a>
            <div class="list-group collapse" id="item-1-4">
               <a href="javascript:void(0)" onclick="window.location='{{ url("plant_create")}}'" class="list-group-item" data-toggle="collapse">
               <i class="glyphicon glyphicon-chevron-right"></i>Create
               </a>
               <a href="#item-1-4-2" onclick="window.location='{{ url("plant_change")}}'" class="list-group-item" data-toggle="collapse">
               <i class="glyphicon glyphicon-chevron-right"></i>Change
               </a>
               <a href="javascript:void(0)" onclick="window.location='{{ url("plant_display")}}'" class="list-group-item" data-toggle="collapse">
               <i class="glyphicon glyphicon-chevron-right"></i>Display
               </a>
            </div>
            <a href="#item-1-5" class="list-group-item" data-toggle="collapse">
            <i id="portal_master" class="glyphicon glyphicon-chevron-right"></i>Portal Master
            </a>

            <div class="list-group collapse" id="item-1-5">
               <a href=""  onclick="window.location='{{ url("portal_create")}}'" class="list-group-item" data-toggle="collapse">
                   <i id="portal_master_create" class="glyphicon glyphicon-chevron-right"></i>Create
               </a>
               <a href="" onclick="window.location='{{ url("portal_change")}}'" class="list-group-item" data-toggle="collapse">
                   <i class="glyphicon glyphicon-chevron-right"></i>Change
               </a>
               <a href="#item-1-5-3" onclick="window.location='{{ url("portal_display")}}'" class="list-group-item" data-toggle="collapse">
                   <i class="glyphicon glyphicon-chevron-right"></i>Display
               </a>
               </div>

            <div class="list-group collapse" id="item-1-6">

            </div>

         </div>

         <a href="#item-4" id="set_up" class="list-group-item" data-toggle="collapse">
         <i id="setups" class="glyphicon glyphicon-chevron-right"></i>Setups
         </a>
         <div class="list-group collapse" id="item-4">
            <a href="#item-4-1" class="list-group-item" data-toggle="collapse">
            <i id="task" class="glyphicon glyphicon-chevron-right"></i>Tasks
            </a>

            <div class="list-group collapse" id="item-4-1">
               <a href="#item-4-1-1" onclick="window.location='{{ url("task_create")}}'" class="list-group-item" data-toggle="collapse">
                   <i class="glyphicon glyphicon-chevron-right"></i>Create
               </a>

               <a href="#item-4-1-2" onclick="window.location='{{ url("task_change")}}'" class="list-group-item" data-toggle="collapse">
                   <i class="glyphicon glyphicon-chevron-right"></i>Change
               </a>

               <a href="#item-4-1-3" onclick="window.location='{{ url("task_display")}}'" class="list-group-item" data-toggle="collapse">
                   <i class="glyphicon glyphicon-chevron-right"></i>Display
               </a>

               </div>
            <a href="#item-4-2" id="scheduler" class="list-group-item" data-toggle="collapse">
            <i class="glyphicon glyphicon-chevron-right"></i>Scheduler Job
            </a>
            <div class="list-group collapse" id="item-4-2">
               <a href="#item-4-2-1" onclick="window.location='{{ url("scheduler_create")}}'" class="list-group-item" data-toggle="collapse">

                  <i class="glyphicon glyphicon-chevron-right"></i>Create
                  </a>

                  <a href="#item-4-2-2" onclick="window.location='{{ url("scheduler_change")}}'"  class="list-group-item" data-toggle="collapse">

                  <i class="glyphicon glyphicon-chevron-right"></i>Change
                  </a>

                  <a href="#item-4-2-3" onclick="window.location='{{ url("scheduler_display")}}'" class="list-group-item" data-toggle="collapse">

                  <i class="glyphicon glyphicon-chevron-right"></i>Display
                  </a>

            </div>
            <a href="#item-4-3" id="communication" class="list-group-item" data-toggle="collapse">
            <i class="glyphicon glyphicon-chevron-right"></i>Communication
            </a>
            <div class="list-group collapse" id="item-4-3" style="background-color:white;">
               <a href="#item-4-3-1" id="mail" class="list-group-item" data-toggle="collapse">
               <i class="glyphicon glyphicon-chevron-right"></i>MAIL
               </a>
                 <div class="list-group collapse" id="item-4-3-1">
                  <a href="#item-4-3-1-2" id="connection" class="list-group-item" data-toggle="collapse">
                  <i class="glyphicon glyphicon-chevron-right"></i>Connections
                  </a>
                   <div class="list-group collapse" id="item-4-3-1-2">
                     <a href="#item-4-3-1-2-1" onclick="window.location='{{ url("create_mail_connection")}}'"  style="padding-left: 65px;"class="list-group-item" data-toggle="collapse" >
                     <i class="glyphicon glyphicon-chevron-right"></i>Create
                     </a>
                     <a href="#item-4-3-1-2-2"  onclick="window.location='{{ url("change_mail_connection")}}'"  style="padding-left: 65px;" class="list-group-item" data-toggle="collapse">
                     <i class="glyphicon glyphicon-chevron-right"></i>Change
                     </a>
                     <a href="#item-4-3-1-2-3" onclick="window.location='{{ url("display_mail_connection")}}'"  style="padding-left: 65px;" class="list-group-item" data-toggle="collapse">
                     <i class="glyphicon glyphicon-chevron-right"></i>Display
                     </a>
                  </div>
                  <a href="#item-4-3-1-1" id="address" class="list-group-item" data-toggle="collapse" >
                  <i class="glyphicon glyphicon-chevron-right"></i>Address
                  </a>
                  <div class="list-group collapse" id="item-4-3-1-1">
                     <a href="#item-4-3-1-1-1" id="system_users" class="list-group-item" data-toggle="collapse" style="padding-left: 65px;">
                     <i class="glyphicon glyphicon-chevron-right"></i>System Users
                     </a>
                     <div class="list-group collapse" id="item-4-3-1-1-1" >
                        <a href="#item-4-3-1-1-1-1" onclick="window.location='{{ url("create_system_user")}}'" style="padding-left: 85px;"  class="list-group-item" data-toggle="collapse" >
                        <i class="glyphicon glyphicon-chevron-right"></i>Create
                        </a>
                        <a href="#item-4-3-1-1-1-2" onclick="window.location='{{ url("change_system_user")}}'" style="padding-left: 85px;" class="list-group-item" data-toggle="collapse">
                        <i class="glyphicon glyphicon-chevron-right"></i>Change
                        </a>
                        <a href="#item-4-3-1-1-1-3"  onclick="window.location='{{ url("display_system_user")}}'"  style="padding-left: 85px;"  class="list-group-item" data-toggle="collapse">
                        <i class="glyphicon glyphicon-chevron-right"></i>Display
                        </a>
                     </div>
                     <a href="#item-4-3-1-1-2" id ="enterprise_users" class="list-group-item" data-toggle="collapse" style="padding-left: 65px;">
                     <i class="glyphicon glyphicon-chevron-right"></i>Enterprise Users
                     </a>
                     <div class="list-group collapse" id="item-4-3-1-1-2" >
                        <a href="#item-4-3-1-1-2-1" onclick="window.location='{{ url("create_enterprise_users")}}'" style="padding-left: 85px;" class="list-group-item" data-toggle="collapse" >
                        <i class="glyphicon glyphicon-chevron-right"></i>Create
                        </a>
                        <a href="#item-4-3-1-1-2-2" onclick="window.location='{{ url("change_enterprise_users")}}'"style="padding-left: 85px;" class="list-group-item" data-toggle="collapse">
                        <i class="glyphicon glyphicon-chevron-right"></i>Change
                        </a>
                        <a href="#item-4-3-1-1-2-3" onclick="window.location='{{ url("display_enterprise_users")}}'" style="padding-left: 85px;"class="list-group-item" data-toggle="collapse">
                        <i class="glyphicon glyphicon-chevron-right"></i>Display
                        </a>
                     </div>
                  </div>
               </div>
               <a href="#item-4-3-2" id="ftp" class="list-group-item" data-toggle="collapse">
               <i class="glyphicon glyphicon-chevron-right"></i>FTP
               </a>
               <div class="list-group collapse" id="item-4-3-2">
                  <a href="#item-4-3-2-2" id="ftp_connection" class="list-group-item" data-toggle="collapse">
                  <i class="glyphicon glyphicon-chevron-right"></i>Connections
                  </a>
                  <div class="list-group collapse" id="item-4-3-2-2" >
                     <a href="#item-4-3-2-2-1" onclick="window.location='{{ url("create_ftp_connection")}}'" style="padding-left: 65px;" class="list-group-item" data-toggle="collapse" >
                     <i class="glyphicon glyphicon-chevron-right"></i>Create
                     </a>
                     <a href="#item-4-3-2-2-2"  onclick="window.location='{{ url("change_ftp_connection")}}'"style="padding-left: 65px;" class="list-group-item" data-toggle="collapse">
                     <i class="glyphicon glyphicon-chevron-right"></i>Change
                     </a>
                     <a href="#item-4-3-2-2-3"  onclick="window.location='{{ url("display_ftp_connection")}}'"style="padding-left: 65px;" class="list-group-item" data-toggle="collapse">
                     <i class="glyphicon glyphicon-chevron-right"></i>Display
                     </a>
                  </div>
                  <a href="#item-4-3-2-1" id ="ftp_address" class="list-group-item" data-toggle="collapse" >
                  <i class="glyphicon glyphicon-chevron-right"></i>Address
                  </a>
                  <div class="list-group collapse" id="item-4-3-2-1">
                     <a href="#item-4-3-1-1-1-1"  onclick="window.location='{{ url("create_ftp_address")}}'" style="padding-left: 65px;" class="list-group-item" data-toggle="collapse" >
                     <i class="glyphicon glyphicon-chevron-right"></i>Create
                     </a>
                     <a href="#item-4-3-1-1-1-2"  onclick="window.location='{{ url("change_ftp_address")}}'" style="padding-left: 65px;" class="list-group-item" data-toggle="collapse">
                     <i class="glyphicon glyphicon-chevron-right"></i>Change
                     </a>
                     <a href="#item-4-3-1-1-1-3"  onclick="window.location='{{ url("display_ftp_address")}}'" style="padding-left: 65px;" class="list-group-item" data-toggle="collapse">
                     <i class="glyphicon glyphicon-chevron-right"></i>Display
                     </a>
                  </div>
               </div>
            </div>
            <a href="#item-4-4" class="list-group-item" data-toggle="collapse">
            <i id = "cross_reference" class="glyphicon glyphicon-chevron-right"></i>Cross Reference Table
            </a>
            <div class="list-group collapse" id="item-4-4" >
               <a id ="plant_customer_code" href="#item-4-4-1"  class="list-group-item" data-toggle="collapse" >
               <i  class="glyphicon glyphicon-chevron-right"></i>Plant Customer Code
               </a>
               <div class="list-group collapse" id="item-4-4-1" >
                  <a href="#item-4-4-1-1" onclick="window.location='{{ url("plant_customer_code_change")}}'" class="list-group-item" data-toggle="collapse" >
                  <i class="glyphicon glyphicon-chevron-right"></i>Change
                  </a>
                  <a href="#item-4-4-1-2" onclick="window.location='{{ url("plant_customer_code_display")}}'"  class="list-group-item" data-toggle="collapse" >
                  <i class="glyphicon glyphicon-chevron-right"></i>Display
                  </a>
               </div>
            </div>
         </div>
         <a href="#item-3" class="list-group-item" data-toggle="collapse">
         <i id = "monitor" class="glyphicon glyphicon-chevron-right"></i>Monitor
         </a>
         <div class="list-group collapse" id="item-3">
            <a href="#item-3-1" onclick="window.location='{{ url("process_monitor")}}'"  class="list-group-item" data-toggle="collapse">
            <i id = "process_monitor" class="glyphicon glyphicon-chevron-right"></i>Process Monitor
            </a>
              <a href="#item-3-3" class="list-group-item" data-toggle="collapse">
            <i class="glyphicon glyphicon-chevron-right"></i>Communication Monitor
            </a>
         </div>
           @endif

         <a href="#item-6" class="list-group-item" data-toggle="collapse">
         <i class="glyphicon glyphicon-chevron-right" id="report_generator"></i>Report Generator
         </a>
         <div class="list-group collapse" id="item-6">
            <a href="#item-6-1"onclick="window.location='{{ url("report_generator")}}'" class="list-group-item" data-toggle="collapse">
            <i class="glyphicon glyphicon-chevron-right" id = "manual_reports"></i>Generate Report
            </a>
            <a href="#item-6-2" class="list-group-item" data-toggle="collapse">
            <i class="glyphicon glyphicon-chevron-right" id="schedule_reports"></i>Schedule Report
            </a>

            <div class="list-group collapse" id="item-6-2">
               <a href="#item-6-2-1" onclick="window.location='{{ url("create_schedule_report")}}'" class="list-group-item" data-toggle="collapse">

                  <i class="glyphicon glyphicon-chevron-right"></i>Create
                  </a>

                  <a href="#item-6-2-2" onclick="window.location='{{ url("change_schedule_report")}}'"  class="list-group-item" data-toggle="collapse">

                  <i class="glyphicon glyphicon-chevron-right"></i>Change
                  </a>

                  <a href="#item-6-2-3" onclick="window.location='{{ url("display_schedule_report")}}'" class="list-group-item" data-toggle="collapse">

                  <i class="glyphicon glyphicon-chevron-right"></i>Display
                  </a>

            </div>


         </div>

         <a href="#item-5"  class="list-group-item" data-toggle="collapse">
         <i id="administration" class="glyphicon glyphicon-chevron-right"></i>Product Administration
         </a>
         <div class="list-group collapse" id="item-5">
            <a href="#item-5-1" onclick="window.location='{{ url("product_administration")}}'" class="list-group-item" data-toggle="collapse">
            <i class="glyphicon glyphicon-chevron-right"></i>Administration
            </a>
            <a href="#item-5-2"class="list-group-item" onclick="window.location='{{ url("help_page_search")}}'" data-toggle="collapse">
            <i id="search_help" class="glyphicon glyphicon-chevron-right"></i>Help
            </a>

         </div>

      </div>
   </div>
</nav>
