@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/communication.css">
<script src="js/communication_validation.js"></script>
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="col-xs-4 display-title">
      <span>Change Mail Connection</span>
   </div>
   <div class="col-xs-2" id = "show_all_connections" style="float: right;text-align:center;line-height:30px;">
      <span id="show_all_p">Show All Connections</span>
   </div>
</div>
@stop

@section('content')
<div class="col-xs-12 content">
   <div id="connection-table-search">
      <div id="search-para-connection">
         <form id="connection_search_form" autocomplete="off">
            <table style="width:40%;">
               <tr>
                 <td>Connection Name</td>
                 <td><input type="text"  id = "search_connection_name" name="search_connection_name"></td>
               </tr>
               <tr>
                 <td>Protocol</td>
                 <td><input type="text" name="search_protocol" id="search_protocol"></td>
               </tr>
               <tr>
                  <td>Server</td>
                  <td><input type="text" name="search_server_name" id="search_server_name"></td>
               </tr>
            </table>
         </form>
      </div>
      <div id="connection_table"  style="display:none">
         <ul class="nav nav-tabs">
            <li class="active"><a id ="details_connection" data-toggle="tab" href="#tab_one_connection">Details</a></li>
            <li ><a data-toggle="tab" href="#tab_two_connection">Additional Details</a></li>
         </ul>
         <div class="tab-content">
            <div id="tab_one_connection" class="tab-pane fade in active">
               <form id = "connection-table-form" autocomplete="off">
                  <table style="width:40%;">
                    <tr>
                       <td>Connection Name</td>
                       <td><input type="text"  id = "connection_name" name="connection_name"></td>
                    </tr>
                    <tr>
                       <td>Protocol</td>
                       <td><input type="text" name="protocol" id="protocol"></td>
                    </tr>
                    <tr>
                       <td>Server</td>
                       <td><input type="text" name="server" id="server"></td>
                    </tr>
                    <tr>
                       <td>Port</td>
                       <td><input type="text" name="port" id="port"></td>
                    </tr>
                    <tr>
                      <td>Request Notification</td>
                      <td><input type="checkbox" name="request_notification" id="request_notification" value=""></td>
                    </tr>
                    <tr>
                      <td>Proxy Mode</td>
                      <td><select id="proxy_mode" name="proxy_mode">
                        <option value="0">Select</option>
                        <option value="No Proxy">No Proxy</option>
                        <option value="Specific Proxy">Specific Proxy</option>
                        <option value="Proxy Group">Proxy Group</option>
                      </select></td>
                    </tr>
                    <tr>
                      <td>Dump Messages</td>
                      <td><input type="checkbox" name="dump_message" id="dump_message" value=""></td>
                    </tr>
                  </table>
            </div>
            <div id="tab_two_connection" class="tab-pane fade">
            <table table style="width:40%; margin-bottom: 20px">
            <tr>
            <td></td>
            </tr>
            <tr>
            <td>Created By</td>
            <td><input id ="created_by" type="text" name="" disabled readonly></td>
            </tr>
            <tr>
            <td>Created Date</td>
            <td><input id ="created_date" type="text" disabled readonly></td>
            </tr>
            <tr>
            <td>Updated By</td>
            <td><input id = "updated_by" type="text" disabled readonly></td>
            </tr>
            <tr>
            <td>Updated Date</td>
            <td><input id = "updated_at" type="text" disabled readonly></td>
            </tr>
            </table>
            </form>
            </div>
         </div>
      </div>
      <div id="list-of-connection">
         <table style="width:100%;margin-bottom: 371px" class="connection_tab">
         </table>
      </div>


      <script type="text/javascript">
          $(document).ready(function() {

              $('#connection_table,#list-of-connection,#back,#update,#cancel').hide();
              $('#search-para-connection').show();


               function showall_p() {

                  $.ajax({
                      type: "GET",
                      url: "get_connection_details",
                      dataType: "json",
                      success: function(data) {
                          $('#show_all_p').hide();
                          var results = data;
                          temp_results = data;
                          $('.connection_tab').append('<tr style="background-color: #D3D3D3;cursor:text;"><th>Connection Name</th><th>Protocol</th><th>Server</th><th>Port</th><th>Edit</th><th>Delete</th></tr>');

                          if (results.length > 12) {

                              $('#list-of-connection').addClass("scroll");

                          } else {
                              $('#list-of-connection').removeClass("scroll");
                          }

                          for (var i = 0; i < results.length; i++) {
                              $('.connection_tab').append('<tr class = "double_click_parent" id = ' + i + '><td>' + results[i].connection_name + '</td><td>' + results[i].protocol + '</td><td>' + results[i].server + '</td><td>' + results[i].port +
                                  '</td><td><button id="' + i + '" type="button" class="edit_parent btn btn-default glyphicon glyphicon-edit edit_color" aria-label="Left Align"></button></td><td><button id="' + results[i].mail_conn_id + '" type="button" class=" delete_parent btn btn-default delete_color" aria-label="Left Align"><span class="glyphicon glyphicon-remove delete_color" aria-hidden="true"></span></button></td></tr>');
                          }

                      },
                      beforeSend: function() {
                          $('.connection_tab tr').remove();
                          //$("#list-of-connection,.parent_bottom_buttons,#parent_back_buttons").addClass("show");
                          $(".customer_type").addClass("hide");
                          $("#parent_change,#parent_search_button,#show_all_parents,#child_change,.child_bottom_buttons,#search_button,#show_all_customer").removeClass("show");
                      },
                      error: function() {

                          $.msgBox({
                              title: "Error",
                              content: "Something went wrong",
                              type: "error",
                          });

                      }
                  });

              }

              function display_parent_data() {

                  var search_connection_name = $('#search_connection_name').val();
                  var search_protocol = $('#search_protocol').val();
                  var search_server_name = $('#search_server_name').val();

                  if ((search_connection_name == "") && (search_protocol == "") && (search_server_name == "")) {

                      $.msgBox({
                          title: "Alert",
                          content: "Search Criteria Not Available",
                          type: "alert",
                      });

                  } else {

                      $.ajax({
                          type: "POST",
                          url: "connection_search",
                          dataType: "json",
                          data: {
                              "_token": "{{ csrf_token() }}",
                              "search_connection_name": search_connection_name,
                              "search_protocol": search_protocol,
                              "search_server_name": search_server_name,
                          },

                          success: function(data) {

                              var results = data;
                              temp_results = data;
                              $('#connection_table,#search-para-connection,#search,#update,#cancel').hide();
                              $('#list-of-connection,#back').show();

                              if (data != "0") {

                                  $('.connection_tab').append('<tr style="background-color: #D3D3D3;cursor:text;"><th>Connection Name</th><th>Protocol</th><th>Server</th><th>Port</th><th>Edit</th><th>Delete</th></tr>');
                                  for (var i = 0; i < results.length; i++) {
                                      $('.connection_tab').append('<tr class = "double_click_parent" id = ' + i + '><td>' + results[i].connection_name + '</td><td>' + results[i].protocol + '</td><td>' + results[i].server + '</td><td>' + results[i].port +
                                          '</td><td><button id="' + i + '" type="button" class="edit_parent btn btn-default glyphicon glyphicon-edit edit_color" aria-label="Left Align"></button></td><td><button id="' + results[i].mail_conn_id + '" type="button" class=" delete_parent btn btn-default delete_color" aria-label="Left Align"><span class="glyphicon glyphicon-remove delete_color" aria-hidden="true"></span></button></td></tr>');
                                  }
                              } else {

                                $('#search-para-connection,#search').show();
                                $('#list-of-connection,#back').hide();

                                      $.msgBox({
                                          title: "Alert",
                                          content: "Result Not available",
                                          type: "alert",
                                      });

                              }

                          },
                          beforeSend: function() {
                              $('.connection_tab tr').remove();
                          },
                          error: function() {

                              $.msgBox({
                                  title: "Error",
                                  content: "Something went wrong",
                                  type: "error",
                              });

                          }
                      });

                  }
              }

              function update_mail_connection() {

                var connection_name = $('#connection_name').val();
                var protocol = $('#protocol').val();
                var server = $('#server').val();
                var port = $('#port').val();
                var proxy_mode = $('#proxy_mode').val();

                if ($('#request_notification').is(":checked")) {
                    request_notification = 1;
                } else {
                    request_notification = 0;
                }

                if ($('#dump_message').is(":checked")) {
                    dump_message = 1;
                } else {
                    dump_message = 0;
                }

                  $.ajax({
                      type: "POST",
                      url: "update_connection",
                      data: {
                          "_token": "{{ csrf_token() }}",
                          "mail_conn_id": mail_conn_id,
                          "connection_name": connection_name,
                          "protocol": protocol,
                          "server_name": server,
                          "port": port,
                          "proxy_mode": proxy_mode,
                          "request_notification": request_notification,
                          "dump_message": dump_message,

                      },
                      success: function(data) {

                          $.msgBox({
                              title: "Message",
                              content: data,
                              type: "info",
                          });
                          $('#connection-table-form')[0].reset();
                          $('#update,#cancel').hide();
                          $("#show_all_connections").trigger("click");
                          $("#show_all_p").trigger("click");
                          $('#list-of-connection').show();

                      },

                      beforeSend: function() {},
                      error: function() {
                          $.msgBox({
                              title: "Alert",
                              content: "Something went wrong while update",
                              type: "alert",
                          });
                      }
                  });
              }

              $(document).on('dblclick', '.double_click_parent', function() {

                var id = $(this).attr('id');
                mail_conn_id = temp_results[id].mail_conn_id;
                console.log("mail_conn_id edit",mail_conn_id);
                $('#connection_name').val(temp_results[id].connection_name);
                $('#protocol').val(temp_results[id].protocol);
                $('#server').val(temp_results[id].server);
                $('#port').val(temp_results[id].port);
                $('#proxy_mode').val(temp_results[id].proxy_mode);
                $('#created_by').val(temp_results[id].created_by);
                $('#created_date').val(temp_results[id].created_at);
                $('#updated_by').val(temp_results[id].updated_by);
                $('#updated_at').val(temp_results[id].updated_at);

                if (temp_results[id].request_notification == 0) {
                    $('#request_notification').prop('checked', false);
                } else {
                    $('#request_notification').prop('checked', true);
                }

                if (temp_results[id].dump_messages == 0) {
                    $('#dump_message').prop('checked', false);
                } else {
                    $('#dump_message').prop('checked', true);
                }

                $('#connection_table,#update,#cancel').show();
                $('#list-of-connection,#back,#search,#search-para-connection').hide();

              });


              $(document).on('click', '#update', function() {
                  if ($('#connection-table-form').valid()) {

                      $.msgBox({
                          title: "Confirmation",
                          content: "Do you want to update the mail connection",
                          type: "confirm",
                          buttons: [{
                              value: "Yes"
                          }, {
                              value: "No"
                          }],
                          success: function(result) {
                              if (result == "Yes") {
                                  update_mail_connection();

                              } else {

                              }
                          }
                      });
                  }
              });

              $(document).on('click', '#back', function() {
                  $('#search-para-connection,#search').show();
                  $('#list-of-connection,#connection_table,#back').hide();
                  $('#show_all_p').show();
              });

              $(document).on('click', '#search', function() {
                display_parent_data();
              });

              $(document).on('click', '#show_all_p', function() {
                $('#connection_table,#search-para-connection,#search').hide();
                $('#list-of-connection,#back').show();
                showall_p();
              });

              $(document).on('click', '.delete_parent', function() {
                  var msg = "Do you want to delete the </br> Mail Connection";
                  var mail_conn_id = $(this).attr('id');

                  $.msgBox({
                      title: "Confirm",
                      content: msg,
                      type: "confirm",
                      buttons: [{
                          value: "Yes"
                      }, {
                          value: "No"
                      }],
                      success: function(result) {
                          if (result == "Yes") {

                              $.ajax({
                                  type: "POST",
                                  url: "connection_delete",
                                  data: {
                                      "_token": "{{ csrf_token() }}",
                                      "mail_conn_id": mail_conn_id,
                                  },
                                  success: function(data) {
                                    showall_p();
                                      $.msgBox({
                                          title: "Message",
                                          content: data,
                                          type: "info",
                                      });

                                  },
                                  beforeSend: function() {

                                  },
                                  error: function() {

                                      $.msgBox({
                                          title: "Message",
                                          content: "Can not delete the Mail Connection.. <br> This Connection has assigned for Address",
                                          type: "info",
                                      });

                                  }
                              });


                          } else {

                          }
                      }
                  });

              });

              $(document).on('click', '.edit_parent', function() {

                  var id = $(this).attr('id');
                  mail_conn_id = temp_results[id].mail_conn_id;
                  console.log("mail_conn_id edit",mail_conn_id);
                  $('#connection_name').val(temp_results[id].connection_name);
                  $('#protocol').val(temp_results[id].protocol);
                  $('#server').val(temp_results[id].server);
                  $('#port').val(temp_results[id].port);
                  //$('#request_notification').val(temp_results[id].request_notification);
                  $('#proxy_mode').val(temp_results[id].proxy_mode);
                  //$('#dump_message').val(temp_results[id].dump_message);
                  $('#created_by').val(temp_results[id].created_by);
                  $('#created_date').val(temp_results[id].created_at);
                  $('#updated_by').val(temp_results[id].updated_by);
                  $('#updated_at').val(temp_results[id].updated_at);

                  if (temp_results[id].request_notification == 0) {
                      $('#request_notification').prop('checked', false);
                  } else {
                      $('#request_notification').prop('checked', true);
                  }

                  if (temp_results[id].dump_messages == 0) {
                      $('#dump_message').prop('checked', false);
                  } else {
                      $('#dump_message').prop('checked', true);
                  }

                  $('#connection_table,#update,#cancel').show();
                  $('#list-of-connection,#back,#search,#search-para-connection').hide();

              });

              $(document).on('click', '#cancel', function() {
                  $('#connection_table,#update,#cancel').hide();
                  $('#list-of-connection,#back').show();

              });
          });

          $(window).load(function() {

            $("#setups").trigger('click');
            $("#communication").trigger('click');
            $("#mail").trigger('click');
            $("#connection").trigger('click');

              var fetch_search_connection_name_display;
              $('input[name="search_connection_name"]').autoComplete({
                  minChars: 1,
                  source: function(term, response) {
                      try {
                          fetch_search_connection_name_display.abort();
                      } catch (e) {}
                      fetch_search_connection_name_display = $.getJSON('communication_controller/autocomplete_connection_name', {
                          fetch_search_connection_name: term
                      }, function(data) {
                          response(data);
                      });
                  }
              });

              var fetch_search_protocol_display;
              $('input[name="search_protocol"]').autoComplete({
                  minChars: 1,
                  source: function(term, response) {
                      try {
                          fetch_search_protocol_display.abort();
                      } catch (e) {}
                      fetch_search_protocol_display = $.getJSON('communication_controller/autocomplete_protocol', {
                          fetch_search_protocol_display: term
                      }, function(data) {
                          response(data);
                      });
                  }
              });
              var fetch_search_server_display;
              $('input[name="search_server_name"]').autoComplete({
                  minChars: 1,
                  source: function(term, response) {
                      try {
                          fetch_search_server_display.abort();
                      } catch (e) {}
                      fetch_search_server_display = $.getJSON('communication_controller/autocomplete_server', {
                          fetch_search_server_display: term
                      }, function(data) {
                          response(data);
                      });
                  }
              });

          });
      </script>
   </div>

</div>
@stop

@section('lower_band')
<div class="col-xs-12 lower_band">
   <div class="bottom_buttons">
      <div  id="search_button">
         <button class="headerbuttons" id="search" type="button">Search</button>
      </div>
      <div  id="back_buttons">
         <button class="headerbuttons" id="back" type="button">Back</button>
      </div>
      <div id="update_button" style="float:right">
         <button class="headerbuttons" id="update" type="button">Update</button>
         <button class="headerbuttons" id="cancel" type="button">Cancel</button>
      </div>
   </div>
</div>
@stop
