@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/communication.css">
<script src="js/communication_validation.js"></script>
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="col-xs-4 display-title">
      <span>Change FTP Connection</span>
   </div>
   <div class="col-xs-3" id = "show_all_connections" style="float: right;text-align:center;line-height:30px;">
      <span id="show_all">Show All FTP Connections</span>
   </div>
</div>
@stop

@section('content')
<div class="col-xs-12 content">
   <div id="connection-table-search">
      <div id="search-para-connection">
         <form id="connection_search_form" autocomplete="off">
            <table style="width:40%;">
               <tr>
                 <td>Connection Name</td>
                 <td><input type="text"  id = "search_connection_name" name="search_connection_name"></td>
               </tr>
               <tr>
                 <td>Server Type</td>
                 <td><input type="text" name="search_server_type" id="search_server_type"></td>
               </tr>
               <tr>
                  <td>Host</td>
                  <td><input type="text" name="search_host" id="search_host"></td>
               </tr>
            </table>
         </form>
      </div>

      <div id="connection_table"  style="display:none">
         <ul class="nav nav-tabs">
            <li class="active"><a id ="details_connection" data-toggle="tab" href="#tab_one_connection">Details</a></li>
            <li ><a data-toggle="tab" href="#tab_two_connection">Additional Details</a></li>
         </ul>
         <div class="tab-content">
            <div id="tab_one_connection" class="tab-pane fade in active">
               <form id = "connection-table-form" autocomplete="off">
                  <table style="width:40%;">
                    <tr>
                       <td>Connection Name</td>
                       <td><input type="text"  id = "connection_name" name="connection_name"></td>
                    </tr>
                    <tr>
                       <td>Server Type</td>
                       <td><select id="server_type" name="server_type">
                         <option value="0">Select</option>
                         <option value="Standard FTP Server">Standard FTP Server</option>
                       </select></td>
                    </tr>
                    <tr>
                       <td>Host</td>
                       <td><input type="text" name="host" id="host"></td>
                    </tr>
                    <tr>
                       <td>Port</td>
                       <td><input type="text" name="port" id="port"></td>
                    </tr>

                    <tr>
                      <td>Connection Mode</td>
                      <td><select id="connection_mode" name="connection_mode">
                        <option value="0">Select</option>
                        <option value="Active">Active</option>
                        <option value="Passive">Passive</option>

                      </select></td>
                    </tr>
                    <tr>
                      <td>Keep Alive</td>
                      <td><input type="checkbox" name="keep_alive" id="keep_alive" value=""></td>
                    </tr>
                    <tr>
                      <td>Transfer Type</td>
                      <td><select id="transfer_file" name="transfer_file">
                        <option value="0">Select</option>
                        <option value="Binary">Binary</option>
                        <option value="ASCII">ASCII</option>
                      </select></td>
                    </tr>
                    <tr>
                      <td>Lock File</td>
                      <td><input type="checkbox" name="lock_file" id="lock_file" value=""></td>
                    </tr>
                    <tr>
                      <td>Mode of file Transfer</td>
                      <td><select id="mode_of_file" name="mode_of_file">
                        <option value="0">Select</option>
                        <option value="Append">Append</option>
                        <option value="Overwrite">Overwrite</option>
                      </select></td>
                    </tr>
                    <tr>
                      <td>Delete File</td>
                      <td><input type="checkbox" name="delete_file" id="delete_file" value=""></td>
                    </tr>
                  </table>
            </div>
            <div id="tab_two_connection" class="tab-pane fade">
            <table table style="width:40%; margin-bottom: 20px">
            <tr>
            <td></td>
            </tr>
            <tr>
            <td>Created By</td>
            <td><input id ="created_by" type="text" name="" disabled readonly></td>
            </tr>
            <tr>
            <td>Created Date</td>
            <td><input id ="created_date" type="text" disabled readonly></td>
            </tr>
            <tr>
            <td>Updated By</td>
            <td><input id = "updated_by" type="text" disabled readonly></td>
            </tr>
            <tr>
            <td>Updated Date</td>
            <td><input id = "updated_at" type="text" disabled readonly></td>
            </tr>
            </table>
            </form>
            </div>
         </div>
      </div>
      <div id="list-of-connection">
         <table style="width:100%;margin-bottom: 371px" class="connection_tab">
         </table>
      </div>

      <script type="text/javascript">
          $(document).ready(function() {

              $('#connection_table,#list-of-connection,#back,#update,#cancel').hide();
              $('#search-para-connection').show();


               function showall() {

                  $.ajax({
                      type: "GET",
                      url: "get_ftp_connection_details",
                      dataType: "json",
                      success: function(data) {
                          $('#show_all').hide();
                          var results = data;
                          temp_results = data;
                          $('.connection_tab').append('<tr style="background-color: #D3D3D3;cursor:text;"><th>Connection Name</th><th>Server Type</th><th>Host</th><th>Port</th><th>Edit</th><th>Delete</th></tr>');

                          if (results.length > 12) {

                              $('#list-of-connection').addClass("scroll");

                          } else {
                              $('#list-of-connection').removeClass("scroll");
                          }

                          for (var i = 0; i < results.length; i++) {
                              $('.connection_tab').append('<tr class = "double_click_connection" id = ' + i + '><td>' + results[i].connection_name + '</td><td>' + results[i].server_type + '</td><td>' + results[i].host + '</td><td>' + results[i].port +
                                  '</td><td><button id="' + i + '" type="button" class="edit_connection btn btn-default glyphicon glyphicon-edit edit_color" aria-label="Left Align"></button></td><td><button id="' + results[i].ftp_conn_id + '" type="button" class=" delete_connection btn btn-default delete_color" aria-label="Left Align"><span class="glyphicon glyphicon-remove delete_color" aria-hidden="true"></span></button></td></tr>');
                          }

                      },
                      beforeSend: function() {
                          $('.connection_tab tr').remove();
                          //$("#list-of-connection,.parent_bottom_buttons,#parent_back_buttons").addClass("show");
                          $(".customer_type").addClass("hide");
                          $("#parent_change,#parent_search_button,#show_all_parents,#child_change,.child_bottom_buttons,#search_button,#show_all_customer").removeClass("show");
                      },
                      error: function() {

                          $.msgBox({
                              title: "Error",
                              content: "Something went wrong",
                              type: "error",
                          });

                      }
                  });

              }

              function display_ftp_connection() {

                  var search_connection_name = $('#search_connection_name').val();
                  var search_host = $('#search_host').val();
                  var search_server_type = $('#search_server_type').val();

                  if ((search_connection_name == "") && (search_host == "") && (search_server_type == "")) {

                      $.msgBox({
                          title: "Alert",
                          content: "Search Criteria Not Available",
                          type: "alert",
                      });

                  } else {

                      $.ajax({
                          type: "POST",
                          url: "ftp_connection_search",
                          dataType: "json",
                          data: {
                              "_token": "{{ csrf_token() }}",
                              "search_connection_name": search_connection_name,
                              "search_host": search_host,
                              "search_server_type": search_server_type,
                          },

                          success: function(data) {

                              var results = data;
                              temp_results = data;

                              $('#connection_table,#search-para-connection,#search,#update,#cancel').hide();
                              $('#list-of-connection,#back').show();

                              if (data != "0") {
                                  $('.connection_tab').append('<tr style="background-color: #D3D3D3;cursor:text;"><th>Connection Name</th><th>Server Type</th><th>Host</th><th>Port</th><th>Edit</th><th>Delete</th></tr>');
                                  for (var i = 0; i < results.length; i++) {
                                    $('.connection_tab').append('<tr class = "double_click_connection" id = ' + i + '><td>' + results[i].connection_name + '</td><td>' + results[i].server_type + '</td><td>' + results[i].host + '</td><td>' + results[i].port +
                                        '</td><td><button id="' + i + '" type="button" class="edit_connection btn btn-default glyphicon glyphicon-edit edit_color" aria-label="Left Align"></button></td><td><button id="' + results[i].ftp_conn_id + '" type="button" class=" delete_connection btn btn-default delete_color" aria-label="Left Align"><span class="glyphicon glyphicon-remove delete_color" aria-hidden="true"></span></button></td></tr>');
                                  }
                              } else {

                                $('#search-para-connection,#search').show();
                                $('#list-of-connection,#back').hide();

                                      $.msgBox({
                                          title: "Alert",
                                          content: "Result Not available",
                                          type: "alert",
                                      });
                              }

                          },
                          beforeSend: function() {
                              $('.connection_tab tr').remove();
                          },
                          error: function() {

                              $.msgBox({
                                  title: "Error",
                                  content: "Something went wrong",
                                  type: "error",
                              });
                          }
                      });

                  }
              }

              function update_parent_data() {

                var connection_name = $('#connection_name').val();
                var server_type = $('#server_type').val();
                var host = $('#host').val();
                var port = $('#port').val();
                var connection_mode = $('#connection_mode').val();
                var transfer_file = $('#transfer_file').val();
                var mode_of_file = $('#mode_of_file').val();

                if ($('#keep_alive').is(":checked")) {
                    keep_alive = 1;
                } else {
                    keep_alive = 0;
                }

                if ($('#lock_file').is(":checked")) {
                    lock_file = 1;
                } else {
                    lock_file = 0;
                }
                if ($('#delete_file').is(":checked")) {
                    delete_file = 1;
                } else {
                    delete_file = 0;
                }

                  $.ajax({
                      type: "POST",
                      url: "update_ftp_connection",
                      data: {
                        "_token": "{{ csrf_token() }}",
                        "ftp_conn_id":ftp_conn_id,
                        "connection_name": connection_name,
                        "server_type": server_type,
                        "host": host,
                        "port": port,
                        "connection_mode":connection_mode,
                        "transfer_file": transfer_file,
                        "mode_of_file": mode_of_file,
                        "keep_alive":keep_alive,
                        "lock_file":lock_file,
                        "delete_file":delete_file,

                      },
                      success: function(data) {

                          $.msgBox({
                              title: "Message",
                              content: data,
                              type: "info",
                          });
                          $('#connection-table-form')[0].reset();
                          $('#update,#cancel').hide();
                          $("#show_all_connections").trigger("click");
                          $("#show_all").trigger("click");
                          $('#list-of-connection').show();
                      },

                      beforeSend: function() {},
                      error: function() {
                          $.msgBox({
                              title: "Alert",
                              content: "Something went wrong while update",
                              type: "alert",
                          });
                      }
                  });
              }

              $(document).on('dblclick', '.double_click_connection', function() {

                var id = $(this).attr('id');
                ftp_conn_id = temp_results[id].ftp_conn_id;
                console.log("ftp_conn_id edit",ftp_conn_id);
                $('#connection_name').val(temp_results[id].connection_name);
                $('#server_type').val(temp_results[id].server_type);
                $('#host').val(temp_results[id].host);
                $('#port').val(temp_results[id].port);
                $('#connection_mode').val(temp_results[id].connection_mode);
                $('#transfer_file').val(temp_results[id].transfer_type);
                $('#mode_of_file').val(temp_results[id].append_mode);
                $('#created_by').val(temp_results[id].created_by);
                $('#created_date').val(temp_results[id].created_at);
                $('#updated_by').val(temp_results[id].updated_by);
                $('#updated_at').val(temp_results[id].updated_at);

                if (temp_results[id].keep_alive == 0) {
                    $('#keep_alive').prop('checked', false);
                } else {
                    $('#keep_alive').prop('checked', true);
                }

                if (temp_results[id].lock_file == 0) {
                    $('#lock_file').prop('checked', false);
                } else {
                    $('#lock_file').prop('checked', true);
                }

                if (temp_results[id].delete_file == 0) {
                    $('#delete_file').prop('checked', false);
                } else {
                    $('#delete_file').prop('checked', true);
                }


                $('#connection_table,#update,#cancel').show();
                $('#list-of-connection,#back,#search,#search-para-connection').hide();

              });

              $(document).on('click', '.edit_connection', function() {

                  var id = $(this).attr('id');
                  ftp_conn_id = temp_results[id].ftp_conn_id;
                  console.log("ftp_conn_id edit",ftp_conn_id);
                  $('#connection_name').val(temp_results[id].connection_name);
                  $('#server_type').val(temp_results[id].server_type);
                  $('#host').val(temp_results[id].host);
                  $('#port').val(temp_results[id].port);
                  $('#connection_mode').val(temp_results[id].connection_mode);
                  $('#transfer_file').val(temp_results[id].transfer_type);
                  $('#mode_of_file').val(temp_results[id].append_mode);
                  $('#created_by').val(temp_results[id].created_by);
                  $('#created_date').val(temp_results[id].created_at);
                  $('#updated_by').val(temp_results[id].updated_by);
                  $('#updated_at').val(temp_results[id].updated_at);

                  if (temp_results[id].keep_alive == 0) {
                      $('#keep_alive').prop('checked', false);
                  } else {
                      $('#keep_alive').prop('checked', true);
                  }

                  if (temp_results[id].lock_file == 0) {
                      $('#lock_file').prop('checked', false);
                  } else {
                      $('#lock_file').prop('checked', true);
                  }

                  if (temp_results[id].delete_file == 0) {
                      $('#delete_file').prop('checked', false);
                  } else {
                      $('#delete_file').prop('checked', true);
                  }

                  $('#connection_table,#update,#cancel').show();
                  $('#list-of-connection,#back,#search,#search-para-connection').hide();

              });


              $(document).on('click', '#update', function() {
                  if ($('#connection-table-form').valid()) {

                      $.msgBox({
                          title: "Confirmation",
                          content: "Do you want to update the mail connection",
                          type: "confirm",
                          buttons: [{
                              value: "Yes"
                          }, {
                              value: "No"
                          }],
                          success: function(result) {
                              if (result == "Yes") {
                                  update_parent_data();
                              } else {

                              }
                          }
                      });
                  }
              });

              $(document).on('click', '#back', function() {
                  $('#search-para-connection,#search').show();
                  $('#list-of-connection,#connection_table,#back').hide();
                  $('#show_all').show();
              });

              $(document).on('click', '#search', function() {
                display_ftp_connection();
              });

              $(document).on('click', '#show_all', function() {
                $('#connection_table,#search-para-connection,#search').hide();
                $('#list-of-connection,#back').show();
                showall();
              });

              $(document).on('click', '.delete_connection', function() {
                  var msg = "Do you want to delete the </br> FTP Connection";
                  var ftp_conn_id = $(this).attr('id');

                  $.msgBox({
                      title: "Confirm",
                      content: msg,
                      type: "confirm",
                      buttons: [{
                          value: "Yes"
                      }, {
                          value: "No"
                      }],
                      success: function(result) {
                          if (result == "Yes") {

                              $.ajax({
                                  type: "POST",
                                  url: "ftp_connection_delete",
                                  data: {
                                      "_token": "{{ csrf_token() }}",
                                      "ftp_conn_id": ftp_conn_id,
                                  },
                                  success: function(data) {
                                    showall();
                                      $.msgBox({
                                          title: "Message",
                                          content: data,
                                          type: "info",
                                      });

                                  },
                                  beforeSend: function() {

                                  },
                                  error: function() {

                                      $.msgBox({
                                          title: "Message",
                                          content: "Can not Delete the Connection..<br> Connection has been assigned to FTP Addresses ",
                                          type: "info",
                                      });

                                  }
                              });


                          } else {

                          }
                      }
                  });

              });

              $(document).on('click', '#cancel', function() {
                  $('#connection_table,#update,#cancel').hide();
                  $('#list-of-connection,#back').show();

              });
          });

          $(window).load(function() {

            $("#setups").trigger('click');
            $("#communication").trigger('click');
            $("#ftp").trigger('click');
            $("#ftp_connection").trigger('click');

              var fetch_search_connection_name_display;
              $('input[name="search_connection_name"]').autoComplete({
                  minChars: 1,
                  source: function(term, response) {
                      try {
                          fetch_search_connection_name_display.abort();
                      } catch (e) {}
                      fetch_search_connection_name_display = $.getJSON('communication_controller/autocomplete_ftp_connection_name', {
                      fetch_search_connection_name: term
                      }, function(data) {
                          response(data);
                      });
                  }
              });

              var fetch_search_host_display;
              $('input[name="search_host"]').autoComplete({
                  minChars: 1,
                  source: function(term, response) {
                      try {
                          fetch_search_host_display.abort();
                      } catch (e) {}
                      fetch_search_host_display = $.getJSON('communication_controller/autocomplete_ftp_host', {
                          fetch_search_host: term
                      }, function(data) {
                          response(data);
                      });
                  }
              });
              var fetch_search_server_type_display;
              $('input[name="search_server_type"]').autoComplete({
                  minChars: 1,
                  source: function(term, response) {
                      try {
                          fetch_search_server_type_display.abort();
                      } catch (e) {}
                      fetch_search_server_type_display = $.getJSON('communication_controller/autocomplete_ftp_server_type', {
                          fetch_search_server_type: term
                      }, function(data) {
                          response(data);
                      });
                  }
              });
          });

      </script>
   </div>

</div>
@stop

@section('lower_band')
<div class="col-xs-12 lower_band">
   <div class="bottom_buttons">
      <div  id="search_button">
         <button class="headerbuttons" id="search" type="button">Search</button>
      </div>
      <div  id="back_buttons">
         <button class="headerbuttons" id="back" type="button">Back</button>
      </div>
      <div id="update_button" style="float:right">
         <button class="headerbuttons" id="update" type="button">Update</button>
         <button class="headerbuttons" id="cancel" type="button">Cancel</button>
      </div>
   </div>
</div>
@stop
