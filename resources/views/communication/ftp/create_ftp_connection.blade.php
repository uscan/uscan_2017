@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/communication.css">
<script src="js/communication_validation.js"></script>
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="col-xs-4 display-title">
      Create FTP Connection
   </div>
</div>
@stop

@section('content')
<div class="col-xs-12 content">
   <ul class="nav nav-tabs">
      <li class="active"><a class="tab_one"  data-toggle="tab" href="#tab_one">Basic Details</a></li>
   </ul>
   <div class="tab-content">
      <div id="tab_one" class="tab-pane fade in active custom_tab">
         <form id="ftp-connection-table-form" autocomplete="off" class="connection-table">
            <table style="width:40%;">
               <tr>
                  <td>Connection Name</td>
                  <td><input type="text"  id = "connection_name" name="connection_name"></td>
               </tr>
               <tr>
                  <td>Server Type</td>
                  <td><select id="server_type" name="server_type">
                    <option value="0">Select</option>
                    <option value="Standard FTP Server">Standard FTP Server</option>
                  </select></td>
               </tr>
               <tr>
                  <td>Host</td>
                  <td><input type="text" name="host" id="host"></td>
               </tr>
               <tr>
                  <td>Port</td>
                  <td><input type="text" name="port" id="port"></td>
               </tr>

               <tr>
                 <td>Connection Mode</td>
                 <td><select id="connection_mode" name="connection_mode">
                   <option value="0">Select</option>
                   <option value="Active">Active</option>
                   <option value="Passive">Passive</option>

                 </select></td>
               </tr>
               <tr>
                 <td>Keep Alive</td>
                 <td><input type="checkbox" name="keep_alive" id="keep_alive" value=""></td>
               </tr>
               <tr>
                 <td>Transfer Type</td>
                 <td><select id="transfer_file" name="transfer_file">
                   <option value="0">Select</option>
                   <option value="Binary">Binary</option>
                   <option value="ASCII">ASCII</option>
                 </select></td>
               </tr>
               <tr>
                 <td>Lock File</td>
                 <td><input type="checkbox" name="lock_file" id="lock_file" value=""></td>
               </tr>
               <tr>
                 <td>Mode of file Transfer</td>
                 <td><select id="mode_of_file" name="mode_of_file">
                   <option value="0">Select</option>
                   <option value="Append">Append</option>
                   <option value="Overwrite">Overwrite</option>
                 </select></td>
               </tr>
               <tr>
                 <td>Delete File</td>
                 <td><input type="checkbox" name="delete_file" id="delete_file" value=""></td>
               </tr>
            </table>
         </form>
      </div>
      <!-- <div class="">
        <form class="" action="index.html" method="post">
          <table>
            <tr>
              <td>Request Notification</td>
              <td><input type="checkbox" name="request_notification" id="request_notification" value=""></td>
            </tr>
            <tr>
              <td>Proxy Mode</td>
              <td><select class="proxy_mode" name="proxy_mode">
                <option value="">Select</option>
                <option value="">No Proxy</option>
                <option value="">Specific Proxy</option>
                <option value="">Proxy Group</option>
              </select></td>
            </tr>
            <tr>
              <td>Dump Messages</td>
              <td><input type="checkbox" name="dump_message" id="dump_message" value=""></td>
            </tr>
          </table>
        </form>
      </div> -->
   </div>

   <script type="text/javascript">
      jQuery.validator.addMethod('selectcheck', function(value) {
          return (value != '0');
      }, "");

      $(".initial_disable").prop('disabled', true);

      $(document).on('click', '#save_button', function() {

          if ($('#ftp-connection-table-form').valid()) {

              var connection_name = $('#connection_name').val();
              var server_type = $('#server_type').val();
              var host = $('#host').val();
              var port = $('#port').val();
              var connection_mode = $('#connection_mode').val();
              var transfer_file = $('#transfer_file').val();
              var mode_of_file = $('#mode_of_file').val();

              if ($('#keep_alive').is(":checked")) {
                  keep_alive = 1;
              } else {
                  keep_alive = 0;
              }

              if ($('#lock_file').is(":checked")) {
                  lock_file = 1;
              } else {
                  lock_file = 0;
              }
              if ($('#delete_file').is(":checked")) {
                  delete_file = 1;
              } else {
                  delete_file = 0;
              }

              $.ajax({
                  type: "POST",
                  url: "save_ftp_connection",
                  data: {
                      "_token": "{{ csrf_token() }}",
                      "connection_name": connection_name,
                      "server_type": server_type,
                      "host": host,
                      "port": port,
                      "connection_mode":connection_mode,
                      "transfer_file": transfer_file,
                      "mode_of_file": mode_of_file,
                      "keep_alive":keep_alive,
                      "lock_file":lock_file,
                      "delete_file":delete_file,
                  },
                  success: function(data) {

                      $.msgBox({
                          title: "Message",
                          content: data,
                          type: "info",
                      });

                      $('#ftp-connection-table-form')[0].reset();
                      $('#proxy_mode').html('<option value="0">Select</option>');

                  },
                  beforeSend: function() {

                  },
                  error: function() {

                  }

              });
          } else {}
      });

      $(document).on('click', '#cancel', function(){

                $('#connection-table-form')[0].reset();
                $('#proxy_mode').html('<option value="0">Select</option>');

             });

      $(window).load(function() {
          $("#setups").trigger('click');
          $("#communication").trigger('click');
          $("#ftp").trigger('click');
          $("#ftp_connection").trigger('click');

      });
   </script>
</div>
@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
   <button class="headerbuttons" type="button" style="float:right;" id="cancel">Cancel</button>
   <button id="save_button" type="button" class="headerbuttons" style="float:right;" type="submit">Save</button>
</div>
@stop
