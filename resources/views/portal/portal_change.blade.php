@extends('layouts.uscan_master_page')
@section('header')
   <!-- <link rel="stylesheet" href="css/screen.css"> -->
   <script src="js/portal_validation.js"></script>
   <link rel="stylesheet" type="text/css" href="css/portal.css">
@stop

@section('upper_band')
      <div class="col-xs-12 upper_band">
          <div class="col-xs-4 display-title">
               Change Portal Master
          </div>
           <div class="col-xs-2" style="float:right;text-align:center;line-height:30px;">
             <span  id="show_all" style="">Show All Portals</span>
           </div>
      </div>
@stop

@section('content')
 <div class="col-xs-12 content">
   <div id="search-para">
     <form autocomplete="off">
      <table style="width:40%;">
        <tr>
          <td class="details_color">Search Parameters</td>
        </tr>

       <tr>
          <td>Customer Name</td>
          <td><input type="text" name="search_customer_name" id="search_customer_name_id"></td>
       </tr>
       <tr>
          <td>Plant Name</td>
          <td><input type="text" name="search_plant_name" id="search_plant_name_id"></td>
       </tr>
       <tr>
        <td>Portal Name</td>
        <td><input type="text" name="search_portal_name_id" id="search_portal_name_id"></td>
       </tr>

       <tr>
       <td>Portal URL</td>
       <td><input type="text" name="search_portal_url_name" id="search_portal_purl"></td>
      </tr>
        </table>
      </form>
  </div>

     <div id="list-of-portal"  class="div1">
        <table style="width:100%;" class="tab">
        </table>
      </div>

      <div class="" id ="create_page">
           <ul class="nav nav-tabs">
                 <li class="active"><a class="tab_one"  data-toggle="tab" href="#tab_one">Details</a></li>
                 <li><a class="tab_two"  data-toggle="tab" href="#tab_two">Additional Details</a></li>
           </ul>
           <div class="tab-content">

              <div id="tab_one" class="tab-pane fade in active custom_tab">
                <form id="portal-table" autocomplete="off">
                  <table style="width:50%;">
          								    <tr>
          									   <td>Customer Name</td>
          									   <td><select id = "customer_id" name="customer_id">
                               </select> </td>
          							      </tr>
          									  <tr>
          										  <td>Plant Name</td>
                                <td><select id="plant_id" name="plant_id" >
                                 </select> </td>
          										</tr>
          										<tr>
          											<td>Portal Name</td>
          											<td><input type="text" name="portal_name" id="portal_name"></td>
          										</tr>
          										<tr>
          											<td>Portal URL</td>
          											<td><input type="text" name="portal_url" id="portal_url"></td>
          										</tr>
          										<tr>
          											<td>User ID</td>
          											<td><input type="text" name="portal_user_id" id="portal_user_id"></td>
          										</tr>
          										<tr>
          											<td>Password</td>
          											<td><input type="text" name="portal_password" id="portal_password"></td>
          										</tr>
          										<tr>
          											<td>Repeat Password</td>
          											<td><input type="text" name="portal_repeat_password" id="portal_repeat_password"></td>
          										</tr>
          										<tr>
          											<td>Max Attempts</td>
          											<td><input type="text" name="max_attempt" id="max_attempt" default></td>
          										</tr>
          										<tr>
          											<td>Password Expires</td>
          											<td><input type="date" name="password_expiry_date" id="password_expiry_date"></td>
          										</tr>
                              <tr>
                                <td>Locked</td>
          											<td><input type="checkbox" name="lockname" id="lockid"></td>
                              </tr>
                              <tr>
                                <td>Active</td>
                                <td><input type="checkbox" name="activeid" id="activeid"></td>
                              </tr>
          		           	</table>
          		        </form>
                   </div>

              <div id="tab_two" class="tab-pane fade custom_tab">
                <form autocomplete="off">
                  <table style="width:50%;">
                  <tr>
                      <td>Created By</td>
                      <td><input type="text" name="created_by" id="created_by" disabled></td>
                  </tr>
                  <tr>
                    <td>Created Date</td>
                    <td><input type="text" name="created_at" id="created_at" disabled></td>
                  </tr>
                  <tr>
                    <td>Modified By</td>
                    <td><input type="text"  name="updated_by" id="updated_by" disabled></td>
                  </tr>
                  <tr>
                    <td>Modified Date</td>
                    <td><input type="text" name="updated_at" id="updated_at" disabled></td>
                  </tr>
                  </table>
                </form>
              </div>
        </div>
  </div>



<script type="text/javascript">

$(document).ready(function(){
  var search_results,results_showall,portal_id_ref;
  var search_click,show_all_click,no_results;
  results_found = "YES";


  search_click = "0";
  show_all_click = "0";
  jQuery.validator.addMethod('selectcheck', function(value) {
    return (value != '0');
  }, "");
// getting customer and plant starts here
        $.ajax({
  							type: "GET",
  							url: "cust_name",
  							dataType: "json",
  							success: function(data) {
  								      var results = data;
  								      console.log(JSON.stringify(data));
  								      for (var i = 0; i < results.length; i++)
  								        {
  									        if (i == '0')
  									          {
  										          $('#customer_id').append("<option id =\"null\" value=\"0\">Select</option>");
  									          }
  								                var customer_name = results[i].customer_name
  								                var customer_id           = results[i].customer_id
  								                $('#customer_id').append("<option id ="+ customer_id +" value="+customer_id+">"+customer_name+"</option>");
  								         }
  						           },
  							beforeSend: function() {
  							                             },
  							error: function() {
  							     results_found == "no";
  							}
            });
        //
        //     $.ajax({
  			// 									type: "GET",
  			// 									url: "plant_name",
  			// 									dataType: "json",
  			// 									success: function(data) {
  			// 										var results = data;
  			// 										console.log(JSON.stringify(data));
  			// 										for (var i = 0; i < results.length; i++)
  			// 										{
  			// 											 if (i == '0')
  			// 											 {
  			// 												 $('#plant_id').append("<option id =\"null\" value=\"0\">Select</option>");
  			// 											 }
  			// 										var plant_name = results[i].plant_name
  			// 										var plant_id           = results[i].plant_id
  			// 										 $('#plant_id').append("<option id ="+ plant_id +" value="+plant_id+">"+plant_name+"</option>");
  			// 										}
  			// 									},
  			// 									beforeSend: function() {
  			// 									},
  			// 									error: function() {
  			// 										alert('Data not found...!!!');
  			// 									}
  			// 					});
// getting customer and plant ends here

$('#customer_id').on('change',function(){

 var customer_id = $(this).val();

 $('#plant_id').empty();

 if(customer_id != 0){


            $.ajax({
                type: "POST",
                url: "task_get_plant_for_customer",
                // dataType: "json",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "customer_id": customer_id,
                },

                success: function(data) {

                  if (data != 0)
                  {
                  for (var i = 0; i < data.length; i++)
                     {
                        if (i == '0')
                        {
                          $('#plant_id').append("<option id =\"null\" value=\"0\">Select</option>");
                        }
                           var plant_name         = data[i].plant_name
                           var plant_id           = data[i].plant_id
                            $('#plant_id').append("<option id ="+ plant_id +" value="+plant_id+">"+plant_name+"</option>");

                     }

                   $('#portal_id').html('<option value="0">Select Plant</option>');
                 }
                 else {
                    $('#plant_id').append("<option id =\"null\" value=\"0\">No plant available</option>");

                 }
                },
                beforeSend: function() {

                },
                error: function() {

                }
            });

        }
        else
        {

            $('#plant_id').html('<option value="0">Select Customer</option>');

        }

});

// show_all function starts here
  function show_all() {
  			$.ajax({
  				type: "GET",
  				url: "portal_change1",
  				dataType: "json",
  				success: function(data) {
  					 var results = data;
             results_showall = data;

              if(results != 0)
                 {
  					      console.log(JSON.stringify(results));
  					      $('.tab tr').remove();
                  $('.tab').append('<tr style="background-color: #D3D3D3;"><th style="display:none;">Portal id</th><th>Customer name</th><th>Plant name</th><th>Portal name</th><th>Portal url</th><th>User id</th><th>Locked</th><th>Active</th><th>Edit</th><th>Delete</th></tr>');

                  if (results.length > 12) {

                      $('#list-of-portal').addClass("scroll");

                  } else {
                      $('#list-of-portal').removeClass("scroll");
                  }

                    for (var i = 0; i < results.length; i++)
                    {
                      if (results[i].locked == 0)
                       {
                         locked_check = '<input type="checkbox" disabled readonly>';
                       }
                      else {
                         locked_check = '<input type="checkbox" checked disabled readonly>';
                       }

                      if (results[i].active == 0)
                       {
                        active_check = '<input type="checkbox" disabled readonly>';
                       }
                      else {
                        active_check = '<input type="checkbox" checked disabled readonly>';
                      }

                       $('.tab').append('<tr data-id="' + i + '" id="'+ results[i].portal_id  +'" class=" showall_tr rows"><td style="display:none;">'+ results[i].portal_id  + '</td><td>' + results[i].customer_name  +'</td><td>' + results[i].plant_name + '</td><td>' + results[i].portal_name + '</td><td>' + results[i].portal_url +'</td><td>'+ results[i].portal_user_id +
                       '</td><td>'+ locked_check +'</td><td>'+ active_check + '</td><td><button id="'+i+'" type="button"  class="edit_showall edit_color btn btn-default glyphicon glyphicon-edit" aria-label="Left Align"></button></td><td><button id="delete_showall" type="button" class="delete_color btn btn-default" aria-label="Left Align"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td></tr>')
    			       }
             }
             else
             {
                      $('#search-para').show();
                      $('#filter,.tab').hide();
                    //  $('.tab').hide();
              }
  				},
  				beforeSend: function() {
  				},
  				error: function() {
  				}
  			});
     }
// show_all function ends here


// search_show function starts here
function show_search() {
            var portal_id = $('#portal_id').val();
            var search_portal_name_id = $('#search_portal_name_id').val();
            var search_portal_purl = $('#search_portal_purl').val();
            var search_customer_name_id = $('#search_customer_name_id').val();
            var search_plant_name_id = $('#search_plant_name_id').val();
            if ((search_portal_name_id == "") && (search_portal_purl == "") && (search_customer_name_id == "") && (search_plant_name_id == ""))
              {
                $('#search-para,#filter').show();
                $('#back2').hide();
                //$('#filter').show();

                  $.msgBox({
                           title:"Alert",
                           content:"Search Criteria Not Available",
                           type:"alert",
                          });
              }
            else
            {
              $.ajax({
                type: "POST",
                url: "portal_change_show",
                data: {
                "_token":"{{ csrf_token() }}",
                "search_portal_name_id":search_portal_name_id,
                "search_portal_purl":search_portal_purl,
                "search_customer_name_id":search_customer_name_id,
                "search_plant_name_id":search_plant_name_id,
                },
                success: function(data) {
                   var results = data;
                   if(results != 0)
                {
                  search_results = data;
                  $('.tab tr').remove();
                  $('.tab').append('<tr style="background-color: #D3D3D3;"><th style="display:none;">Portal id</th><th>Customer name</th><th>Plant name</th><th>Portal name</th><th>Portal url</th><th>User id</th><th>Locked</th><th>Active</th><th>Edit</th><th>Delete</th></tr>');

                  for (var i = 0; i < results.length; i++)
                  {
                  if (results[i].locked == 0)
                   {
                    locked_check = '<input type="checkbox" disabled readonly>';
                   }
                  else
                  {
                    locked_check = '<input type="checkbox" checked disabled readonly>';
                  }
                  if (results[i].active == 0)
                  {
                    active_check = '<input type="checkbox" disabled readonly>';
                  }
                  else
                  {
                    active_check = '<input type="checkbox" checked disabled readonly>';
                  }
                  $('.tab').append('<tr data-id="' + i + '" id="'+ results[i].portal_id  +'" class="search_tr rows"><td style="display:none;">'+ results[i].portal_id  + '</td><td>' + results[i].customer_name  +'</td><td>' + results[i].plant_name + '</td><td>' + results[i].portal_name + '</td><td>' + results[i].portal_url +'</td><td>'+ results[i].portal_user_id +
                    '</td><td>'+ locked_check +'</td><td>'+ active_check + '</td><td><button id="'+i+'" type="button" style="color:#003c00;"class="edit_search btn btn-default glyphicon glyphicon-edit" aria-label="Left Align"></button></td><td><button id="delete_search" type="button" class="btn btn-default" aria-label="Left Align"><span style="color:#990000;" class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td></tr>')         }
                //  $('#search-para').hide();
                  $('#filter,#search-para').hide();
                  $('.tab').show();

              }
              else
              {
                   $('#search-para,#show_all').show();
                   $('#filter,.tab').hide();
                   results_found ="no";

                  //  $('#search-para').show();
                  //  $('#filter').hide();
                  //  $('#show_all').show();
                  //  $('.tab').hide();

                   $.msgBox({
                            title:"Alert",
                            content:"Data not found",
                            type:"alert",
                           });
               }
            },
                beforeSend: function() {
                 },
                error: function() {

                  $.msgBox({
                           title:"Alert",
                           content:"Data not found",
                           type:"alert",
                          });

                          $('#search-para,#filter').show();
                          $('#back2').hide();
                          //$('#filter').show();

                 }
            });
    }
}
// search_show function ends here


// delete_showall function starts here
		$(document).on('click','#delete_showall',function(){
   	var portal_id=$(this).closest('.rows').attr('id');
      $.msgBox({
                   title: "Confirm",
                   content: "Do you want to delete the portal",
                   type: "confirm",
                 buttons: [{ value: "Yes" }, { value: "No" }],
                 success: function (result) {
                 if (result == "Yes") {
      $.ajax({
        type: "POST",
        url: "portal_delete",
        data:  {
        "_token":"{{ csrf_token() }}",
        "portal_id":portal_id,
        },
        success: function() {
        show_all();
        },
        beforeSend: function() {
        },
        error: function() {
          $.msgBox({
              title: "Message",
              content: "Unable to delete the Portal</br>Please delete the dependent Task",
              type: "info",
          });
        }
      });
    }
      else {

      if (results_found == "no")
      {
        show_all();
      }
      }
      }
      });
});
// delete_showall function ends here


// delete_search function starts here
    $(document).on('click','#delete_search',function(){
    var portal_id=$(this).closest('.rows').attr('id');
           $.msgBox({
                   title: "Confirm",
                   content: "Do you want to delete the portal",
                   type: "confirm",
                   buttons: [{ value: "Yes" }, { value: "No" }],
                   success: function (result) {
                  if (result == "Yes") {
                     $.ajax({
                       type: "POST",
                       url: "portal_delete",
                       data:  {
                       "_token":"{{ csrf_token() }}",
                       "portal_id":portal_id,
                     },
                     success: function() {
                     show_search();
                     },
                     beforeSend: function() {
                     },
                     error: function() {
                           $.msgBox({
                               title: "Alert",
                               content: "Unable to delete the Portal</br>Please delete the dependent Task",
                               type: "alert",
                           });
                     }
                   });
                  }
                  else
                  {
                    if (results_found == "no")
                    {
                      show_search();
                    }

                  }
                 }
            });
        });
// delete_search function ends here



$(document).on('click', '#show_all', function() {
  search_click = "0";
  show_all_click = "1";
        show_all();
        $('#search-para,#filter,#show_all').hide();
        $('#back2,.tab').show();

        // $('#search-para').hide();
        // $('#back2').show();
        // $('.tab').show();
        // $('#filter,#show_all').hide();
});

$(document).on('click', '#filter', function() {
  search_click = "1";
  show_all_click = "0";
  $('.tab tr').remove();
  $('#search-para,#filter').hide();
  $('#back2,.tab').show();
  show_search();

  // $('#search-para').hide();
  // $('#back2').show();
  // $('.tab').show();
  // $('#filter').hide();

});


$(document).on('dblclick', '.showall_tr', function() {
    var id = $(this).data("id");
    customer_id = results_showall[id].customer_id;
    if(customer_id != 0){


               $('#plant_id option').remove();
               $.ajax({
                   type: "POST",
                   url: "task_get_plant_for_customer",
                   // dataType: "json",
                   data: {
                       "_token": "{{ csrf_token() }}",
                       "customer_id": customer_id,
                   },

                   success: function(data) {

                     if (data != 0)
                     {
                     for (var i = 0; i < data.length; i++)
                        {
                           if (i == '0')
                           {
                             $('#plant_id').append("<option id =\"null\" value=\"0\">Select</option>");
                           }
                              var plant_name         = data[i].plant_name
                              var plant_id           = data[i].plant_id
                               $('#plant_id').append("<option id ="+ plant_id +" value="+plant_id+">"+plant_name+"</option>");

                        }

                      $('#portal_id').html('<option value="0">Select Plant</option>');
                    }
                    else {
                       $('#plant_id').append("<option id =\"null\" value=\"0\">No plant available</option>");

                    }

                    $('#plant_id').val(results_showall[id].plant_id);
                    $('.tab_one').click();
                    $('#create_page,#update_button').show();
                    $('#back1,#back2,.tab,#show_all,#filter').hide();

                    // $('#create_page').show();
                    // $('#back1').hide();
                    // $('#back2').hide();
                    // $('.tab').hide();
                    // $('#show_all').hide();
                    // $('#update_button').show();
                    // $('#filter').hide();
                   },
                   beforeSend: function() {

                   },
                   error: function() {

                   }
               });

           }
           else
           {

               $('#plant_id').html('<option value="0">Select Customer</option>');

           }




    portal_id_ref = results_showall[id].portal_id;
    $('#customer_id').val(results_showall[id].customer_id);

    $('#portal_name').val(results_showall[id].portal_name);
    $('#portal_url').val(results_showall[id].portal_url);
    $('#portal_user_id').val(results_showall[id].portal_user_id);
    $('#portal_password').val(results_showall[id].portal_password);
    $('#portal_repeat_password').val(results_showall[id].portal_repeat_password);
    $('#max_attempt').val(results_showall[id].max_attempt);
    $('#password_expiry_date').val(results_showall[id].password_expiry_date);
    $('#created_by').val(results_showall[id].created_by);
    $('#created_at').val(results_showall[id].created_at);
    $('#updated_by').val(results_showall[id].updated_by);
    $('#updated_at').val(results_showall[id].updated_at);


    if (results_showall[id].locked == 0) {
        $('#lockid').prop('checked', false);
    } else {
        $('#lockid').prop('checked', true);
    }

    if (results_showall[id].active == 0) {
        $('#activeid').prop('checked', false);
    } else {
        $('#activeid').prop('checked', true);
    }
});

$(document).on('dblclick', '.search_tr', function() {


    var id = $(this).data("id");
    customer_id = search_results[id].customer_id;
    if(customer_id != 0){

               $('#plant_id option').remove();
               $.ajax({
                   type: "POST",
                   url: "task_get_plant_for_customer",
                   // dataType: "json",
                   data: {
                       "_token": "{{ csrf_token() }}",
                       "customer_id": customer_id,
                   },

                   success: function(data) {

                     if (data != 0)
                     {
                     for (var i = 0; i < data.length; i++)
                        {
                           if (i == '0')
                           {
                             $('#plant_id').append("<option id =\"null\" value=\"0\">Select</option>");
                           }
                              var plant_name         = data[i].plant_name
                              var plant_id           = data[i].plant_id
                               $('#plant_id').append("<option id ="+ plant_id +" value="+plant_id+">"+plant_name+"</option>");

                        }

                      $('#portal_id').html('<option value="0">Select Plant</option>');
                    }
                    else {
                       $('#plant_id').append("<option id =\"null\" value=\"0\">No plant available</option>");

                    }
                    $('#plant_id').val(search_results[id].plant_id);
                    $('.tab_one').click();
                    $('#create_page,#update_button').show();
                    $('#back1,#back2,.tab,#show_all,#filter').hide();

                    // $('#create_page').show();
                    // $('#back1').hide();
                    // $('#back2').hide();
                    // $('.tab').hide();
                    // $('#show_all').hide();
                    // $('#update_button').show();
                    // $('#filter').hide();
                   },
                   beforeSend: function() {

                   },
                   error: function() {

                   }
               });

           }
           else
           {

               $('#plant_id').html('<option value="0">Select Customer</option>');

           }



    portal_id_ref = search_results[id].portal_id;
    $('#customer_id').val(search_results[id].customer_id);

    $('#portal_name').val(search_results[id].portal_name);
    $('#portal_url').val(search_results[id].portal_url);
    $('#portal_user_id').val(search_results[id].portal_user_id);
    $('#portal_password').val(search_results[id].portal_password);
    $('#portal_repeat_password').val(search_results[id].portal_repeat_password);
    $('#max_attempt').val(search_results[id].max_attempt);
    $('#password_expiry_date').val(search_results[id].password_expiry_date);
    $('#created_by').val(search_results[id].created_by);
    $('#created_at').val(search_results[id].created_at);
    $('#updated_by').val(search_results[id].updated_by);
    $('#updated_at').val(search_results[id].updated_at);

    if (search_results[id].locked == 0) {
        $('#lockid').prop('checked', false);
    } else {
        $('#lockid').prop('checked', true);
    }

    if (search_results[id].active == 0) {
        $('#activeid').prop('checked', false);
    } else {
        $('#activeid').prop('checked', true);
    }
});


//edit_showall onclick action starts here
$(document).on('click', '.edit_showall', function() {
 var id =  $(this).attr('id');
  customer_id = results_showall[id].customer_id
  if(customer_id != 0){

             $('#plant_id option').remove();
             $.ajax({
                 type: "POST",
                 url: "task_get_plant_for_customer",
                 // dataType: "json",
                 data: {
                     "_token": "{{ csrf_token() }}",
                     "customer_id": customer_id,
                 },

                 success: function(data) {

                   if (data != 0)
                   {
                   for (var i = 0; i < data.length; i++)
                      {
                         if (i == '0')
                         {
                           $('#plant_id').append("<option id =\"null\" value=\"0\">Select</option>");
                         }
                            var plant_name         = data[i].plant_name
                            var plant_id           = data[i].plant_id
                             $('#plant_id').append("<option id ="+ plant_id +" value="+plant_id+">"+plant_name+"</option>");

                      }

                    $('#portal_id').html('<option value="0">Select Plant </option>');


                  }
                  else {
                     $('#plant_id').append("<option id =\"null\" value=\"0\">No plant available</option>");

                  }

                  $('#plant_id').val(results_showall[id].plant_id);
                  $('.tab_one').click();
                  $('#create_page,#update_button').show();
                  $('#back1,#back2,.tab,#show_all,#filter').hide();

                  // $('#create_page').show();
                  // $('#back1').hide();
                  // $('#back2').hide();
                  // $('.tab').hide();
                  // $('#show_all').hide();
                  // $('#update_button').show();
                  // $('#filter').hide();
                 },
                 beforeSend: function() {

                 },
                 error: function() {

                 }
             });

         }
         else
         {

             $('#plant_id').html('<option value="0">Select Customer</option>');

         }


    portal_id_ref = results_showall[id].portal_id;
    $('#customer_id').val(results_showall[id].customer_id);

    $('#portal_name').val(results_showall[id].portal_name);
    $('#portal_url').val(results_showall[id].portal_url);
    $('#portal_user_id').val(results_showall[id].portal_user_id);
    $('#portal_password').val(results_showall[id].portal_password);
    $('#portal_repeat_password').val(results_showall[id].portal_repeat_password);
    $('#max_attempt').val(results_showall[id].max_attempt);
    $('#password_expiry_date').val(results_showall[id].password_expiry_date);
    $('#created_by').val(results_showall[id].created_by);
    $('#created_at').val(results_showall[id].created_at);
    $('#updated_by').val(results_showall[id].updated_by);
    $('#updated_at').val(results_showall[id].updated_at);

    if (results_showall[id].locked == 0)
    {
        $('#lockid').prop('checked', false);
    }
    else
     {
        $('#lockid').prop('checked', true);
     }

    if (results_showall[id].active == 0)
    {
        $('#activeid').prop('checked', false);
    }
    else
    {
        $('#activeid').prop('checked', true);
    }


});
//edit_showall onclick action ends here


//edit_search onclick action starts here
$(document).on('click', '.edit_search', function() {

     var id =  $(this).attr('id');


     customer_id = search_results[id].customer_id
     if(customer_id != 0){

                $('#plant_id option').remove();
                $.ajax({
                    type: "POST",
                    url: "task_get_plant_for_customer",
                    // dataType: "json",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "customer_id": customer_id,
                    },

                    success: function(data) {

                      if (data != 0)
                      {
                      for (var i = 0; i < data.length; i++)
                         {
                            if (i == '0')
                            {
                              $('#plant_id').append("<option id =\"null\" value=\"0\">Select</option>");
                            }
                               var plant_name         = data[i].plant_name
                               var plant_id           = data[i].plant_id
                                $('#plant_id').append("<option id ="+ plant_id +" value="+plant_id+">"+plant_name+"</option>");

                         }

                       $('#portal_id').html('<option value="0">Select Plant</option>');
                     }
                     else {
                        $('#plant_id').append("<option id =\"null\" value=\"0\">No plant available</option>");

                     }
                     $('#plant_id').val(search_results[id].plant_id);
                     $('.tab_one').click();
                     $('#create_page,#update_button').show();
                     $('#back1,#back2,.tab,#show_all,#filter').hide();

                    //  $('#create_page').show();
                    //  $('#back1').hide();
                    //  $('#back2').hide();
                    //  $('.tab').hide();
                    //  $('#show_all').hide();
                    //  $('#update_button').show();
                    //  $('#filter').hide();


                    },
                    beforeSend: function() {

                    },
                    error: function() {

                    }
                });

            }
            else
            {

                $('#plant_id').html('<option value="0">Select Customer</option>');

            }



             portal_id_ref = search_results[id].portal_id;
             $('#customer_id').val(search_results[id].customer_id);

             $('#portal_name').val(search_results[id].portal_name);
             $('#portal_url').val(search_results[id].portal_url);
             $('#portal_user_id').val(search_results[id].portal_user_id);
             $('#portal_password').val(search_results[id].portal_password);
             $('#portal_repeat_password').val(search_results[id].portal_repeat_password);
             $('#max_attempt').val(search_results[id].max_attempt);
             $('#password_expiry_date').val(search_results[id].password_expiry_date);
             $('#created_by').val(search_results[id].created_by);
             $('#created_at').val(search_results[id].created_at);
             $('#updated_by').val(search_results[id].updated_by);
             $('#updated_at').val(search_results[id].updated_at);

             if (search_results[id].locked == 0) {
                 $('#lockid').prop('checked', false);
             } else {
                 $('#lockid').prop('checked', true);
             }

             if (search_results[id].active == 0) {
                 $('#activeid').prop('checked', false);
             } else {
                 $('#activeid').prop('checked', true);
             }
});
//edit_search onclick action ends here






//Update function starts here
function update_data() {
        var customer_id = $('#customer_id').val();
        var plant_id = $('#plant_id').val();
        var portal_name = $('#portal_name').val();
        var portal_url  = $('#portal_url').val();
        var portal_user_id = $('#portal_user_id').val();
        var portal_password = $('#portal_password').val();
        var portal_repeat_password = $('#portal_repeat_password').val();
        var max_attempt = $('#max_attempt').val();
        var password_expiry_date = $('#password_expiry_date').val();
        var created_at = $('#created_at').val();
        var created_by = $('#created_by').val();
        var updated_at = $('#updated_at').val();
        var updated_by = $('#updated_at').val();
        if ($('#lockid').is(":checked"))
         {   locked = 1; }
         else
         {   locked = 0;  }

        if ($('#activeid').is(":checked"))
         {     active = 1; }
         else
         {    active = 0; }

          $.ajax({
            type: "POST",
            url: "portal_update",
            data: {
                          "_token": "{{ csrf_token() }}",
                          "portal_id": portal_id_ref,
                          "customer_id": customer_id,
                          "plant_id": plant_id,
                          "portal_name": portal_name,
                          "portal_url": portal_url,
                          "portal_user_id": portal_user_id,
                          "portal_password": portal_password,
                          "portal_repeat_password": portal_repeat_password,
                          "max_attempt": max_attempt,
                          "password_expiry_date": password_expiry_date,
                          "locked": locked,
                          "active": active,
                          "created_at":created_at,
                          "created_by":created_by,
                          "updated_at":updated_at,
                          "updated_by":updated_by,

                    },
              success: function(data) {

                $('#create_page').hide();
                $('.tab').show();
                $('#back2').show();
                $.msgBox({
                             title:"Message",
                             content:"Portal updated successfully",
                             type:"info",
                        });
              if (show_all_click == "1")
              {
                  show_all();
              }
              if (search_click == "1")
              {
                show_search();
              }
            },
            beforeSend: function() {

            },
            error: function() {
              $.msgBox({
              title: "Alert",
              content: "Something went wrong while update",
              type:"alert",
            });

            }
          });
      }
//Update function ends here


//onclick update starts here
$(document).on('click', '#update', function() {

if ($('#portal-table').valid()) {

  no_results = "TRUE";
    $.msgBox({
             title: "Confirm",
             content: "Do you want to update the portal",
             type: "confirm",
             buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {
              if (result == "Yes")
                {
                  update_data();
                  $('#create_page,#update_button,#search-para,#filter').hide();

                  // $('#create_page').hide();
                  // $('#update_button').hide();
                  // $('#search-para').hide();
                  // $('#filter').hide();
                }
              else
                {
                  $('#search-para,#create_page,#back1,#update_button').hide();
                  $('#show_all,#back2,.tab').show();

                  //  $('#search-para').hide();
                  //  $('#show_all').show();
                  //  $('#back2').show();
                  //  $('.tab').show();
                  //  $('#create_page').hide();
                  //  $('#back1').hide();
                  //  $('#update_button').hide();

                  if (show_all_click == "1")
                     {
                       show_all();
                     }
                  if (search_click == "1")
                    {
                      show_search();
                    }
                 }
            }
          });
      }
});
//onclick update ends here


$(document).on('click', '#cancel', function() {

  $('#plant_id-error,#customer_id-error,#portal_name-error,#portal_url-error,#portal_user_id-error,#portal_password-error,#portal_repeat_password-error').remove();
  $('#plant_id,#customer_id,#portal_name,#portal_url,#portal_user_id,#portal_password,#portal_repeat_password').removeClass("error");

  $('#search-para,#create_page,#back1,#update_button').hide();
  $('#show_all,#back2,.tab').show();

  // $('#search-para').hide();
  // $('#show_all').show();
  // $('#back2').show();
  // $('.tab').show();
  // $('#create_page').hide();
  // $('#back1').hide();
  // $('#update_button').hide();


  if (show_all_click == "1")
   {
      show_all();
   }
  if (search_click == "1")
  {
   show_search();
  }
});

$('#back1').click(function() {
  $('#search-para,#create_page,#back1').hide();
  $('#show_all,#back2,.tab').show();

  // $('#search-para').hide();
  // $('#show_all').show();
  // $('#back2').show();
  // $('.tab').show();
  // $('#create_page').hide();
  // $('#back1').hide();

});

$('#back2').click(function() {
  $('#back1,#back2,.tab').hide();
  $('#search-para,#show_all,#filter').show();


    // $('#search-para').show();
    // $('#show_all').show();
    // $('.tab').hide();
    // $('#back2').hide();
    // $('#back1').hide();
    // $('#filter,#show_all').show();

});


//Update for show_all ends here



  //Ready function ends here
});

$(window).on('load', function(){
    $("#master_data").trigger('click');
    $("#portal_master").trigger('click');
    $("#create_page,#save_button,#ResetButton").hide();

    // $("#create_page").hide();
    // $("#save_button").hide();
    // $("#ResetButton").hide();

    var xhr;
    $('input[name="search_portal_name_id"]').autoComplete({
        minChars: 1,
        source: function(term, response){
            try { xhr.abort(); } catch(e){}
            xhr = $.getJSON('portal_controller/autocomplete_portal_name', { search_portal_name_id: term }, function(data){ response(data); });
        }
    });


    var xhr2;
    $('input[name="search_portal_url_name"]').autoComplete({
        minChars: 1,
        source: function(term, response){
            try { xhr2.abort(); } catch(e){}
            xhr2 = $.getJSON('portal_controller/autocomplete_portal_url', { search_portal_url: term }, function(data){ response(data); });
        }
    });

    var xhr3;
    $('input[name="search_customer_name"]').autoComplete({
        minChars: 1,
        source: function(term, response){
            try { xhr3.abort(); } catch(e){}
            xhr3 = $.getJSON('portal_controller/autocomplete_customer_name', { search_customer_name_id: term }, function(data){ response(data); });
        }
    });

    var xhr4;
    $('input[name="search_plant_name"]').autoComplete({
        minChars: 1,
        source: function(term, response){
            try { xhr4.abort(); } catch(e){}
            xhr4 = $.getJSON('portal_controller/autocomplete_plant_name', { search_plant_name_id: term }, function(data){ response(data); });
        }
    });

});

</script>
</div>

@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
  <div class="col-xs-2" id="search_button">
   <button class="headerbuttons" id="filter" type="button">Search</button>
   <button class="headerbuttons" id="back1" type="button">Back</button>
   <button class="headerbuttons" id="back2" type="button">Back</button>
 </div>

<div class="col-xs-6">
</div>
    <div class="col-xs-4" id="update_button" style="display:none;">
      <div id="update_search_button">
        <button class="headerbuttons" id="cancel" style="float:right;" type="button">Cancel</button>
        <button class="headerbuttons" id="update" style="float:right;" type="button">Update</button>

      </div>
   </div>
</div>
@stop
