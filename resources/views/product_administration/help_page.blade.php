

@extends('layouts.uscan_master_page')
@section('header')
   <!-- <link rel="stylesheet" href="css/screen.css"> -->
   <link rel="stylesheet" type="text/css" href="css/product_administration.css">
   <style media="screen">
     #wrapper{
       width: 100%;
       height: 100%;
       overflow: auto;
     }
   </style>
@stop

@section('upper_band')
      <div class="col-xs-12 upper_band">
          <div class="col-xs-4 display-title">

          </div>
           <div class="col-xs-2" style="float:right;text-align:center;line-height:30px;">
             <!-- <span  id="show_all" style="">go to page</span> -->
             <a href="document/USCAN User Manual WIP V1.0 03_06_2017.pdf" target="_blank">Open in New tab</a>
           </div>
      </div>
@stop

@section('content')
 <div class="col-xs-12 content">
   <div id="wrapper" >
 <!-- iframe will be continually destroyed and re-created here -->
</div>

 </div>
<script type="text/javascript">

  function openHelp() {
    window.open("document/USCAN User Manual WIP V1.0 03_06_2017.pdf");
  }

  openHelp();

  $(document).ready(function(){
    $('<iframe id="iFrame" scrolling="no" src="document/USCAN User Manual WIP V1.0 03_06_2017.pdf" width="100%" height="100%,"_blank"></iframe>').appendTo('#wrapper');

  });

  $(window).load(function() {
    $("#administration").trigger('click');
  });

</script>

@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
  <!-- <div  id="back_buttons">
      <button type = "button" style = "display:none" id="back">Back</button>
</div> -->
</div>
@stop
