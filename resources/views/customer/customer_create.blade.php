@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" type="text/css" href="css/customer_create.css">
<script src="js/customer_validation.js"></script>
@stop
@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="col-xs-4 display-title">
      <span>Create Customer Master</span>
   </div>
</div>
@stop
@section('content')
<div class="col-xs-12 content">
   <ul class="nav nav-tabs">
      <li class="active"><a class="tab_one" data-toggle="tab" href="#tab_one">Details</a></li>
   </ul>
   <div class="customer_type">
      <table id = "type" style="width:40%;">
         <tr>
            <td>Type</td>
            <td>
               <select id="parent-child">
               </select>
            </td>
         </tr>
         <tr>
            <td></td>
         </tr>
      </table>
   </div>
   <div id="parent-table">
      <div class="tab-content">
         <div id="tab_one_parent" class="tab-pane fade in active">
            <form id="parent-table-form-create" autocomplete="off">
               <table style="margin-bottom: 20px;width:40%;">
                  <tr>
                     <td>Parent Company Name</td>
                     <td><input type="text" name="parent_name" id="parent_name_id" value=""><input id = "customer_parent_name_token_id" name="_token" value="{{ csrf_token() }}" style="display:none"></td>
                     </td>
                  </tr>
                  <tr>
                     <td>Parent Company Code</td>
                     <td><input type="text" name="parent_code" id="parent_code_id" value=""></td>
                  </tr>
                  <tr>
                     <td><input type="hidden" id = "parent_token_id" name="_token" value="{{ csrf_token() }}"></td>
                  </tr>
               </table>
            </form>
         </div>
      </div>
   </div>
   <div id="child-table">
      <div class="tab-content">
         <div id="tab_one" class="tab-pane fade in active">
            <form id="child-table-form-create" autocomplete="off">
            <table style="margin-bottom: 20px;width:80%;">
               <tr>
                  <td></td>
               </tr>
               <tr>
                  <td>Parent Company Name</td>
                  <td><select name = "parent_company_name_id" id = "parent_company_name_id">
                     </select>
                  </td>
                  <td>Primary Contact Name </td>
                  <td><input class="initial_disable" type="text" id="p_contact_name"></td>
               </tr>
               <tr>
                  <td>Customer Name</td>
                  <td><input class="initial_disable" type="text" name="customer_name" id="customer_name" >
                  <input id = "customer_name_token_id" name="_token" value="{{ csrf_token() }}" style="display:none"></td>
                  <td>Primary Contact Mail ID</td>
                  <td><input class="initial_disable" type="email" name = "p_contact_email"  id="p_contact_email" class="email"></td>
               </tr>
               <tr>
                  <td>Customer Code</td>
                  <td><input class="initial_disable" type="text"  id="customer_code"></td>
                  <td>Primary Contact Number</td>
                  <td><input class="initial_disable" type='text' name = "p_contact_no" id="p_contact_number"></td>
               </tr>
               <tr>
                  <td>DUNS No</td>
                  <td><input class="initial_disable" type="text" id="dun_no" name ="dun_no"></td>
                  <td>Secondary Contact Name </td>
                  <td><input class="initial_disable" type="text" id="s_contact_name"></td>
               </tr>
               <tr>
                  <td>Customer Type</td>
                  <td>
                     <select class="initial_disable" id="customer_type" name="customer_type">
                     </select>
                  </td>
                  <td>Secondary Contact Mail ID</td>
                  <td><input class="initial_disable" type="email" name = "s_contact_email" id="s_contact_email" class="email"></td>
               </tr>
               <!-- <tr style="display:none;">
                  <td>Customer Plants</td>
                  <td><input type="text" id="customer_plants"></td>
                  </tr> -->
               <tr>
                  <td>Active</td>
                  <td><input class="initial_disable" type="checkbox" id="customer_active"></td>
                  <td>Secondary Contact Number</td>
                  <td><input class="initial_disable" type="text" name = "s_contact_no" id="s_contact_number"></td>
               </tr>
            </table>
         </div>
      </div>
   </div>
   <script type="text/javascript">
      jQuery.validator.addMethod('selectcheck', function(value) {
          return (value != '0');
      }, "");

      $(document).ready(function() {


          customer_master_type();

          function parent_company_name() {

              $('#parent_company_name_id').empty();
              $('#parent_company_name_id').append("<option id =\"null\" value=\"0\">Select</option>");
              $.ajax({

                  type: "GET",
                  url: "parent_results",
                  dataType: "json",
                  success: function(data) {

                      var results = data;
                      console.log(JSON.stringify(data));

                      for (var i = 0; i < results.length; i++) {
                      var parent_company_name = results[i].parent_name;
                          var parent_id = results[i].parent_id;
                          $('#parent_company_name_id').append("<option id =" + parent_id + " value=" + parent_id + ">" + parent_company_name + "</option>");
                      }

                  },
                  beforeSend: function() {

                  },
                  error: function() {
                      alert('Sorry we are not available');
                  }
              });

          }


          function customer_type() {

              $('#customer_type').empty();
              $.ajax({

                  type: "GET",
                  url: "customer_type",
                  dataType: "json",
                  success: function(data) {


                      var results = data;
                      console.log(JSON.stringify(data));

                      for (var i = 0; i < results.length; i++) {
                          if (i == '0') {

                              $('#customer_type').append("<option id =\"null\" value=\"0\">Select</option>");

                          }

                          var customer_type_id = results[i].customer_type_id;
                          var customer_type_name = results[i].customer_type;
                          $('#customer_type').append("<option id =" + customer_type_id + " value=" + customer_type_id + ">" + customer_type_name + "</option>");
                      }

                  },
                  beforeSend: function() {

                  },
                  error: function() {

                  }
              });

          }

          function customer_master_type() {

              $('parent-child').empty();
              $.ajax({

                  type: "GET",
                  url: "customer_master_type",
                  dataType: "json",
                  success: function(data) {


                      var results = data;
                      console.log(JSON.stringify(data));

                      for (var i = 0; i < results.length; i++) {
                          if (i == '0') {

                              $('#parent-child').append("<option  value = \"Select\" >Select</option>");

                          }

                          var customer_master_type = results[i].customer_master_type;
                          var customer_master_type_id = results[i].customer_master_type_id;
                          $('#parent-child').append("<option  value=" + customer_master_type_id + ">" + customer_master_type + "</option>");
                      }

                  },
                  beforeSend: function() {

                  },
                  error: function() {

                  }
              });

          }

          $('#parent-child').on('change', function() {

              if (this.value == 1) {

                  $("#parent-table,#parent_button,#show_all_parent").addClass("show");
                  $("#child_button,#child-table").removeClass("show");
                  $('#child-table-form-create')[0].reset();
                  $('#customer_name-error,#parent_company_name_id-error,#customer_type-error').remove();
                  $('#parent_company_name_id,#customer_type').removeClass("error");

              } else if (this.value == 2) {

                  $(".initial_disable").prop('disabled', true);
                  $('#customer_type').css('background-color','rgb(235, 235, 228)');
                  $("#child-table,#child_button").addClass("show");
                  $("#parent-table,#show_all_parent,#parent_button").removeClass("show");
                  $('#parent-table-form-create')[0].reset();
                  $('#parent_name_id-error,#parent_code_id-error').remove();

                  customer_type();
                  parent_company_name();


              } else {

                  $('#parent_company_name_id,#customer_type').removeClass("error");
                  $('#customer_name-error,#parent_company_name_id-error,#customer_type-error').remove();
                  $('#parent_name_id-error,#parent_code_id-error').remove();
                  $("#parent-table,#child-table,#show_all_parent,#child_button,#parent_button").removeClass("show");

              }

          });

          $('#parent_company_name_id').on('change', function() {

              if (this.value != 0) {

                     $(".initial_disable").prop('disabled', false);
                     $('#customer_type').css('background-color','white');

              } else {

                     $('#customer_type').val("0");
                     $('#customer_type').css('background-color','rgb(235, 235, 228)');
                     $(".initial_disable").prop('disabled', true);

              }

          });




          $(document).on('click', '#back_button_for_parent_results_div', function() {

              $("#list-of-parents,#back_button_for_parent_results_div").removeClass("show");
              $(".customer_type").removeClass("hide");
              $("#parent-table,#parent_button,#show_all_parent").addClass("show");

          });

          $(document).on('click', '#parent_button_save_id', function() {
              if ($('#parent-table-form-create').valid()) {


                $.msgBox({
                    title: "Confirm",
                    content: "Do you want to save the information?",
                    type: "confirm",
                    buttons: [{
                        value: "Yes"
                    }, {
                        value: "No"
                    }],
                    success: function(result) {
                        if (result == "Yes") {

                          var parent_name = $('#parent_name_id').val();
                          var parent_code = $('#parent_code_id').val();


                              $.ajax({
                                  type: "POST",
                                  url: "parent_insert",
                                  data: {
                                      "_token": "{{ csrf_token() }}",
                                      "parent_name": parent_name,
                                      "parent_code": parent_code,
                                  },
                                  success: function(data) {
                                      $.msgBox({
                                          title: "Message",
                                          content: data,
                                          type: "info",
                                      });

                                      $('#parent-table-form-create')[0].reset();
                                      $("#parent-table,#child-table,#show_all_parent,#child_button,#parent_button").removeClass("show");
                                      $(".customer_type").removeClass("hide");
                                      $('#parent-child').val("Select");



                                  },


                                  beforeSend: function() {

                                  },
                                  error: function(xhr, ajaxOptions, thrownError) {

                                    jsonValue = jQuery.parseJSON( xhr.responseText );
                                    error_data ="";

                                    $.each(jsonValue, function (key, val) {

                                        if(error_data)
                                        {
                                          error_data = error_data +  "</br>" + val;
                                        }
                                        else {
                                          error_data = val;
                                        }


                                    });

                                    $.msgBox({
                                        title: "Error",
                                        content: error_data,
                                        type: "error",
                                    });



                                  }

                              });

                        } else {

                        }
                    }
                });





              } else {

              }

          });

          $(document).on('click', '#child_button_save_id', function() {

              if ($('#child-table-form-create').valid()) {

                $.msgBox({
                    title: "Confirm",
                    content: "Do you want to save the information?",
                    type: "confirm",
                    buttons: [{
                        value: "Yes"
                    }, {
                        value: "No"
                    }],
                    success: function(result) {
                        if (result == "Yes") {

                          var parent_company_name_id = $('#parent_company_name_id').val();
                          var customer_name = $('#customer_name').val();
                          var customer_code = $('#customer_code').val();
                          var dun_no = $('#dun_no').val();
                          var customer_type = $('#customer_type').val();
                          var customer_plants = $('#customer_plants').val();
                          var p_contact_name = $('#p_contact_name').val();
                          var p_contact_email = $('#p_contact_email').val();
                          var p_contact_number = $('#p_contact_number').val();
                          var s_contact_name = $('#s_contact_name').val();
                          var s_contact_email = $('#s_contact_email').val();
                          var s_contact_number = $('#s_contact_number').val();

                          if ($('#customer_active').is(":checked")) {
                              customer_active = 1;

                          } else {

                              customer_active = 0;

                          }


                          $.ajax({
                              type: "POST",
                              url: "child_insert",
                              data: {
                                  "_token": "{{ csrf_token() }}",
                                  "parent_company_name_id": parent_company_name_id,
                                  "customer_name": customer_name,
                                  "customer_code": customer_code,
                                  "duns_no": dun_no,
                                  "customer_type": customer_type,
                                  "customer_plants ": customer_plants,
                                  "p_contact_name": p_contact_name,
                                  "p_contact_email": p_contact_email,
                                  "p_contact_number": p_contact_number,
                                  "s_contact_name": s_contact_name,
                                  "s_contact_email": s_contact_email,
                                  "s_contact_number": s_contact_number,


                                  "customer_active": customer_active,

                              },
                              success: function(data) {

                                  $.msgBox({
                                      title: "Message",
                                      content: data,
                                      type: "info",
                                  });

                                  $('#child-table-form-create')[0].reset();
                                  $("#parent-table,#child-table,#show_all_parent,#child_button,#parent_button").removeClass("show");
                                  $(".customer_type").removeClass("hide");
                                  $('#parent-child').val("Select");

                              },


                              beforeSend: function() {

                              },
                              error: function(xhr, ajaxOptions, thrownError) {

                                jsonValue = jQuery.parseJSON(xhr.responseText);
                                error_data ="";

                                $.each(jsonValue, function (key, val) {

                                    if(error_data)
                                    {
                                      error_data = error_data +  "</br>" + val;
                                    }
                                    else {
                                      error_data = val;
                                    }


                                });

                                $.msgBox({
                                    title: "Error",
                                    content: error_data,
                                    type: "error",
                                });



                              }

                          });

                        } else {

                        }
                    }
                });


              } else {

              }

          });

          $('#parent_button_cancel_id,#child_button_cancel_id').on('click', function() {

              $("#parent-table,#child-table,#show_all_parent,#child_button,#parent_button").removeClass("show");
              $(".customer_type").removeClass("hide");
              $('#parent-child').val("Select");

          });

      });

      $(window).load(function() {
          $("#master_data").trigger('click');
          $("#customer_master").trigger('click');
      });
   </script>
</div>
@stop
@section('lower_band')
<div class="col-xs-12 lower_band">
   <div id="parent_button" style="float:right;">
      <button id="parent_button_save_id" class="headerbuttons" type="button">Save</button>
      <button id="parent_button_cancel_id"class="headerbuttons" type="button">Cancel</button>
   </div>
   <div id="child_button" style="float:right;">
      <button id="child_button_save_id" class="headerbuttons" type="button">Save</button>
      <button id="child_button_cancel_id"class="headerbuttons" type="button">Cancel</button>
   </div>
   <div id="back_button_for_parent_results_div">
      <button id="back_button_for_parent_results" class="headerbuttons" type="button">BACK</button>
   </div>
</div>
@stop
