@extends('layouts.uscan_master_page')
@section('header')
<link rel="stylesheet" href="/css/process_monitor.css">
@stop

@section('upper_band')
<div class="col-xs-12 upper_band">
   <div class="col-xs-4 display-title">
      Process Monitor
   </div>
   <div class="col-xs-12 refresh">
    <a href="#" id="refresh_link" ><img src="images/refresh.jpg" alt="" class="refresh_image" ></a>
   </div>
</div>
@stop

@section('content')
<div class="col-xs-12 content">
   <div id="connection-table-search">

      <div id="list-of-connection" class="overflow ">
         <table style="width:100%;margin-bottom: 371px" class="connection_tab">
         </table>
      </div>

      <div class="display_desc scroll" style="display:none; margin-left: 15px;">
        <p style="float:left">Log ID :</p> <p class="log_id"></p><br>
        <p style="float:left">Task / Report Name :</p><p class="task_report_name"></p><br>
        <p style="float:left">Task Type :</p> <p class="task_type"></p><br>
        <p style="">Description :</p><br>
        <div class="description"></div>
        <p style="float:left">State :</p> <p class="State"></p>
      </div>

      <script type="text/javascript">
          $(document).ready(function() {

          function process_monitor_details(){

                  $.ajax({
                      type: "GET",
                      url: "get_process_monitor_details",
                      dataType: "json",
                      success: function(data) {
                        $('.connection_tab tr').remove();
                          var results = data;
                          temp_results = data;
                          $('.connection_tab').append('<tr style="background-color: #D3D3D3;cursor:text;"><th>Log ID</th><th>State</th><th>Task /Report Name</th><th>Type</th><th>Start Date Time</th><th>End Date Time</th></tr>');

                          if (results.length > 12) {

                              $('#list-of-connection').addClass("scroll");

                          } else {
                              $('#list-of-connection').removeClass("scroll");
                          }

                          for (var i = 0; i < results.length; i++) {
                              var task_name1 = results[i].variant?results[i].variant:results[i].task_name;

                              $('.connection_tab').append('<tr class = "double_click_monitor" id = ' + i + '><td>' + results[i].log_id + '</td><td>' + results[i].state + '</td><td>' + task_name1 + '</td><td>' + results[i].referece_type +
                                  '</td><td>' + results[i].start_date + '</td><td>' + results[i].end_date + '</td></tr>');
                          }

                      },
                      beforeSend: function() {
                          $('.connection_tab tr').remove();
                      },
                      error: function() {

                          $.msgBox({
                              title: "Error",
                              content: "Something went wrong",
                              type: "error",
                          });

                      }
                  });
                }

                    process_monitor_details();

                    $(document).on('click','#refresh_link',function(){
                      process_monitor_details();
                    });

                    $(document).on('dblclick', '.double_click_monitor', function() {
                    $('.display_desc,#back').show();
                    $('#list-of-connection').hide();

                    var id = $(this).attr('id');
                    var log_id = temp_results[id].log_id;

                    $('.log_id').text(temp_results[id].log_id);
                    $('.State').text(temp_results[id].state);
                    $('.task_report_name').text(temp_results[id].variant?temp_results[id].variant:temp_results[id].task_name);
                    $('.task_type').text(temp_results[id].referece_type);

                    $('.start_date').text(temp_results[id].start_date);
                    $('.end_date').text(temp_results[id].end_date);

                    $.ajax({
                        type: "GET",
                        url: "get_process_monitor_description",
                        data: {
                            "_token": "{{ csrf_token() }}",
                             "log_id": log_id,
                        },
                        success: function(data) {
                            var results = data;
                            $('.description').empty();

                            if (results.length > 12) {
                                $('.display_desc').addClass("scroll");
                            } else {
                                $('.display_desc').removeClass("scroll");
                            }


                            for (var i = 0; i < results.length; i++) {
                              console.log('results width',results[i].description.length);

                              if (results[i].description.length > 190) {
                                  $('.display_desc').addClass("scroll-x");
                              } else {
                                  $('.display_desc').removeClass("scroll-x");
                              }

                              $('.description ').append('<p> '+ "Step " +' '+ results[i].step_no + ' '+ "-" +' '+ "On" +' '+ results[i].date_time +'  '+ ":" +' <span> '+ results[i].description +' </span> </p> <br>');
                            }

                        },
                        beforeSend: function() {
                        },
                        error: function() {

                            $.msgBox({
                                title: "Error",
                                content: "Something went wrong",
                                type: "error",
                            });

                        }
                    });


                   });

                   $(document).on('click','#back',function(){
                     $('.display_desc,#back').hide();
                     $('#list-of-connection').show();
                   });


            });

          $(window).load(function() {

            $("#monitor").trigger('click');

          });

      </script>
   </div>

</div>
@stop

@section('lower_band')
<div  id="back_buttons">
   <button class="headerbuttons" id="back" type="button">Back</button>
</div>

@stop
