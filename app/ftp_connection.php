<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ftp_connection extends Model
{
    //
    protected $table = 'ftp_connections';
    protected $dates = ['deleted_at'];
}
