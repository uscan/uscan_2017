<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class process_monitor_desc_table extends Model
{
    //
    protected $table = 'process_monitor_descs';
    public $timestamps = false;
}
