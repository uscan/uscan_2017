<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class customer_child_model extends Model
{
     use SoftDeletes;
     
     protected $table = 'customer_child';
     protected $dates = ['deleted_at'];

}
