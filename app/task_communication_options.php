<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class task_communication_options extends Model
{
    //
    use SoftDeletes;
    protected $table = 'task_communication_options';
    protected $dates = ['deleted_at'];
}
