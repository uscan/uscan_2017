<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mail_enterprise_user_group extends Model
{
    //
    protected $table = 'mail_enterprise_user_groups';
    protected $dates = ['deleted_at'];
}
