<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class report_name_customer_type extends Model
{
    //
    protected $table = 'report_name_customer_type';
    protected $dates = ['deleted_at'];
    public $timestamps = false;
}
