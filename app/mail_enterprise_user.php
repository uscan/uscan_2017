<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mail_enterprise_user extends Model
{
    //
    protected $table = 'mail_enterprise_users';
    protected $dates = ['deleted_at'];
}
