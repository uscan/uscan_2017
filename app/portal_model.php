<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class portal_model extends Model
{

    use SoftDeletes;
    protected $table = 'portal';
    protected $dates = ['deleted_at'];
    // protected $softDelete = true;
}
