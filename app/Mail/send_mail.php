<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class send_mail extends Mailable
{
    use Queueable, SerializesModels;
    public $path;
    public $subject;
    public $FileName;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($path,$subject,$FileName)
    {
        //
        $this->path = $path;
        $this->subject = $subject;
        $this->FileName = $FileName;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        ini_set('max_execution_time', 300);
        $address = 'noreply.uscan.2016@gmail.com';
        $name = 'Uscan Support';
        // $subject = 'Scheduled Program';
        // $path    = "C:\\xampp\htdocs\uscan\public\crawler\documents\\ford\\ford_q1_scores\\Test Data.xls";
        return $this->view('emails.mail_view')
                    ->from($address, $name)
                    ->bcc($address, $name)
                    ->replyTo($address, $name)
                    ->subject($this->subject)
                    ->attach($this->path, [
                        'as' => $this->FileName,
                        'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    ]);
    }
}
