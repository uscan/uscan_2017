<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Auth;

class report_generator_controller extends Controller
{

  public static function saveReportGeneratorTables($request, $report_id) {
    $businessUnitArray = $request->business_unit;
    if($businessUnitArray != 0) {
      foreach ($businessUnitArray as $value) {
        $report = new \App\report_name_business_unit_reference;
        $report->report_id           = $report_id;
        $report->business_unit       = $value;
        $report->enterprise_id       =  Auth::user()->enterprise_id;
        $report->save();
      }
    }

    $customerTypeArray = $request->customer_type_id;
    if($customerTypeArray != 0) {
      foreach ($customerTypeArray as $value) {
        $report = new \App\report_name_customer_type;
        $report->report_id           = $report_id;
        $report->customer_type_id       = $value;
        $report->enterprise_id       =  Auth::user()->enterprise_id;
        $report->save();
      }
    }

    $productGroupArray = $request->product_group;
    if($productGroupArray != 0) {
      foreach ($productGroupArray as $value) {
        $report = new \App\report_name_product_group_reference;
        $report->report_id           = $report_id;
        $report->product_group       = $value;
        $report->enterprise_id       =  Auth::user()->enterprise_id;
        $report->save();
      }
    }

    $plantArray = $request->plant_id;
    if($plantArray != 0) {
      foreach ($plantArray as $value) {
        $report = new \App\report_name_plant_reference;
        $report->report_id      = $report_id;
        $report->plant_id       = $value;
        $report->enterprise_id       =  Auth::user()->enterprise_id;
        $report->save();
      }
    }


    $customerIdArray = $request->cust_id;
    if($customerIdArray != 0) {
      foreach ($customerIdArray as $value) {
        $report = new \App\report_name_customer_reference;
        $report->report_id      = $report_id;
        $report->customer_id    = $value;
        $report->enterprise_id  = Auth::user()->enterprise_id;
        $report->save();
      }
    }

    $customerIdArray = $request->plant_status_id;
    if($customerIdArray != 0) {
      foreach ($customerIdArray as $value) {
        $report = new \App\report_name_plant_status_reference;
        $report->report_id      = $report_id;
        $report->plant_status_id= $value;
        $report->enterprise_id  = Auth::user()->enterprise_id;
        $report->save();
      }
    }
  }

  public function save_varient(Request $request)
  {
      $query = 'select current_no from number_range where table_name = "report_generator"';
      $result = \DB::select($query);
      $report_id = $result[0]->current_no + 1;

      $update_current_no =   \DB::table('number_range')
      ->where('table_name',  'report_generator')
      ->limit(1)
      ->update(array('current_no' => $report_id));

      $report = new \App\report_generator;
      $report->report_id           = $report_id;
      $report->variant             = $request->variant;
      $report->report_name_id      = $request->report_name_id;
      $report->start_date          = $request->start_date;

      if ($request->end_date != "")
      {
          $report->end_date        = $request->end_date;
      }
      else {
          $report->end_date = "9999-12-31";
      }


      $report->created_by          = Auth::user()->email;
      $report->updated_by          = Auth::user()->email;
      $report->enterprise_id       = Auth::user()->enterprise_id;
      $report->save();

      report_generator_controller::saveReportGeneratorTables($request, $report_id);
      return "New Varient saved Sucessfully";


  }


  public function update(Request $request)
  {
    $report = new \App\report_generator;
     $data =   \DB::table('report_generators')
    ->where('report_id',  $request->report_id)
    ->limit(1)
    ->update(array('variant' => $request->variant,
    'report_name_id'  => $request->report_name_id,
    'start_date'  => $request->start_date,
    'end_date'  => $request->end_date,

    'updated_by' => Auth::user()->email));

    $report_id = $request->report_id;

    report_generator_controller::deleteReportGeneratorTables($report_id);
    report_generator_controller::saveReportGeneratorTables($request, $report_id);
    return $data;
  }

  public function get_business_unit(Request $request)
  {
      $data1 = \DB::select("select DISTINCT business_unit FROM plant");
      return $data1;
  }
  public function get_product_group(Request $request)
  {
      $data = \DB::select("select DISTINCT product_group FROM plant");
      return $data;
  }
  public function get_plant_name(Request $request)
  {
      $data = \DB::select("select DISTINCT plant_name,plant_id FROM plant");
      return $data;
  }
  public function get_plant_status(Request $request)
  {
      $data = \DB::select("select DISTINCT plant_status,plant_status_id FROM plant_status");
      return $data;
  }
  public function get_cust_name(Request $request)
  {
      $data = \DB::select("select DISTINCT customer_name,customer_id FROM customer_child");
      return $data;
  }
  // public function get_cust_type(Request $request)
  // {
  //     $data = \DB::select("select DISTINCT * FROM customer_type");
  //     return $data;
  // }
  public function get_variant_name(Request $request)
  {
      $data = \DB::select("select * FROM report_generators");
      return $data;
  }
  public function get_selected_variant_name(Request $request)
  {
      $report_id = $request->filterKey;
      $data = \DB::select("select rn.report_type_id,rg.report_id, rg.variant FROM report_generators as rg JOIN report_name AS rn on rg.report_name_id = rn.report_name_id where rg.report_id = '$report_id' ");
      return $data;
  }
  public function get_variant_details(Request $request)
  {
      $data = \DB::select("select rt.report_type_name,rt.report_type_id,rn.report_name,rg.report_id,rg.variant FROM report_types AS rt JOIN report_name AS rn ON rt.report_type_id=rn.report_type_id JOIN report_generators AS rg ON rg.report_name_id = rn.report_name_id");
      return $data;
  }

  public function get_report_type()
  {
      $data = \DB::select("select * FROM report_types");
      return $data;
  }
  public function get_report_name()
  {
      $data = \DB::select("select * FROM report_name");
      return $data;
  }
  public function get_report_name_by_report_type(Request $request)
  {
      $report_type_id = $request->filterKey;
      $data = \DB::select("select DISTINCT rn.report_name,rn.report_name_id FROM report_name AS rn JOIN report_types AS rt ON rn.report_type_id = rt.report_type_id WHERE rn.report_type_id = '$report_type_id' ");
      return $data;
  }
  public function get_product_group_by_business_unit(Request $request)
  {
      $business_unit = $request->filterKey;
      $data = \DB::select("select DISTINCT product_group FROM plant WHERE business_unit in ('$business_unit') ");
      return $data;
  }
  public function get_plant_name_by_product_group(Request $request)
  {
      $product_group = $request->filterKey;
      $data = \DB::select("select DISTINCT plant_name, plant_id FROM plant WHERE product_group in ('$product_group')");
      return $data;
  }
  // public function get_cust_name_by_cust_type(Request $request)
  // {
  //     $customer_type_id = $request->filterKey;
  //     $data = \DB::select("select DISTINCT cc.customer_name, cc.customer_id FROM customer_child AS cc JOIN customer_type AS ct  ON cc.customer_type_id = ct.customer_type_id WHERE  ct.customer_type_id in ('$customer_type_id') ");
  //     return $data;
  // }
  public function get_custType_custName_byReportType(Request $request)
  {
      $report_type_id = $request->filterKey;
      $data = \DB::select("select DISTINCT cc.customer_name, cc.customer_id, ct.customer_type, ct.customer_type_id FROM customer_child AS cc JOIN report_types AS rt ON rt.customer_id = cc.customer_id JOIN customer_type AS ct ON cc.customer_type_id = ct.customer_type_id WHERE rt.report_type_id = '$report_type_id' ");
      return $data;
  }

  public function get_bu_by_reportID(Request $request)
  {
      $report_id = $request->report_id;
      $data = \DB::select("select DISTINCT * FROM report_name_business_unit WHERE report_id = ('$report_id')");
      return $data;
  }
  public function get_bu_by_report_type(Request $request)
  {
      $report_type_id = $request->filterKey;
      $data = \DB::select("select DISTINCT plant.business_unit FROM plant AS plant JOIN plant_customer_ref AS pcref ON plant.plant_id = pcref.plant_id JOIN report_types AS rt ON rt.customer_id = pcref.customer_id WHERE rt.report_type_id =  ('$report_type_id')");
      return $data;
  }
  public function get_pg_by_report_type(Request $request)
  {
      $report_type_id = $request->filterKey;
      $data = \DB::select("select DISTINCT plant.product_group FROM plant AS plant JOIN plant_customer_ref AS pcref ON plant.plant_id = pcref.plant_id JOIN report_types AS rt ON rt.customer_id = pcref.customer_id WHERE rt.report_type_id =  ('$report_type_id')");
      return $data;
  }
  public function get_plant_by_report_type(Request $request)
  {
      $report_type_id = $request->filterKey;
      $data = \DB::select("select DISTINCT plant.plant_id,plant.plant_name FROM plant AS plant JOIN plant_customer_ref AS pcref ON plant.plant_id = pcref.plant_id JOIN report_types AS rt ON rt.customer_id = pcref.customer_id WHERE rt.report_type_id =  ('$report_type_id')");
      return $data;
  }

  public function get_pg_by_reportID(Request $request)
  {
      $report_id = $request->report_id;
      $data = \DB::select("select DISTINCT * FROM report_name_product_group WHERE report_id = ('$report_id')");
      return $data;
  }
  public function get_plant_by_reportID(Request $request)
  {
      $report_id = $request->report_id;
      // $report_type_id = $request->filterKey;
       $data = \DB::select("select pt.plant_name, pt.plant_id FROM plant AS pt JOIN report_name_plant AS rnp ON pt.plant_id=rnp.plant_id WHERE rnp.report_id = ('$report_id')");
      // $data = \DB::select("select pt.plant_name, pt.plant_id FROM plant AS pt JOIN report_name_plant AS rnp ON pt.plant_id=rnp.plant_id WHERE rnp.report_id = ('$report_id')");
      return $data;
  }
  public function get_custType_by_reportID(Request $request)
  {
      $report_id = $request->report_id;
      $data = \DB::select("select ct.customer_type, ct.customer_type_id FROM customer_type AS ct JOIN report_name_customer_type AS rnct ON ct.customer_type_id =rnct.customer_type_id WHERE rnct.report_id = ('$report_id')");
      return $data;
  }
  public function get_customer_by_reportID(Request $request)
  {
      $report_id = $request->report_id;
      $data = \DB::select("select cc.customer_name, cc.customer_id,rnc.customer_id FROM customer_child AS cc JOIN report_name_customer AS rnc ON cc.customer_id = rnc.customer_id WHERE rnc.report_id = ('$report_id')");
      return $data;
  }

  public function get_plant_status_by_reportID(Request $request)
  {
      $report_id = $request->report_id;
      $data = \DB::select("select ps.plant_status, ps.plant_status_id,rnps.plant_status_id FROM plant_status AS ps JOIN report_name_plant_status AS rnps ON ps.plant_status_id = rnps.plant_status_id WHERE rnps.report_id = ('$report_id')");
      return $data;
  }
  public function get_report_name_by_reportID(Request $request)
  {
      $report_id = $request->report_id;
      $data = \DB::select("select report_name_id,start_date,end_date FROM report_generators WHERE report_id = ('$report_id')");
      return $data;
  }
  public static function deleteReportGeneratorTables($report_id) {
    $data = \DB::table('report_name_business_unit')->where('report_id','=',$report_id)->delete();
    $data = \DB::table('report_name_product_group')->where('report_id','=',$report_id)->delete();
    $data = \DB::table('report_name_plant')->where('report_id','=',$report_id)->delete();
    $data = \DB::table('report_name_customer_type')->where('report_id','=',$report_id)->delete();
    $data = \DB::table('report_name_customer')->where('report_id','=',$report_id)->delete();
    $data = \DB::table('report_name_plant_status')->where('report_id','=',$report_id)->delete();
  }

  public function destroy(Request $request)
  {
    $report_id = $request->report_id;
        report_generator_controller::deleteReportGeneratorTables($report_id);
        $data = \DB::select("delete from report_generators where report_id = '$report_id' and enterprise_id = '". Auth::user()->enterprise_id ."' ");
        return "Customer Deleted Sucessfully";
  }
  public function get_start_conditions_for_scheduler(Request $request)
  {
      $start_condition = App\start_condition::all()->toArray();
      return $start_condition;
  }
  public function get_frequency(Request $request)
  {
      $frequency = App\frequency_model::all()->toArray();
      return $frequency;
  }
  public function save_schedule_reports(Request $request)
  {
            $query = 'select current_no from number_range where table_name = "scheduler"';
            $result = \DB::select($query);
            $job_id = $result[0]->current_no + 1;

            $update_current_no =   \DB::table('number_range')
            ->where('table_name',  'scheduler')
            ->limit(1)
            ->update(array('current_no' => $job_id));

            $job = new App\scheduler_model;
            $job->job_id = $job_id;
            $job->job_name = $request->job_name;
            $job->job_type = "R";
            $job->task_id = $request->variant;
            $job->job_priority = $request->job_priority;
            $job->job_status ="scheduled";
            $job->active= $request->active;
            $job->start_type_id= $request->start_type_id;
            $job->enterprise_id= Auth::user()->enterprise_id;
            $job->created_by = Auth::user()->email;
            $job->updated_by = Auth::user()->email;
            $job->periodic= "0";
            $job->after_job= "0";

            $job->job_start_date= $request->job_start_date;
            $job->job_end_date= $request->job_end_date;
            $job->job_last_run_date= $request->job_last_run_date;
            $job->job_start_time= $request->job_start_time;
            $job->job_end_time= $request->job_end_time;
            $job->job_last_run_time= $request->job_last_run_time;

            if($request->start_type_id=="1")
            {
                        if($request->period_immediately=="1")
                        {
                            $job->periodic= $request->period_immediately;
                            $currTimeStamp = date('c');
                            $date = date('Y-m-d', time());
                            $time_imm = date('His', time());

                            $job->job_start_time= $date;
                            $job->job_end_date= $date;
                            $job->job_last_run_date= $date;

                            $job->job_start_time= $time_imm;
                            $job->job_end_time= $time_imm;
                            $job->job_last_run_time= $time_imm;

                            $job->save();

                            $periodic = new App\periodic;
                            $periodic->frequency_id = $request->frequency;
                            $periodic->enterprise_id= Auth::user()->enterprise_id;
                            $periodic->job_id = $job_id;
                            $periodic->save();
                        }
                        else {
                          $minutes_to_add = 1;
                          // $time = new DateTime('2011-11-17 05:05');
                          // $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

                          $currTimeStamp = date('c');
                          $date = date('Y-m-d', time());
                          $h = date('H', time());
                          $i = date('i', time());
                          $i = $i +1;
                          $s = date('s', time());
                          $time_imm = $h .':'.$i.':'.$s;
                          // $time_imm = date('Y-m-d His', time());
                          // $time_imm->add(new DateInterval('PT' . $minutes_to_add . 'M'));
                          // $time_imm = date('His', time());

                          $job->job_start_time= $date;
                          $job->job_end_date= $date;
                          $job->job_last_run_date= $date;

                          $job->job_start_time= $time_imm;
                          $job->job_end_time= $time_imm;
                          $job->job_last_run_time= $time_imm;

                          $job->save();
                        }

            }
            elseif ($request->start_type_id=="2") {
                  if($request->periodic_date_time=="1")
                  {
                      $job->periodic= $request->periodic_date_time;
                      $job->save();

                      $periodic = new App\periodic;
                      $periodic->frequency_id = $request->frequency_date_time;
                      $periodic->enterprise_id= Auth::user()->enterprise_id;
                      $periodic->job_id = $job_id;
                      $periodic->save();
                  }  else {
                        $job->save();
                    }
            }
            elseif ($request->start_type_id=="3") {
                $job->after_job= $request->after_job;
                $job->save();

                // $after_job = new App\predecessor_model;
                // $after_job->predecessor_job_id = $request->predecessor_job_name;
                // $after_job->job_id = $job_id;
                // $after_job->enterprise_id= Auth::user()->enterprise_id;
                // $after_job->save();
            }

              $length =  sizeof($request->distribution_strategy);

              for ($i = 0; $i < $length; $i++) {

                $task_model = new \App\distribution_strategy;
                $task_model->task_status              = $request->distribution_strategy[$i]['report_status'];
                $task_model->communication_options    = $request->distribution_strategy[$i]['communication_options'];
                $task_model->mail_type                = $request->distribution_strategy[$i]['mail_type'];
                $task_model->mail_address             = $request->distribution_strategy[$i]['mail_address'];
                $task_model->task_id                  = $job_id;
                $task_model->enterprise_id            = Auth::user()->enterprise_id;
                $task_model->save();
             }
}
public function fetch_job_name_report(Request $request)
{
  $job_name = $request->job_name;
  $results = array();
  $results;
  $query = "select job_name from job where job_name LIKE '%$job_name%'";
  $res   = \DB::select($query);
  foreach ($res as $query)
    {
        array_push($results,$query->job_name);
    }
  return $results;
}


public function fetch_variant(Request $request)
{
  $variant = $request->variant;
  $results = array();
  $results;
  $query = "select variant,report_id from report_generators where variant LIKE '%$variant%'";
  $res   = \DB::select($query);
  foreach ($res as $query)
    {
        array_push($results,$query->variant);
    }
  return $results;
}

public function show(Request $request)
{
    $job_name_search = $request->job_name_search;
    // $task_type_search = $request->task_type_search;
    $variant_search = $request->variant_search;
    $query_data = "";
    if($job_name_search != "")
    {
      $query_data = " job.job_name = '". $job_name_search. "' and ";
    }
    if($variant_search != "")
    {
      $query_data = $query_data . " variant = '". $variant_search. "' and ";
    }
    $query_data = substr($query_data, 0, -4);
    $data = \DB::select("select job.*,rg.variant FROM job JOIN report_generators AS rg ON job.task_id = rg.report_id where " .$query_data );
    if (empty($data)) {
        return "0";
    }
    else {
      return $data;
    }
}
public function get_frequency_for_scheduler(Request $request)
{
  $job_id = $request->job_id;
  $data = \DB::select("select job.*,peri.frequency_id FROM job JOIN periodic AS peri ON job.job_id = peri.job_id where job.job_id =" .$job_id);
  return $data;
}

public function get_after_job_for_scheduler(Request $request)
{
  $job_id = $request->job_id;
  $data = \DB::select("select job.*,after_job.predecessor_job_id FROM job JOIN after_job AS after_job ON job.job_id = after_job.job_id where job.job_id =" .$job_id);
  return $data;
}
public function get_jobs(Request $request){
  $data = \DB::select("select job.*,rg.variant FROM job JOIN report_generators AS rg ON job.task_id = rg.report_id");
  return $data;
}

public function get_jobs_for_scheduler_reports(Request $request){
  // $task_type_id = $request->task_type_id;

  $job_name = \DB::select("select job_id,job_name from job where job_type = 'R' and enterprise_id = ".Auth::user()->enterprise_id);
  return $job_name;
}

public function get_task_name(Request $request){
  $report_id = $request->report_id;

  $variant = \DB::select("select report_id,variant from report_generators WHERE report_id = '$report_id'");
  return $variant;
}
public function get_variant_for_scheduler(Request $request){

  $variant = \DB::select("select report_id,variant from report_generators");
  return $variant;
}
public function get_variant(Request $request){
  $variant = \DB::select("select report_id,variant from report_generators");
  return $variant;
}



public function scheduler_update(Request $request)
{
  $updated_at = date('Y-m-d H:i:s', time());
  $job = new App\scheduler_model;
  $job_id = $request->job_id;

  // $data = \DB::select("delete from after_job where job_id='$job_id'");
  $data = \DB::select("delete from periodic where job_id='$job_id' and enterprise_id = ".Auth::user()->enterprise_id);

  if($request->start_type_id == "1")
  {
              if($request->period_immediately=="1")
              {
                $data =   \DB::table('job')
               ->where('job_id',  $job_id)
               ->where('enterprise_id',Auth::user()->enterprise_id)
               ->limit(1)
               ->update(array('job_name' => $request->job_name,
               'job_type' =>   "R",
               'task_id' =>  $request->variant,
               'job_priority' =>  $request->job_priority,
               'job_status' =>  "scheduled",
               'start_type_id' =>  $request->start_type_id,
               'active' =>  $request->active,
               'updated_at' =>  $updated_at,
               'updated_by' => Auth::user()->email,

               'job_start_date' =>  $request->job_start_date,
               'job_last_run_date' =>  $request->job_last_run_date,
               'job_start_time' =>  $request->job_start_time,
               'job_end_date' =>  $request->job_end_date,
               'job_end_time' =>  $request->job_end_time,
               'job_last_run_time' =>  $request->job_last_run_time,
               'periodic' =>  $request->period_immediately,
               'after_job' => "0",));

                  $periodic = new App\periodic;
                  $periodic->frequency_id = $request->frequency;
                  $periodic->enterprise_id= Auth::user()->enterprise_id;
                  $periodic->job_id = $job_id;
                  $periodic->save();
              }
              else {
                $currTimeStamp = date('c');
                $date = date('Y-m-d', time());
                $h = date('H', time());
                $i = date('i', time());
                $i = $i +1;
                $s = date('s', time());
                $time_imm = $h .':'.$i.':'.$s;

                $data =   \DB::table('job')
               ->where('job_id',  $job_id)
               ->where('enterprise_id',Auth::user()->enterprise_id)
               ->limit(1)
               ->update(array('job_name' => $request->job_name,
               'job_type' =>    "R",
               'task_id' =>  $request->variant,
               'job_priority' =>  $request->job_priority,
               'job_status' =>  "scheduled",
               'start_type_id' =>  $request->start_type_id,
               'active' =>  $request->active,
               'updated_at' =>  $updated_at,
               'updated_by' => Auth::user()->email,

               'job_start_date' =>  $request->job_start_date,
               'job_last_run_date' =>  $request->job_last_run_date,
               'job_start_time' =>  $time_imm,
               'job_end_date' =>  $request->job_end_date,
               'job_end_time' =>  $time_imm,
               'job_last_run_time' =>  $time_imm,
               'periodic' =>  "0",
               'after_job' => "0",));
              }
  }
  elseif ($request->start_type_id=="2") {
        if($request->periodic_date_time=="1")
        {
              $data =   \DB::table('job')
             ->where('job_id',  $job_id)
             ->limit(1)
             ->update(array('job_name' => $request->job_name,
             'job_type' =>    "R",
             'task_id' =>  $request->variant,
             'job_priority' =>  $request->job_priority,
             'job_status' =>  "scheduled",
             'start_type_id' =>  $request->start_type_id,
             'enterprise_id' =>  Auth::user()->enterprise_id,
             'active' =>  $request->active,
             'updated_at' =>  $updated_at,
             'updated_by' => Auth::user()->email,

             'job_start_date' =>  $request->job_start_date,
             'job_last_run_date' =>  $request->job_last_run_date,
             'job_start_time' =>  $request->job_start_time,
             'job_end_date' =>  $request->job_end_date,
             'job_end_time' =>  $request->job_end_time,
             'job_last_run_time' =>  $request->job_last_run_time,
             'periodic' => $request->periodic_date_time,
             'after_job' => "0",));

              $periodic = new App\periodic;
              $periodic->frequency_id = $request->frequency_date_time;
              $periodic->job_id = $job_id;
              $periodic->enterprise_id= Auth::user()->enterprise_id;
              $periodic->save();
        }
        else {
          $data =   \DB::table('job')
         ->where('job_id',  $job_id)
         ->limit(1)
         ->update(array('job_name' => $request->job_name,
         'job_type' =>    "R",
         'task_id' =>  $request->variant,
         'job_priority' =>  $request->job_priority,
         'job_status' =>  "scheduled",
         'start_type_id' =>  $request->start_type_id,
         'enterprise_id' =>  Auth::user()->enterprise_id,
         'active' =>  $request->active,
         'updated_at' =>  $updated_at,
         'updated_by' => Auth::user()->email,

         'job_start_date' =>  $request->job_start_date,
         'job_last_run_date' =>  $request->job_last_run_date,
         'job_start_time' =>  $request->job_start_time,
         'job_last_run_time' =>  $request->job_last_run_time,
         'job_end_date' =>  $request->job_end_date,
         'job_end_time' =>  $request->job_end_time,
         'periodic' => "0",
         'after_job' => "0",));
        }
  }
  elseif ($request->start_type_id=="3") {
                  $data =   \DB::table('job')
                 ->where('job_id',  $job_id)
                 ->limit(1)
                 ->update(array('job_name' => $request->job_name,
                 'job_type' =>   "R",
                 'task_id' =>  $request->variant,
                 'job_priority' =>  $request->job_priority,
                 'job_status' =>  "scheduled",
                 'start_type_id' =>  $request->start_type_id,
                 'enterprise_id' =>   Auth::user()->enterprise_id,
                 'active' =>  $request->active,
                 'updated_at' =>  $updated_at,
                 'updated_by' => Auth::user()->email,

                 'job_start_date' =>  $request->job_start_date,
                 'job_last_run_date' =>  $request->job_last_run_date,
                 'job_start_time' =>  $request->job_start_time,
                 'job_last_run_time' =>  $request->job_last_run_time,
                 'job_end_date' =>  $request->job_end_date,
                 'job_end_time' =>  $request->job_end_time,
                 'periodic' => "0",
                 'after_job' =>  $request->after_job,
               ));

              //  $after_job = new App\predecessor_model;
              //  $after_job->predecessor_job_id = $request->predecessor_job_name;
              //  $after_job->job_id = $job_id;
              // $after_job->enterprise_id= Auth::user()->enterprise_id;
              //  $after_job->save();
     }

     $ds_count =  sizeof($request->distribution_strategy);
     $distribution_strategy_check = $request->distribution_strategy_check;

     if($distribution_strategy_check == 1)
     {
             for($i=0;$i<$ds_count;$i++)
             {
                 $distribution_strategy_id = $request->distribution_strategy[$i]['distribution_strategy_id'];
                 //echo $distribution_strategy_id;
                 $query = "select * from distribution_strategy where distribution_strategy_id = '$distribution_strategy_id'";
                 $result = \DB::select($query);

                if(count($result))
                   {
                   $data =   \DB::table('distribution_strategy')
                   ->where('distribution_strategy_id',  $request->distribution_strategy[$i]['distribution_strategy_id'])
                   ->where('distribution_strategy.enterprise_id',Auth::user()->enterprise_id)
                   ->limit(1)
                   ->update(array('task_status' => $request->distribution_strategy[$i]['report_status'],
                   'communication_options' => $request->distribution_strategy[$i]['communication_options'],
                   'mail_type' => $request->distribution_strategy[$i]['mail_type'],
                   'mail_address' => $request->distribution_strategy[$i]['mail_id'],
                     ));
                  }
                  else if(!count($result))
                  {
                    $length =  sizeof($request->distribution_strategy);
                    for ($i = 0; $i < $length; $i++)
                    {
                      $task_model = new \App\distribution_strategy;
                      $task_model->task_status              = $request->distribution_strategy[$i]['report_status'];
                      $task_model->communication_options    = $request->distribution_strategy[$i]['communication_options'];
                      $task_model->mail_type                = $request->distribution_strategy[$i]['mail_type'];
                      $task_model->mail_address             = $request->distribution_strategy[$i]['mail_id'];
                      $task_model->task_id                  = $job_id;
                      $task_model->enterprise_id            = Auth::user()->enterprise_id;
                      $task_model->save();
                       }
                  }

              }

      }
     return $data;
}

    public function delete_job(Request $request)
    {
       $job_id = $request->job_id;
      //  $data = \DB::select("delete from after_job where job_id='$job_id'");
       $data = \DB::select("delete from periodic where job_id='$job_id'");
       $data = \DB::select("delete from job where job_id='$job_id'");
       return $data;
    }

}
