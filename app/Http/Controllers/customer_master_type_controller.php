<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Auth;

class customer_master_type_controller extends Controller
{

    public function index()
    {

      $customer_master_type_results =  \DB::table('customer_master_type')
               ->orderBy('customer_master_type_id', 'asc')
               ->get();

      return $customer_master_type_results;

    }

}
