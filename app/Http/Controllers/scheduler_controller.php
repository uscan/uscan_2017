<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Auth;

class scheduler_controller extends Controller
{
  public function get_portal(Request $request)
  {
      $portal_name = App\portal_model::all()->toArray();
      return $portal_name;
  }

  public function get_task_type(Request $request)
  {
      $task_type = App\task_type_model::all()->toArray();
      return $task_type;
  }

  // public function get_task_name(Request $request)
  // {
  //     $task_name = App\task_model::all()->toArray();
  //     return $task_name;
  // }

  public function get_task_name(Request $request){
    $task_type_id = $request->task_type_id;

    $task_name = \DB::select("select task_id,task_name from task WHERE task_type_id = '$task_type_id' and enterprise_id = ".Auth::user()->enterprise_id);
    return $task_name;
  }

  public function get_task_name_for_scheduler(Request $request){
    $task_name = \DB::select("select task_id,task_name from task where enterprise_id = ".Auth::user()->enterprise_id);
    return $task_name;
  }

  public function get_jobs_for_scheduler(Request $request){
    $job_name = \DB::select("select job_id,job_name from job where enterprise_id = ".Auth::user()->enterprise_id);
    return $job_name;
  }

  public function get_frequency(Request $request)
  {
      $frequency = App\frequency_model::all()->toArray();
      return $frequency;
  }


  public function get_start_conditions_for_scheduler(Request $request)
  {
      $start_condition = App\start_condition::all()->toArray();
      return $start_condition;
  }

  public function store(Request $request)
  {
            // $max_address_id = \DB::table('address')->max('address_id');
            $query = 'select current_no from number_range where table_name = "scheduler" and enterprise_id = '.Auth::user()->enterprise_id;
            $result = \DB::select($query);
            $job_id = $result[0]->current_no + 1;

            $update_current_no =   \DB::table('number_range')
            ->where('table_name',  'scheduler')
            ->limit(1)
            ->update(array('current_no' => $job_id));

            $job = new App\scheduler_model;
            $job->job_id = $job_id;
            $job->job_name = $request->job_name;
            $job->job_type = "T";
            $job->task_id = $request->task_name;
            $job->job_priority = $request->job_priority;
            $job->job_status ="scheduled";
            $job->active= $request->active;
            $job->start_type_id= $request->start_type_id;
            $job->enterprise_id= Auth::user()->enterprise_id;
            $job->created_by =  Auth::user()->email;
            $job->updated_by =  Auth::user()->email;
            $job->periodic= "0";
            $job->after_job= "0";

            $job->job_start_date= $request->job_start_date;
            $job->job_end_date= $request->job_end_date;
            $job->job_last_run_date= $request->job_last_run_date;
            $job->job_start_time= $request->job_start_time;
            $job->job_end_time= $request->job_end_time;
            $job->job_last_run_time= $request->job_last_run_time;

            if($request->start_type_id=="1")
            {
                        if($request->period_immediately=="1")
                        {
                            $job->periodic= $request->period_immediately;

                            $currTimeStamp = date('c');
                            $date = date('Y-m-d', time());
                            $time_imm = date('His', time());

                            $job->job_start_time= $date;
                            $job->job_end_date= $date;
                            $job->job_last_run_date= $date;

                            $job->job_start_time= $time_imm;
                            $job->job_end_time= $time_imm;
                            $job->job_last_run_time= $time_imm;

                            $job->save();

                            $periodic = new App\periodic;
                            $periodic->frequency_id = $request->frequency;
                            $periodic->enterprise_id= Auth::user()->enterprise_id;
                            $periodic->job_id = $job_id;
                            $periodic->save();
                        }
                        else {
                            $minutes_to_add = 1;
                            // $time = new DateTime('2011-11-17 05:05');
                            // $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

                            $currTimeStamp = date('c');
                            $date = date('Y-m-d', time());
                            $h = date('H', time());
                            $i = date('i', time());
                            $i = $i +1;
                            $s = date('s', time());
                            $time_imm = $h .':'.$i.':'.$s;
                            // $time_imm = date('Y-m-d His', time());
                            // $time_imm->add(new DateInterval('PT' . $minutes_to_add . 'M'));
                            // $time_imm = date('His', time());

                            $job->job_start_time= $date;
                            $job->job_end_date= $date;
                            $job->job_last_run_date= $date;

                            $job->job_start_time= $time_imm;
                            $job->job_end_time= $time_imm;
                            $job->job_last_run_time= $time_imm;


                            $job->save();
                        }

            }
            elseif ($request->start_type_id=="2") {
                  if($request->periodic_date_time=="1")
                  {
                      $job->periodic= $request->periodic_date_time;
                      $job->save();

                      $periodic = new App\periodic;
                      $periodic->frequency_id = $request->frequency_date_time;
                      $periodic->enterprise_id= Auth::user()->enterprise_id;
                      $periodic->job_id = $job_id;
                      $periodic->save();
                  }  else {
                        $job->save();
                    }
            }
            elseif ($request->start_type_id=="3") {
                $job->after_job= $request->after_job;
                $job->save();

                // $after_job = new App\predecessor_model;
                // $after_job->predecessor_job_id = $request->predecessor_job_name;
                // $after_job->job_id = $job_id;
                // $after_job->enterprise_id= Auth::user()->enterprise_id;
                // $after_job->save();
            }



            // $contact = new App\plant_contact_model;
            // $contact->contact_name = $request->contact_name;
            // $contact->contact_email_id = $request->contact_email;
            // $contact->contact_number = $request->contact_no;
            // $contact->plant_id = $plant_id;
            // $contact->enterprise_id = '1';
            // $contact->save();
            //
            // $address = new App\plant_address_model;
            // $address->street = $request->street;
            // $address->city = $request->city;
            // $address->state = $request->state;
            // $address->country = $request->country;
            // $address->region = $request->region;
            // $address->zip_code =$request->zip_code;
            // $address->enterprise_id ='1';
            // $address->plant_id = $plant_id;
            // $address->save();
    }

    public function fetch_job_name(Request $request)
    {
      $job_name = $request->job_name;
      $results = array();
      $results;
      $query = "select job_name from job where job_name LIKE '%$job_name%' and enterprise_id = ".Auth::user()->enterprise_id;
      $res   = \DB::select($query);
      foreach ($res as $query)
        {
            array_push($results,$query->job_name);
        }
      return $results;
    }

    public function fetch_task_type(Request $request)
    {
      $task_type = $request->task_type;
      $results = array();
      $results;
      $query = "select task_type_name from task_type where task_type_name LIKE '%$task_type%' and enterprise_id = ".Auth::user()->enterprise_id;
      $res   = \DB::select($query);
      foreach ($res as $query)
        {
            array_push($results,$query->task_type_name);
        }
      return $results;
    }

    public function fetch_task_name(Request $request)
    {
      $task_name = $request->task_name;
      $results = array();
      $results;
      $query = "select task_name from task where task_name LIKE '%$task_name%' and enterprise_id = ".Auth::user()->enterprise_id;
      $res   = \DB::select($query);
      foreach ($res as $query)
        {
            array_push($results,$query->task_name);
        }
      return $results;
    }

    public function show(Request $request)
    {
        $job_name_search = $request->job_name_search;
        $task_type_search = $request->task_type_search;
        $task_name_search = $request->task_name_search;
        $query_data = "";
        if($job_name_search != "")
        {
          $query_data = " job.job_name = '". $job_name_search. "' and ";
        }
        if($task_type_search != "")
        {
          $query_data = $query_data . " task_type.task_type_name = '". $task_type_search. "' and ";
        }
        if($task_name_search != "")
        {
          $query_data = $query_data . " task.task_name = '". $task_name_search. "' and ";
        }
        $query_data = substr($query_data, 0, -4);
        $data = \DB::select("select job.*,task.task_name,task.task_type_id,task_type.task_type_name FROM job JOIN task AS task ON job.task_id = task.task_id JOIN task_type as task_type ON task.task_type_id = task_type.task_type_id where " .$query_data );
        if (empty($data)) {
            return "0";
        }
        else {
          return $data;
        }
    }

    public function get_jobs(Request $request){
      $data = \DB::select("select job.*,task.task_name,task.task_type_id,task_type.task_type_name FROM job JOIN task AS task ON job.task_id = task.task_id JOIN task_type as task_type ON task.task_type_id = task_type.task_type_id;");
      return $data;
    }

    public function get_frequency_for_scheduler(Request $request)
    {
      $job_id = $request->job_id;
      $data = \DB::select("select job.*,peri.frequency_id FROM job JOIN periodic AS peri ON job.job_id = peri.job_id where job.job_id =" .$job_id);
      return $data;
    }

    public function get_after_job_for_scheduler(Request $request)
    {
      $job_id = $request->job_id;
      $data = \DB::select("select job.* FROM job where job.job_id =" .$job_id);
      return $data;
    }

    public function scheduler_update(Request $request)
    {
      $updated_at = date('Y-m-d H:i:s', time());
      $job = new App\scheduler_model;
      $job_id = $request->job_id;

      //$data = \DB::select("delete from after_job where job_id='$job_id' and enterprise_id = ".Auth::user()->enterprise_id);
      $data = \DB::select("delete from periodic where job_id='$job_id' and enterprise_id = ".Auth::user()->enterprise_id);

      if($request->start_type_id == "1")
      {
                  if($request->period_immediately=="1")
                  {
                    $data =   \DB::table('job')
                   ->where('job_id',  $job_id)
                   ->where('enterprise_id',Auth::user()->enterprise_id)
                   ->limit(1)
                   ->update(array('job_name' => $request->job_name,
                   'job_type' =>   "T",
                   'task_id' =>  $request->task_name,
                   'job_priority' =>  $request->job_priority,
                   'job_status' =>  "scheduled",
                   'start_type_id' =>  $request->start_type_id,
                   'active' =>  $request->active,
                   'updated_at' =>  $updated_at,
                   'updated_by' => Auth::user()->email,

                   'job_start_date' =>  $request->job_start_date,

                   'job_last_run_date' =>  $request->job_last_run_date,
                   'job_start_time' =>  $request->job_start_time,
                   'job_end_date' =>  $request->job_end_date,
                   'job_end_time' =>  $request->job_end_time,
                   'job_last_run_time' =>  $request->job_last_run_time,
                   'periodic' =>  $request->period_immediately,
                   'after_job' => "0",));

                      $periodic = new App\periodic;
                      $periodic->frequency_id = $request->frequency;
                      $periodic->enterprise_id= Auth::user()->enterprise_id;
                      $periodic->job_id = $job_id;
                      $periodic->save();
                  }
                  else {

                    $currTimeStamp = date('c');
                    $date = date('Y-m-d', time());
                    $h = date('H', time());
                    $i = date('i', time());
                    $i = $i +1;
                    $s = date('s', time());
                    $time_imm = $h .':'.$i.':'.$s;

                    $data =   \DB::table('job')
                   ->where('job_id',  $job_id)
                   ->where('enterprise_id',Auth::user()->enterprise_id)
                   ->limit(1)
                   ->update(array('job_name' => $request->job_name,
                   'job_type' =>    "T",
                   'task_id' =>  $request->task_name,
                   'job_priority' =>  $request->job_priority,
                   'job_status' =>  "scheduled",
                   'start_type_id' =>  $request->start_type_id,
                   'active' =>  $request->active,
                   'updated_at' =>  $updated_at,
                   'updated_by' => Auth::user()->email,

                   'job_start_date' =>  $request->job_start_date,
                   'job_last_run_date' =>  $request->job_last_run_date,
                   'job_start_time' =>  $time_imm,
                   'job_last_run_time' =>  $time_imm,
                   'job_end_date' =>  $request->job_end_date,
                   'job_end_time' =>  $time_imm,
                   'periodic' =>  "0",
                   'after_job' => "0",));
                  }
      }
      elseif ($request->start_type_id=="2") {
            if($request->periodic_date_time=="1")
            {
                  $data =   \DB::table('job')
                 ->where('job_id',  $job_id)
                 ->limit(1)
                 ->update(array('job_name' => $request->job_name,
                 'job_type' =>    "T",
                 'task_id' =>  $request->task_name,
                 'job_priority' =>  $request->job_priority,
                 'job_status' =>  "scheduled",
                 'start_type_id' =>  $request->start_type_id,
                 'enterprise_id' =>  Auth::user()->enterprise_id,
                 'active' =>  $request->active,
                 'updated_at' =>  $updated_at,
                 'updated_by' => Auth::user()->email,

                 'job_start_date' =>  $request->job_start_date,
                 'job_last_run_date' =>  $request->job_last_run_date,
                 'job_start_time' =>  $request->job_start_time,
                 'job_last_run_time' =>  $request->job_last_run_time,
                 'job_end_date' =>  $request->job_end_date,
                 'job_end_time' =>  $request->job_end_time,
                 'periodic' => $request->periodic_date_time,
                 'after_job' => "0",));

                  $periodic = new App\periodic;
                  $periodic->frequency_id = $request->frequency_date_time;
                  $periodic->job_id = $job_id;
                    $periodic->enterprise_id= Auth::user()->enterprise_id;
                  $periodic->save();
            }
            else {
              $data =   \DB::table('job')
             ->where('job_id',  $job_id)
             ->limit(1)
             ->update(array('job_name' => $request->job_name,
             'job_type' =>    "T",
             'task_id' =>  $request->task_name,
             'job_priority' =>  $request->job_priority,
             'job_status' =>  "scheduled",
             'start_type_id' =>  $request->start_type_id,
             'enterprise_id' =>  Auth::user()->enterprise_id,
             'active' =>  $request->active,
             'updated_at' =>  $updated_at,
             'updated_by' => Auth::user()->email,

             'job_start_date' =>  $request->job_start_date,
             'job_last_run_date' =>  $request->job_last_run_date,
             'job_start_time' =>  $request->job_start_time,
             'job_last_run_time' =>  $request->job_last_run_time,
             'job_end_date' =>  $request->job_end_date,
             'job_end_time' =>  $request->job_end_time,
             'periodic' => "0",
             'after_job' => "0",));
            }
      }
      elseif ($request->start_type_id=="3") {
                      $data =   \DB::table('job')
                     ->where('job_id',  $job_id)
                     ->limit(1)
                     ->update(array('job_name' => $request->job_name,
                     'job_type' =>   "T",
                     'task_id' =>  $request->task_name,
                     'job_priority' =>  $request->job_priority,
                     'job_status' =>  "scheduled",
                     'start_type_id' =>  $request->start_type_id,
                     'enterprise_id' =>  Auth::user()->enterprise_id,
                     'active' =>  $request->active,
                     'updated_at' =>  $updated_at,
                     'updated_by' => Auth::user()->email,
                     'job_start_date' =>  $request->job_start_date,
                     'job_last_run_date' =>  $request->job_last_run_date,
                     'job_start_time' =>  $request->job_start_time,
                     'job_last_run_time' =>  $request->job_last_run_time,
                     'job_end_date' =>  $request->job_end_date,
                     'job_end_time' =>  $request->job_end_time,
                     'periodic' => "0",
                     'after_job' =>  $request->after_job,
                   ));

                  //  $after_job = new App\predecessor_model;
                  //  $after_job->predecessor_job_id = $request->predecessor_job_name;
                  //  $after_job->job_id = $job_id;
                  // $after_job->enterprise_id= Auth::user()->enterprise_id;
                  //  $after_job->save();
         }
    }


        public function destroy(Request $request)
        {
           $job_id = $request->job_id;
           //$data = \DB::select("delete from after_job where job_id='$job_id'");
           $data = \DB::select("delete from periodic where job_id='$job_id'");
           $data = \DB::select("delete from job where job_id='$job_id'");
           return $data;
        }

}
