<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class predecessor_model extends Model
{
  protected $table = 'after_job';
  public $timestamps = false;
}
