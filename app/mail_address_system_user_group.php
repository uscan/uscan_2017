<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mail_address_system_user_group extends Model
{
    //
    protected $table = 'mail_communicaion_user_group';
    protected $dates = ['deleted_at'];
}
