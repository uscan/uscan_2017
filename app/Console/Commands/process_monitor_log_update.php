<?php

namespace App\Console\Commands;
use App;
use Illuminate\Console\Command;

class process_monitor_log_update extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    }


    function insert_log_data($log_id,$enterprise_id,$step_no,$description)
    {
        $date_time = date('Y-m-d H:i:s');
        $log = new App\process_monitor_desc_table;
        $log->log_id = $log_id;
        $log->enterprise_id = $enterprise_id;
        $log->step_no = $step_no;
        $log->description = $description;
        $log->date_time = $date_time;
        $log->save();

        \Log::info("LOG ID: ".$log_id);

        // $sql = "insert into  `uscan_master`.`process_monitor_descs` (`log_id`,`enterprise_id`,`step_no`,`description`,`date_time`) values ('$log_id','$enterprise_id','$step_no','$description','$date_time')";
        //
        // if ($GLOBALS['conn_master']->query($sql) === TRUE) {
        //     echo "successfully inserted log</br>";
        // } else {
        //     echo "Error: " . $sql . "<br>" . $GLOBALS['conn_master']->error;
        // }
    }

    function update_log_state($log_id,$state)
    {
        $date_time = date('Y-m-d H:i:s');
        \Log::info("Date time : ".$date_time);
        \Log::info("state : ".$state);
        $update_current_no =   \DB::table('process_monitor')
        ->where('log_id',  $log_id)
        ->limit(1)
        ->update(array('state' => $state,
                        'end_date' => $date_time,
                      ));
    }

}
