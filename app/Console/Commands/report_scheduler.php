<?php

namespace App\Console\Commands;
use DB;
use Illuminate\Console\Command;

class report_scheduler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //$this->report_generator();
    }

    public function report_generator(string $job_id,string $log_id,string $report_id,string $enterprise_id)
    //public function report_generator()
    {
      $step_no = 0;
      $date_time_for_file = date('Y-m-d His');
      $process_monitor_log_update = new process_monitor_log_update();
      $db_connection = DB::connection('mysql_transaction');
      $step_no     = $step_no + 1;
      $description = "Task Started";
      $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

      // $plant_id = $request->plant_id;
      // $plant_ids = str_replace(",","','",$plant_id);
      // $business_unit = $request->business_unit;
      // $business_units = str_replace(",","','",$business_unit);
      // $product_group = $request->product_group;
      // $product_groups = str_replace(",","','",$product_group);
      // $cust_id = $request->cust_id;
      // $cust_name = $request->cust_name;
      // $cust_ids = str_replace(",","','",$cust_id);
      // $plant_status = $request->plant_status;
      // $report_id = "80000001";
      // $enterprise_id = "0010000002";
      //$fp1 = fopen(dirname(__FILE__).'/Report_log.txt', 'w+');

      $select_prd_group = "Select product_group from uscan_master.report_name_product_group where report_id = '".$report_id."' and enterprise_id = '".$enterprise_id."'";
      $select_prd_group_data = $db_connection->select($select_prd_group);
      $product_groups = "";
      for($i=0;$i<count($select_prd_group_data);$i++)
      {
        if($i==0)
        {
          $product_groups = $select_prd_group_data[$i]->product_group;
        }
        else {
          $product_groups = $product_groups."','".$select_prd_group_data[$i]->product_group;
        }
      }

       $select_business_units = "Select business_unit from uscan_master.report_name_business_unit where report_id = '".$report_id."' and enterprise_id = '".$enterprise_id."'";
       $select_business_units_data = $db_connection->select($select_business_units);
       $business_units = "";
       for($i=0;$i<count($select_business_units_data);$i++)
       {
         if($i==0)
         {
           $business_units = $select_business_units_data[$i]->business_unit;
         }
         else {
           $business_units = $business_units."','".$select_business_units_data[$i]->business_unit;
         }
       }

       $select_plant = "Select plant_id from uscan_master.report_name_plant where report_id = '".$report_id."' and enterprise_id = '".$enterprise_id."'";
       $select_plant_data = $db_connection->select($select_plant);
       $plant_ids = "";
       for($i=0;$i<count($select_plant_data);$i++)
       {
        if($i==0)
        {
          $plant_ids = $select_plant_data[$i]->plant_id;
        }
        else {
          $plant_ids = $plant_ids."','".$select_plant_data[$i]->plant_id;
        }
       }

       $select_customers = "Select customer_id from uscan_master.report_name_customer where report_id = '".$report_id."' and enterprise_id = '".$enterprise_id."'";
       $select_customers_data = $db_connection->select($select_customers);
       $customer_ids = "";
       for($i=0;$i<count($select_customers_data);$i++)
       {
        if($i==0)
        {
          $customer_ids = $select_customers_data[$i]->customer_id;
        }
        else {
          $customer_ids = $customer_ids."','".$select_customers_data[$i]->customer_id;
        }
       }
      //$plant_ids = "0040000002','0040000003','0040000004','0040000005','0040000006','0040000007','0040000008','0040000009','0040000010','0040000011','0040000012','0040000013','0040000014','0040000015','0040000016','0040000017','0040000018','0040000019','0040000020','0040000021','0040000022','0040000023','0040000024','0040000025','0040000026','0040000027','0040000028','0040000029','0040000030','0040000031','0040000032','0040000033','0040000034','0040000035','0040000036','0040000037','0040000038','0040000039','0040000040','0040000041','0040000042','0040000043','0040000044','0040000045','0040000046','0040000047','0040000048','0040000049','0040000050','0040000051','0040000052','0040000053','0040000054','0040000055','0040000056','0040000057','0040000058','0040000059','0040000060','0040000061','0040000062','0040000063','0040000064','0040000065','0040000066','0040000067','0040000068','0040000069','0040000070','0040000071','0040000072','0040000073','0040000074','0040000075','0040000076','0040000077','0040000078','0040000079','0040000080','0040000081','0040000082','0040000083','0040000084','0040000085','0040000086','0040000087','0040000088','0040000089','0040000090','0040000091','0040000092','0040000093','0040000094','0040000095','0040000096','0040000097','0040000098','0040000099','0040000100','0040000101";
      //$business_units = "Global Valvetrain','Rings & Liners and Valve Seats & Guides','Global Pistons','Sealing & Gaskets and Systems Protection','Bearings and Ignition','Administrative','Lighting','Administrative & Technical Centers";
      //$product_groups = "Global Valvetrain','Valve Seats and Guides','Pistons','Camshafts','Sealing and Gaskets','Systems Protection','Rings/Liners','Ignition','Engine Bearings','Administrative','Lighting','Technical Centers";
      $cust_name = "General Motor Corporation,FCA Production,FCA Mopar,Ford Motor Company,PSA";
      //$cust_ids = "0030000002','0030000003','0030000004','0030000005','0030000006";
      // fwrite($fp1,"Plant Ids : ".$plant_ids);
      // fwrite($fp1,"\r\n");
      // fwrite($fp1,"Business Units : ".$business_units);
      // fwrite($fp1,"\r\n");
      // fwrite($fp1,"Product Groups : ".$product_groups);
      // fwrite($fp1,"\r\n");
      // fwrite($fp1,$cust_name);
      // fwrite($fp1,"\r\n");
      // fwrite($fp1,"Customer Ids : ".$customer_ids);
      // fwrite($fp1,"\r\n");
      // fwrite($fp1,$plant_status);
      // fwrite($fp1,"\r\n");
      $query_data = "";
      if($business_units != ""){
        if($query_data != "")
        {
            $query_data = $query_data. " and p.business_unit IN('".$business_units."')";
        }
        else {
            $query_data = $query_data. " p.business_unit IN('".$business_units."')";
        }
      }
      if($product_groups != ""){
        if($query_data != "")
        {
            $query_data = $query_data. " and p.product_group IN('".$product_groups."')";
        }
        else {
            $query_data = $query_data. " p.product_group IN('".$product_groups."')";
        }
      }
      if($plant_ids != ""){
        if($query_data != "")
        {
          $query_data = $query_data. " and p.plant_id IN('".$plant_ids."')";
        }
        else {
          $query_data = $query_data. " p.plant_id IN('".$plant_ids."')";
        }
      }
      if($customer_ids != ""){
        if($query_data != "")
        {
          $query_data = $query_data. " and cc.customer_id IN ('".$customer_ids."')";
        }
        else {
          $query_data = $query_data. " cc.customer_id IN ('".$customer_ids."')";
        }

      }
      $query = "SELECT p.duns_no,p.plant_name,pc.supplier_code,pc.customer_id
      FROM plant p JOIN plant_customer_ref pc ON p.plant_id =  pc.plant_id
      JOIN customer_child cc ON pc.customer_id = cc.customer_id WHERE ".$query_data;
      $result = \DB::select($query);
      // fwrite($fp1,$query);
      $mfg_duns = "";
      $supplier_code = "";
      $customer_id = "";
      for($i=0;$i<count($result);$i++)
      {
        if($result[$i]->supplier_code != $result[$i]->duns_no)
        {
          if($supplier_code == "")
          {
            $supplier_code = "'".$result[$i]->supplier_code."'";
          }
          else
          {
            $supplier_code = $supplier_code.",'".$result[$i]->supplier_code."'";
          }
        }
        else {
          if($mfg_duns == "")
          {
             $mfg_duns = "'".$result[$i]->duns_no."'";
          }
          else {
             $mfg_duns = $mfg_duns.",'". $result[$i]->duns_no."'";
          }
        }
        if($customer_id == "")
        {
           $customer_id = "'".$result[$i]->customer_id."'";
        }
        else {
           $customer_id = $customer_id.",'". $result[$i]->customer_id."'";
        }
      }
      if($mfg_duns != "" && $supplier_code != "")
      {
        $master_dashboard = "Select * from uscan_transactions.master_dashboard
        where supplier_code in (".$mfg_duns.",".$supplier_code.") AND customer_id IN (".$customer_id.")
        ORDER BY plant_name ASC";

        $master_plant_list = "SELECT DISTINCT(plant_id),plant_name FROM uscan_transactions.master_dashboard
        WHERE supplier_code IN (".$mfg_duns.",".$supplier_code.") AND customer_id IN (".$customer_id.") ORDER BY plant_name ASC";

        $master_customer_list = "SELECT DISTINCT(customer_id),customer_name FROM uscan_transactions.master_dashboard
        WHERE supplier_code IN (".$mfg_duns.",".$supplier_code.") AND customer_id IN (".$customer_id.") ORDER BY customer_name ASC";
      }
      elseif ($mfg_duns != "" && $supplier_code == "") {
        $master_dashboard = "Select * from uscan_transactions.master_dashboard
        where supplier_code in (".$mfg_duns.") AND customer_id IN (".$customer_id.") ORDER BY plant_name ASC";

        $master_plant_list = "SELECT DISTINCT(plant_id),plant_name FROM uscan_transactions.master_dashboard
        WHERE supplier_code IN (".$mfg_duns.") AND customer_id IN (".$customer_id.") ORDER BY plant_name ASC";

        $master_customer_list = "SELECT DISTINCT(customer_id),customer_name FROM uscan_transactions.master_dashboard
        WHERE supplier_code IN (".$mfg_duns.") AND customer_id IN (".$customer_id.") ORDER BY customer_name ASC";
      }
      elseif ($mfg_duns == "" && $supplier_code != "") {
        $master_dashboard = "SELECT * from uscan_transactions.master_dashboard
        WHERE supplier_code in (".$supplier_code.") AND customer_id IN (".$customer_id.") ORDER BY plant_name ASC";

        $master_plant_list = "SELECT DISTINCT(plant_id),plant_name FROM uscan_transactions.master_dashboard
        WHERE supplier_code IN (".$supplier_code.") AND customer_id IN (".$customer_id.") ORDER BY plant_name ASC";

        $master_customer_list = "SELECT DISTINCT(customer_id),customer_name FROM uscan_transactions.master_dashboard
        WHERE supplier_code IN (".$supplier_code.") AND customer_id IN (".$customer_id.") ORDER BY customer_name ASC";
      }

      $master_dashboard_data = $db_connection->select($master_dashboard);
      $master_dashboard_plant = $db_connection->select($master_plant_list);
      $master_dashboard_customer = $db_connection->Select($master_customer_list);

      // fwrite($fp1,"\n");
      // fwrite($fp1,"Inside Master Data");
      // fwrite($fp1,"\n");
      // fwrite($fp1,$master_dashboard);
      // fwrite($fp1,"\n");
      // fwrite($fp1,$master_plant_list);
      // fwrite($fp1,"\n");
      // fwrite($fp1,$master_customer_list);
      if (empty($master_dashboard_data)) {
        $step_no     = $step_no + 1;
        $description = "Report Generation Failed";
        $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);
        return "Report Generation Failed for Logid : ".$log_id;
      }
      else {
        $report_file_name = "Master_Dashboard_".$log_id."_".$date_time_for_file.".xls";
        $file_name = "/var/www/uscan/uscan/public/reports/$report_file_name";
        $this->generate_html_table($master_dashboard_customer,$master_dashboard_plant,$master_dashboard_data,$file_name);
        $step_no     = $step_no + 1;
        $description = "Report Generated Successfully";
        $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

        $subject = 'Scheduled Report';
        //$FileName  = "GM Bidlist" . "_" . $log_id . "_" . $date_time_for_file . ".xls";
        // $path = "C:\\xampp\htdocs\uscan\public\crawler\documents\\ford\\ford_q1_scores\\Test Data.xls";

        $distribution_strategy = new distribution_strategy();
        $distribution_strategy_data = $distribution_strategy->distribution_strategy_check($job_id,$enterprise_id,$subject,$file_name,$report_file_name);

        if ($distribution_strategy_data)
        {
          $step_no = $step_no + 1;
          $description = $distribution_strategy_data;
          $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);
        }

        $process_monitor_log_update_new = $process_monitor_log_update->update_log_state($log_id,'Finished');
        return "Report Generated Successfully for Logid : ".$log_id;
        //var_dump($master_dashboard_data);
        //return array($master_dashboard_data,"master","master",$master_dashboard_plant,$master_dashboard_customer);
      }
    }

    public function generate_html_table($customer_list,$plant_list,$master_data,$file_name)
    {
      echo count($customer_list)."\n";
      echo count($plant_list)."\n";
      echo count($master_data)."\n";
      $html_table_data = "<table border='1'><tr><td>CUSTOMERS / PLANTS</td>";
      for ($i=0; $i < count($customer_list) ; $i++) {
        echo $customer_list[$i]->customer_name."\n";
        $html_table_data = $html_table_data . "<td>".$customer_list[$i]->customer_name."</td>";
      }
      $html_table_data = $html_table_data ."</tr>";
      for($j=0;$j < count($plant_list);$j++)
      {
        //echo $plant_list[$j]->plant_name."\n";
        $plant_id = $plant_list[$j]->plant_id;
        $plant_name = $plant_list[$j]->plant_name;
        $plant_row = "<tr><td>".$plant_list[$j]->plant_name."</td>";
        for($k=0;$k <count($customer_list);$k++)
        {
          $customer_id = $customer_list[$k]->customer_id;
          $customer_name = $customer_list[$k]->customer_name;
          $jk_plant = 0;
          for($jk=0;$jk < count($master_data);$jk++)
          {
            if($plant_id == $master_data[$jk]->plant_id && $customer_id == $master_data[$jk]->customer_id)
            {
              if($customer_name == "General Motor Corporation")
              {
                echo "Plant Name : ".$plant_name."\n";
                echo $master_data[$jk]->quality.",".$master_data[$jk]->supplychain.",".$master_data[$jk]->cca."\n";
                if($master_data[$jk]->quality != "" && $master_data[$jk]->supplychain != "" && $master_data[$jk]->cca != "")
                {
                  $gm_td = $this->set_gmtd($master_data[$jk]->quality,$master_data[$jk]->supplychain,$master_data[$jk]->cca);
                  $plant_row = $plant_row . $gm_td;
                  $jk_plant = 1;
                }
                else
                {
                    $plant_row = $plant_row . "<td></td>";
                    $jk_plant = 1;
                }
              }
              else if ($customer_name == "PSA")
              {
                $psa_td = $this->set_psatd($master_data[$jk]->quality_psa,$master_data[$jk]->supplychain_psa,$master_data[$jk]->aftersales_psa);
                $plant_row = $plant_row . $psa_td;
                $jk_plant = 1;
              }
              else if ($customer_name == "Ford Motor Company")
              {
                $ford_td = $this->set_fordtd($master_data[$jk]->present_q1_status);
                $plant_row = $plant_row . $ford_td;
                $jk_plant = 1;
              }
              else if($customer_name == "FCA Production")
              {
                $overall_prdn = $master_data[$jk]->overall_prdn;
                // echo "Production : ".$overall_prdn."\n";
                // echo "Production Length : ".strlen($overall_prdn)."\n";
                $overall_prdn = substr($overall_prdn,0,strlen($overall_prdn)-1);
                // echo "Production without Perc : ".substr($overall_prdn,0,strlen($overall_prdn))."\n";
                if(strlen($overall_prdn) == 0)
                {
                    // echo "Inside empty producion"."\n";
                    $plant_row = $plant_row . "<td></td>";
                    $jk_plant = 1;
                }
                else {
                    $fcaprdn_td = $this->set_fcatd($overall_prdn);
                    // echo "FCA Prdn TD : ".$fcaprdn_td."\n";
                    $plant_row = $plant_row.$fcaprdn_td;
                    $jk_plant = 1;
                }

              }
              else if($customer_name == "FCA Mopar")
              {
                $overall_mopar = $master_data[$jk]->overall_mopar;
                // echo "Mopar : ".$overall_mopar."\n";
                // echo "Mopar Length : ".strlen($overall_mopar)."\n";
                $overall_mopar = substr($overall_mopar,0,strlen($overall_mopar)-1);
                // echo "Mopar without Perc : ".substr($overall_mopar,0,strlen($overall_mopar))."\n";
                if(strlen($overall_mopar) == 0)
                {
                    echo "Inside empty mophar"."\n";
                    $plant_row = $plant_row . "<td></td>";
                    $jk_plant = 1;
                }
                else {
                  $fcamopar_td = $this->set_fcatd($overall_mopar);
                  echo "FCA Mopar TD : ".$fcamopar_td."\n";
                  $plant_row = $plant_row.$fcamopar_td;
                  $jk_plant = 1;
                }

              }
              else
              {
                $jk_plant = 0;
              }

            }
          }
          if($jk_plant == 0)
          {
            $plant_row = $plant_row . "<td></td>";
          }
        //  $plant_row = $plant_row . "</tr>";

        }
        $html_table_data = $html_table_data . $plant_row."</tr>";
        //$html_table_data = $html_table_data ."<tr id = "."'".$plant_list[$j]->plant_id."'"."><td>".$plant_list[$j]->plant_name."</td></tr>";
      }
      $html_table_data = $html_table_data ."</table>";
      echo "\n".$html_table_data;
      $this->generate_excel($html_table_data,$file_name);
    }

    public function generate_excel($html_table_data,$report_file_name)
    {

      // $file="E:/xampp/htdocs/uscan/public/reports/Master_dashboard_".$date_time_for_file.".xls";
      //$file="/var/www/uscan/uscan/public/reports/".$file_name;
      file_put_contents($report_file_name,$html_table_data);
    }
    public function set_gmtd($quality,$supplychain,$cca)
    {
      $tdcolor;
      if($quality == 0 && $supplychain == "NR"){
        if($cca == "R"){
          $tdcolor = "#FF0000"; // RED Color
        }
        else if ($cca == "G") {
            $tdcolor = "#008000";  // GREEN Color
        }
        else {
            $tdcolor = "#FFFF00";  // YELLOW Color
        }
      }
      else {
        if ($quality < 80 || $supplychain < 75 || $cca == "R") {
          $tdcolor = "#FF0000"; // RED Color
        }
        else if($quality >= 80 && $supplychain >=85 && $cca == "G")
        {
          $tdcolor = "#008000";  // GREEN Color
        }
        else {
          $tdcolor = "#FFFF00";  // YELLOW Color
        }
      }
      $td_value = "<td bgcolor = ".$tdcolor."></td>";
      return $td_value;
    }
    public function set_fordtd($q1_score)
    {
      // var tdid = duns_no+"_ford";
      // var data_tkn = customer_name + " : " + plant_name;
      $tdcolor="";
      if($q1_score == "R" || $q1_score == "X"){
        $tdcolor = "#FF0000"; // RED Color
      }
      else if($q1_score == "A" || $q1_score == "D"){
        $tdcolor = "#FFFF00";  // YELLOW Color
      }
      else if($q1_score == "Y" || $q1_score == "W" || $q1_score == "2"){
        $tdcolor = "#008000";  // GREEN Color
      }
      else {
        $tdcolor = "#808080";  // Grey Color
      }
      //$td_value = "<td id=".$tdid." data-token = '".$data_tkn."' class='td_click' bgcolor = ".$tdcolor."></td>";
      $td_value = "<td bgcolor = ".$tdcolor."></td>";
      return $td_value;
    }
    public function set_fcatd($overall)
    {
      // $tdid = "";
      // if($customer_name == "FCA PRODUCTION")
      // {
      //   $tdid = $supplier_code."_prdn";
      // }
      // else
      // {
      //   $tdid = $supplier_code."_mopar";
      // }
      // $data_tkn = $customer_name . " : " .$plant_name;
      //echo "overall Value : ".$overall."\n";
      if($overall < 70){

          $tdcolor = "#FF0000"; // RED Color
         }
        else if ($overall >= 70 && $overall < 90) {
          $tdcolor = "#FFFF00";  // YELLOW Color

        }
        else if($overall >= 90) {
             $tdcolor = "#008000";  // GREEN Color
        }
        else {
            $tdcolor = "#FFFFFF";
        }
      //  echo "overall color : ".$tdcolor."\n";
          //$td_value = "<td data-id = '".$tdid."' data-token = '".$data_tkn."'  class='fca_click' bgcolor = '".$tdcolor+"'></td>";
          $td_value = "<td bgcolor = ".$tdcolor."></td>";
          return $td_value;
    }
    public function set_psatd($quality,$supplychain,$aftersales)
    {
      // var tdid = duns_no+"_psa";
      // var data_tkn = customer_name + " : " + plant_name;
      $tdcolor="";
      if($quality == "U" && $supplychain == "U" && $aftersales == "U")
      {
        $tdcolor = "#FFFFFF"; // WHITE Color
      }
      else if($quality == "U" && $supplychain == "U")
      {
        if($aftersales < 75)
        {
          $tdcolor = "#FF0000"; // RED Color
        }
        else if ($aftersales >= 75 && $aftersales < 85) {
            $tdcolor = "#FFFF00";  // YELLOW Color
        }
        else {
            $tdcolor = "#008000";  // GREEN Color
        }
      }
      else if($quality == "U" && $aftersales == "U")
      {
        if($supplychain < 75)
        {
          $tdcolor = "#FF0000"; // RED Color
        }
        else if ($supplychain >= 75 && $supplychain < 85) {
            $tdcolor = "#FFFF00";  // YELLOW Color
        }
        else {
            $tdcolor = "#008000";  // GREEN Color
        }
      }
      else if($supplychain == "U" && $aftersales == "U")
      {
        if($quality < 80){
          $tdcolor = "#FF0000"; // RED Color
        }
        else {
            $tdcolor = "#008000";  // GREEN Color
        }
      }
      else if($quality == "U")
      {
        if ($supplychain < 75 || $aftersales < 75)
        {
          $tdcolor = "#FF0000"; // RED Color
        }
        else if($supplychain >= 85 && $aftersales >= 85)
        {
          $tdcolor = "#008000";  // GREEN Color
        }
        else
        {
          $tdcolor = "#FFFF00";  // YELLOW Color
        }
      }
      else if($supplychain == "U")
      {
        if ($quality < 80 || $aftersales < 75)
        {
          $tdcolor = "#FF0000"; // RED Color
        }
        else if($quality >= 80 && $aftersales >= 85)
        {
          $tdcolor = "#008000";  // GREEN Color
        }
        else
        {
          $tdcolor = "#FFFF00";  // YELLOW Color
        }
      }
      else if($aftersales == "U")
      {
        if ($quality < 80 || $supplychain < 75)
        {
          $tdcolor = "#FF0000"; // RED Color
        }
        else if($quality >= 80 && $supplychain >= 85)
        {
          $tdcolor = "#008000";  // GREEN Color
        }
        else
        {
          $tdcolor = "#FFFF00";  // YELLOW Color
        }
      }
      else
      {
        if ($quality < 80 || $supplychain < 75 || $aftersales < 75) {
          $tdcolor = "#FF0000"; // RED Color
        }
        else if($quality >= 80 && $supplychain >=85 && $aftersales >= 85)
        {
          $tdcolor = "#008000";  // GREEN Color
        }
        else
        {
          $tdcolor = "#FFFF00";  // YELLOW Color
        }
      }
      //$td_value = "<td id=".$tdid." data-token = '".$data_tkn."' class='td_click' bgcolor = ".$tdcolor."></td>";
      $td_value = "<td bgcolor = ".$tdcolor."></td>";
      return $td_value;
    }
}
