<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class gm_bidlist extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      \Log::info("Hello working pavan");
        // return "Hello working pavan";
    }

    public function get_gm_bidlist_hourly(string $job_id) {
          return "job executed successfully Cust GM job id: $job_id";
    }

    public function get_gm_bidlist(string $job_id,string $log_id,string $user_name_parameter,string $password_parameter,string $task_id,string $enterprise_id) {

          $process_monitor_log_update = new process_monitor_log_update();


                include('/var/www/uscan/uscan/public/crawler/general/db_connect.php');
                include('/var/www/uscan/uscan/public/crawler/general/log_functions.php');
                include('/var/www/uscan/uscan/public/crawler/general/simple_html_dom.php');

                  $date_time_for_file = date('Y-m-d His');
                  echo $date_time_for_file;
                  $step_no            = 0;
                  $log_id             = $log_id;
                  $enterprise_id      = '0010000002';

              set_time_limit(0);

              $step_no     = $step_no + 1;
              $description = "Task Started";
              // $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              $cookie              = "cookie.txt";
              $currTime            = time();
              $date_time_for_mysql = date('Y-m-d H:i:s');
              $ch                  = curl_init();

              $login_user     = $user_name_parameter;
              $login_password = $password_parameter;

              $data_login = array(
                  'auth_mode' => 'basic',
                  'langSelect' => '1',
                  'submitTime' => $currTime,
                  'user' => $login_user,
                  'password' => $login_password,
                  'login' => 'Login'
              );
              $nav_url    = 'https://us.sso.covisint.com/sso?cmd=LOGIN';
              $fi_login   = http_build_query($data_login);
              curl_setopt($ch, CURLOPT_URL, $nav_url);
              curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
              curl_setopt($ch, CURLOPT_POST, true);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $fi_login);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_COOKIESESSION, true);
              curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name'); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              curl_setopt($ch, CURLOPT_TIMEOUT, 180);

              $answer = curl_exec($ch);

              $html_login_check = str_get_html($answer);
              $check = $html_login_check->find('.whatsNewTitleSm',0);

              if($check)
              {
                    $pos = strpos($check->innertext, "invalid password ");

                    if ($pos) {

                      $step_no = $step_no + 1;
                      $description = "Incorrect USER ID or PASSWORD Login Failed";
                      $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);
                      update_log_state($log_id,'Failed');

                      $step_no = $step_no + 1;
                      $description = "Task Completed";
                      $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

                      $GLOBALS['conn_master']->close();
                      $GLOBALS['conn_transaction']->close();

                      curl_close($ch);
                      exit;

                    }

              }
              else
              {

              $status = curl_getinfo($ch);
              if ($status['http_code'] == '200' or $status['http_code'] == '302') {
                  $status_info = "Successful";
              } else {
                  $status_info = "Failed";


              }

              $step_no     = $step_no + 1;
              $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url;
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              if (curl_error($ch)) {
                  echo curl_error($ch);
              }

              $nav_url_home = 'https://portal.covisint.com/web/portal/home';
              curl_setopt($ch, CURLOPT_URL, $nav_url_home);
              curl_setopt($ch, CURLOPT_POST, false);
              curl_setopt($ch, CURLOPT_POSTFIELDS, "");
              $answer1 = curl_exec($ch);

              $status = curl_getinfo($ch);
              if ($status['http_code'] == '200' or $status['http_code'] == '302') {
                  $status_info = "Successful";

                  $step_no     = $step_no + 1;
                  $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_home;
                  $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

                  $step_no     = $step_no + 1;
                  $description = "Login Successful";
                  $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);
              } else {
                  $status_info = "Failed";

                  $step_no     = $step_no + 1;
                  $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_home;
                  $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

                  $step_no     = $step_no + 1;
                  $description = "Login Failed";
                  $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);
              }

              if (curl_error($ch)) {
                  echo curl_error($ch);
              }



              $nav_url_saml = 'https://fedhub.covisint.com/fed/app/idp.saml20?entityID=https://game-osso-vip.iw.gm.com:443/gmgameosso-sp&TARGET=https://smarttdm-gqts.gmsupplypower.com/GQTSWeb/Index.jsp';
              curl_setopt($ch, CURLOPT_URL, $nav_url_saml);
              curl_setopt($ch, CURLOPT_HEADER, true);
              curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
              curl_setopt($ch, CURLOPT_COOKIESESSION, true);
              curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name'); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              curl_setopt($ch, CURLOPT_TIMEOUT, 180);

              $answer2 = curl_exec($ch);
              $status  = curl_getinfo($ch);
              if ($status['http_code'] == '200' or $status['http_code'] == '302') {
                  $status_info = "Successful";
              } else {
                  $status_info = "Failed";


              }
              $step_no     = $step_no + 1;
              $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_saml;
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              if (curl_error($ch)) {
                  echo curl_error($ch);
              }

              $html_str   = str_get_html($answer2);
              $saml_str   = $html_str->find('input', 0);
              $target_str = $html_str->find('input', 1);
              $relay_str  = $html_str->find('input', 2);

              $saml   = $saml_str->value;
              $target = $target_str->value;
              $relay  = $relay_str->value;


              $data            = array(
                  'SAMLResponse' => $saml,
                  'TARGET' => $target,
                  'RelayState' => $relay
              );
              $fi              = http_build_query($data);
              $nav_url_gateway = 'https://gateway.gmsupplypower.com:443/gmgameosso/Consumer/metaAlias/gm/gmsp/sp';
              curl_setopt($ch, CURLOPT_URL, $nav_url_gateway);
              curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
              curl_setopt($ch, CURLOPT_POST, true);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $fi);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_COOKIESESSION, true);
              curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name'); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              curl_setopt($ch, CURLOPT_TIMEOUT, 180);

              $answer3 = curl_exec($ch);

              $status = curl_getinfo($ch);
              if ($status['http_code'] == '200' or $status['http_code'] == '302') {
                  $status_info = "Successful";
              } else {
                  $status_info = "Failed";


              }
              $step_no     = $step_no + 1;
              $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_gateway;
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);


              if (curl_error($ch)) {
                  echo curl_error($ch);
              }


              $html_str_1 = str_get_html($answer3);

              $sunamcompositeadvice = $html_str_1->find('input', 0);

              $sunamcompositeadvice = $sunamcompositeadvice->value;


              $data = array(
                  'sunamcompositeadvice' => $sunamcompositeadvice
              );

              $fi_1               = http_build_query($data);
              $nav_url_gateway_gm = 'https://gateway.gmsupplypower.com:443/gmgameosso/UI/Login?realm=/gm/gmsp&service=gmidService&goto=https%3A%2F%2Fsmarttdm-gqts.gmsupplypower.com%3A443%2FGQTSWeb%2FIndex.jsp';
              curl_setopt($ch, CURLOPT_URL, $nav_url_gateway_gm);
              curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
              curl_setopt($ch, CURLOPT_POST, true);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $fi_1);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_COOKIESESSION, true);
              curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name'); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              curl_setopt($ch, CURLOPT_TIMEOUT, 180);

              $answer4 = curl_exec($ch);

              $status = curl_getinfo($ch);
              if ($status['http_code'] == '200' or $status['http_code'] == '302') {
                  $status_info = "Successful";
              } else {
                  $status_info = "Failed";


              }
              $step_no     = $step_no + 1;
              $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_gateway_gm;
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              if (curl_error($ch)) {
                  echo curl_error($ch);
              }


              $html_str_2 = str_get_html($answer4);

              $sunamcompositeadvice_4  = $html_str_2->find('input', 4);
              $sunamcompositeadvice_6  = $html_str_2->find('input', 6);
              $sunamcompositeadvice_8  = $html_str_2->find('input', 8);
              $sunamcompositeadvice_9  = $html_str_2->find('input', 9);
              $sunamcompositeadvice_10 = $html_str_2->find('input', 10);
              $sunamcompositeadvice_11 = $html_str_2->find('input', 11);

              foreach ($html_str_2->find('form') as $element)
                  $action_url = "https://gateway.gmsupplypower.com:443" . $element->action;


              $data_1 = array(
                  'IDToken0' => '',
                  'IDToken1' => '',
                  'IDToken2' => '',
                  'IDButton' => 'Submit',
                  'goto' => $sunamcompositeadvice_4->value,
                  'gotoOnFail' => '',
                  'SunQueryParamsString' => $sunamcompositeadvice_6->value,
                  'encoded' => 'true',
                  'dateStr' => $sunamcompositeadvice_8->value,
                  'pageTimeOut' => $sunamcompositeadvice_9->value,
                  'gx_charset' => $sunamcompositeadvice_10->value
              );

              $fi_2 = http_build_query($data_1);

              curl_setopt($ch, CURLOPT_URL, $action_url);
              curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
              curl_setopt($ch, CURLOPT_POST, true);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $fi_2);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_COOKIESESSION, true);
              curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name'); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
              curl_setopt($ch, CURLOPT_MAXREDIRS, 20);
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              curl_setopt($ch, CURLOPT_TIMEOUT, 180);

              $answer5 = curl_exec($ch);
              $status  = curl_getinfo($ch);
              if ($status['http_code'] == '200' or $status['http_code'] == '302') {
                  $status_info = "Successful";
              } else {
                  $status_info = "Failed";


              }
              $step_no     = $step_no + 1;
              $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $action_url;
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              if (curl_error($ch)) {
                  echo curl_error($ch);
              }

              $html_str_3 = str_get_html($answer5);

              foreach ($html_str_3->find('form') as $element)
                  $action_url_1 = $element->action;

              $SAMLResponse = $html_str_3->find('input', 0);

              $a = str_replace("&#xa;", "", $SAMLResponse->value);
              $b = str_replace("&#x2b;", "+", $a);


              $data_2 = array(
                  'SAMLResponse' => $b
              );

              $fi_3               = http_build_query($data_2);
              $nav_url_smart_gqts = 'https://smarttdm-gqts.gmsupplypower.com/saml2/sp/acs/post';
              curl_setopt($ch, CURLOPT_URL, $nav_url_smart_gqts);
              curl_setopt($ch, CURLOPT_POST, true);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $fi_3);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_COOKIESESSION, true);
              curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name'); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              curl_setopt($ch, CURLOPT_TIMEOUT, 180);

              $answer6 = curl_exec($ch);

              $status = curl_getinfo($ch);
              if ($status['http_code'] == '200' or $status['http_code'] == '302') {
                  $status_info = "Successful";
              } else {
                  $status_info = "Failed";


              }
              $step_no     = $step_no + 1;
              $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_smart_gqts;
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              if (curl_error($ch)) {
                  echo curl_error($ch);
              }

              $nav_url_search_sheet = 'https://smarttdm-gqts.gmsupplypower.com/GQTSWeb/metrics/bidlistSearchAction.do';
              curl_setopt($ch, CURLOPT_URL, $nav_url_search_sheet);
              curl_setopt($ch, CURLOPT_POST, false);
              curl_setopt($ch, CURLOPT_POSTFIELDS, "");
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_COOKIESESSION, true);
              curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name'); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              curl_setopt($ch, CURLOPT_TIMEOUT, 180);
              $answer7 = curl_exec($ch);
              $status  = curl_getinfo($ch);
              if ($status['http_code'] == '200' or $status['http_code'] == '302') {
                  $status_info = "Successful";
              } else {
                  $status_info = "Failed";


              }
              $step_no     = $step_no + 1;
              $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_search_sheet;
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              if (curl_error($ch)) {
                  echo curl_error($ch);
              }

              $data_final = array(
                  'bidlistReportSearchData.reportLevel' => 'GPSC',
                  'bidlistReportSearchData.ultimateDuns' => '555897164',
                  'bidlistReportSearchData.region' => '',
                  'bidlistReportSearchData.creativityTeamDesc' => '',
                  'bidlistReportSearchData.commodityDesc' => '',
                  'bidlistReportSearchData.directorDesc' => '',
                  'bidlistReportSearchData.majorFunctionDesc' => '',
                  'bidlistReportSearchData.subTeamDesc' => ''
              );

              $fi_final          = http_build_query($data_final);
              $nav_url_web_sheet = 'https://smarttdm-gqts.gmsupplypower.com/GQTSWeb/metrics/CurrentBidlistReport.do';
              curl_setopt($ch, CURLOPT_URL, $nav_url_web_sheet);
              curl_setopt($ch, CURLOPT_POST, true);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $fi_final);
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              curl_setopt($ch, CURLOPT_TIMEOUT, 180);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_COOKIESESSION, true);
              curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name'); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie); //could be empty, but cause problems on some hosts

              $answer8 = curl_exec($ch);

              $status = curl_getinfo($ch);
              if ($status['http_code'] == '200' or $status['http_code'] == '302') {
                  $status_info = "Successful";
              } else {
                  $status_info = "Failed";


              }
              $step_no     = $step_no + 1;
              $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_web_sheet;
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              $html = str_get_html($answer8);

              //$sched = array();
              foreach ($html->find('tr[class=gmfont]') as $event) {
                  $quality                           = trim($event->find('td', 0)->plaintext);
                  $supplychain                       = trim($event->find('td', 1)->plaintext);
                  $cca                               = trim($event->find('td', 2)->plaintext);
                  $country_risk                      = trim($event->find('td', 3)->plaintext);
                  $financial_risk                    = trim($event->find('td', 4)->plaintext);
                  $mmog_le                           = trim($event->find('td', 5)->plaintext);
                  $conflict_materials                = trim($event->find('td', 6)->plaintext);
                  $diversity                         = trim($event->find('td', 7)->plaintext);
                  $name                              = trim($event->find('td', 8)->plaintext);
                  $location                          = trim($event->find('td', 9)->plaintext);
                  $mfg_duns                          = trim($event->find('td', 10)->plaintext);
                  $lead_region                       = trim($event->find('td', 11)->plaintext);
                  $quality_score                     = trim($event->find('td', 12)->plaintext);
                  $service_score                     = trim($event->find('td', 13)->plaintext);
                  $sc_rating                         = trim($event->find('td', 14)->plaintext);
                  $ipb                               = trim($event->find('td', 15)->plaintext);
                  $severity_score                    = trim($event->find('td', 16)->plaintext);
                  $severity_ipb                      = trim($event->find('td', 17)->plaintext);
                  $prog_mgt_prrs                     = trim($event->find('td', 18)->plaintext);
                  $unauth_change                     = trim($event->find('td', 19)->plaintext);
                  $customer_sat_open                 = trim($event->find('td', 20)->plaintext);
                  $launch_prrs                       = trim($event->find('td', 21)->plaintext);
                  $field_action                      = trim($event->find('td', 22)->plaintext);
                  $plant_disruption                  = trim($event->find('td', 23)->plaintext);
                  $controlled_shipping_level1        = trim($event->find('td', 24)->plaintext);
                  $controlled_shipping_level2        = trim($event->find('td', 25)->plaintext);
                  $nbh                               = trim($event->find('td', 26)->plaintext);
                  $iso_14001                         = trim($event->find('td', 27)->plaintext);
                  $ts16949_cert                      = trim($event->find('td', 28)->plaintext);
                  $qsb_quality_systems_basis         = trim($event->find('td', 29)->plaintext);
                  $supplier_quality_excellence_award = trim($event->find('td', 30)->plaintext);

                  $sql = "INSERT INTO uscan_transactions.gm_historical_bidlist(`quality`,`supplychain`,`cca`,`country_risk`,`financial_risk`,`mmog_le`,`conflict_materials`,`diversity`,`name`,`location`,`mfg_duns`,`lead_region`,`quality_score`,`service_score`,`sc_rating`,`ipb`,`severity_score`,`severity_ipb`,`prog_mgt_prrs`,`unauth_change`,`customer_sat_open`,`launch_prrs`,`field_action`,`plant_disruption`,`controlled_shipping_level1`,`controlled_shipping_level2`,`nbh`,`iso_14001`,`ts16949_cert`,`qsb_quality_systems_basis`,`supplier_quality_excellence_award`,`transaction_date`,`log_id`)
                  VALUES ('$quality','$supplychain','$cca','$country_risk','$financial_risk','$mmog_le','$conflict_materials','$diversity','$name','$location','$mfg_duns','$lead_region','$quality_score','$service_score','$sc_rating','$ipb','$severity_score','$severity_ipb','$prog_mgt_prrs','$unauth_change','$customer_sat_open','$launch_prrs','$field_action','$plant_disruption','$controlled_shipping_level1','$controlled_shipping_level2','$nbh','$iso_14001','$ts16949_cert','$qsb_quality_systems_basis','$supplier_quality_excellence_award','$date_time_for_mysql','$log_id')";

                  if ($GLOBALS['conn_transaction']->query($sql) === TRUE) {
                      echo "New record created successfully /n ";
                  } else {
                      echo "Error: " . $sql . "<br>" . $GLOBALS['conn_transaction']->error;
                      $step_no     = $step_no + 1;
                      $description = "Error: " . $sql . "<br>" . $GLOBALS['conn_transaction']->error;
                      $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);
                  }

              }

              $step_no     = $step_no + 1;
              $description = "Data Inserted Successfully";
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);
              echo "---------******All records inserted successfully********--------";
              //Pavan download

              $data_excel = array(
                  'bidlistReportSearchData.reportLevel' => 'GPSC',
                  'bidlistReportSearchData.region' => '',
                  'bidlistReportSearchData.productionSupport' => '',
                  'bidlistReportSearchData.commodityCode' => '',
                  'bidlistReportSearchData.directorCode' => '',
                  'bidlistReportSearchData.creativityTeamCode' => '',
                  'bidlistReportSearchData.subTeamCode' => '',
                  'bidlistReportSearchData.majorFunctionCode' => '',
                  'bidlistReportSearchData`.commodityDesc' => '',
                  'bidlistReportSearchData.directorDesc' => '',
                  'bidlistReportSearchData.creativityTeamDesc' => '',
                  'bidlistReportSearchData.subTeamDesc' => '',
                  'bidlistReportSearchData.majorFunctionDesc' => '',
                  'suppliersDunsNumber' => '',
                  'bidlistReportSearchData.ultimateDuns' => '555897164'
              );

              $fi_excel               = http_build_query($data_excel);
              $nav_url_excel_download = 'https://smarttdm-gqts.gmsupplypower.com/GQTSWeb/metrics/DownLoadToExcel.do';
              curl_setopt($ch, CURLOPT_URL, $nav_url_excel_download);
              curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
              curl_setopt($ch, CURLOPT_POST, true);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $fi_excel);
              curl_setopt($ch, CURLOPT_HEADER, false);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_COOKIESESSION, true);
              curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name'); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie); //could be empty, but cause problems on some hosts
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
              curl_setopt($ch, CURLOPT_MAXREDIRS, 20);
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
              curl_setopt($ch, CURLOPT_TIMEOUT, 180);


              $answer9 = curl_exec($ch);
              $status  = curl_getinfo($ch);
              if ($status['http_code'] == '200' or $status['http_code'] == '302') {
                  $status_info = "Successful";
              } else {
                  $status_info = "Failed";


              }

              $step_no     = $step_no + 1;
              $description = "Status = " . $status_info . ";" . "Response code = " . $status['http_code'] . ";" . "URL = " . $nav_url_excel_download;
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);

              curl_close($ch);


              $file_name = "/var/www/uscan/uscan/public/crawler/documents/gm/gm_bidlist/gm_" . $date_time_for_file . ".xls";

              $file1 = $file_name;
              if (!(file_put_contents($file_name, $answer9))) {
                  echo "file_put error";
                  $step_no     = $step_no + 1;
                  $description = "Excel Sheet Download Failed";
                  $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);
              }

              $step_no     = $step_no + 1;
              $description = "Excel Sheet Downloaded Scuccessfully";
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);


              $subject = 'Scheduled Task';
              $FileName  = "GM Bidlist" . "_" . $log_id . "_" . $date_time_for_file . ".xls";
              // $path = "C:\\xampp\htdocs\uscan\public\crawler\documents\\ford\\ford_q1_scores\\Test Data.xls";

              $distribution_strategy = new distribution_strategy();
              $distribution_strategy_data = $distribution_strategy->distribution_strategy_check($task_id,$enterprise_id,$subject,$file_name,$FileName);

              if ($distribution_strategy_data)
              {
                $step_no = $step_no + 1;
                $description = $distribution_strategy_data;
                $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);
              }


              $step_no     = $step_no + 1;
              $description = "Task Completed";
              $process_monitor_log_update_new = $process_monitor_log_update->insert_log_data($log_id, $enterprise_id, $step_no, $description);


              echo "GM Bidlist downloaded successfully";
              }

              $GLOBALS['conn_master']->close();
              $GLOBALS['conn_transaction']->close();

              $process_monitor_log_update_new = $process_monitor_log_update->update_log_state($log_id,'Finished');


      return "job executed successfully gm_bidlist job id: $job_id";

    }

}
