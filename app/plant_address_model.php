<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class plant_address_model extends Model
{
    use SoftDeletes;
    protected $table = 'plant_address';
    public $timestamps = false;
    protected $dates = ['deleted_at'];
}
