<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class report_name_product_group_reference extends Model
{
    //
    protected $table = 'report_name_product_group';
    protected $dates = ['deleted_at'];
    public $timestamps = false;
}
