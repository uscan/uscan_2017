<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class task_type_model extends Model
{
    //
    use SoftDeletes;
    protected $table = 'task_type';
    protected $dates = ['deleted_at'];
}
